package controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import utilities.sql;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class MainController
 */
@WebServlet("/MainController")
public class MainController extends HttpServlet {
	private static final long serialVersionUID = 1L;
     public Connection conn;
     public ResultSet rs = null;
     public Statement stmt = null;
     model.answerBean mab = new model.answerBean();
    /**
     * @see HttpServlet#HttpServlet()
     */
     public void init(ServletConfig config) throws ServletException {
 		try
 		{
 			Class.forName("com.mysql.jdbc.Driver");
 			String username = "root";
 			String password = "p@ssword";
 			String url = "jdbc:mysql://localhost:3306/practice";
 			
 			conn = DriverManager.getConnection(url, username, password);
 		}	catch (ClassNotFoundException cnfe) {
 			System.err.println(cnfe.getMessage());
 		}
 		catch (SQLException sqle){
 			System.err.println(sqle.getMessage());
 		}
 		
 	}

    public MainController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
    
    public void doGet(HttpServletRequest request, HttpServletResponse response)
    		throws IOException{
    	if(rs != null){
    		try
    		{
				rs.close();
			} catch (SQLException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    	

	    	try{
				if(conn != null)
				{	
					
					ResultSet rs =  setResultSet(conn, stmt);
					
					//perform binding to HttpSession
					HttpSession session = request.getSession();
					session.setAttribute("records", rs);
					
					
					RequestDispatcher dispatcher =
							request.getRequestDispatcher("index.jsp");
					dispatcher.forward(request, response);				
				}
				else
				{
					System.err.println("Connection is NULL.");
				}
			}
			catch (ServletException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	
		
	}
    		
	private ResultSet setResultSet(Connection conn2, Statement stmt2) {
		// TODO Auto-generated method stub
		return null;
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
