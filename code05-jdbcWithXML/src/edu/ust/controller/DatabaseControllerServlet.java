package edu.ust.controller;

import java.io.IOException;


import javax.annotation.PostConstruct;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.sql.*;

import edu.ust.model.ConnectionBean;
import edu.ust.utility.DatabaseUtility;
@WebServlet("/index.html")
public class DatabaseControllerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static Connection connection;
	private ConnectionBean connBean;
	
	@PostConstruct
	public void databaseAdminTask() {
		DatabaseUtility dbUtilityTask = new DatabaseUtility();
		//dbUtilityTask.createXMLDatabaseConfiguration();
		connBean = dbUtilityTask.getConnectionBeanFromXMLDatabaseConfiguration();
	}
	
	public void init(ServletConfig config) throws ServletException {
		// TODO This is where we will perform database connection
		
		
		if (connBean != null) {
			try {	
				Class.forName(connBean.getDriverClass());
				// jdbc:mysql://localhost:3306/
				String url = connBean.getJdbcPartURL()+"//"
					+ connBean.getServer() + ":" + connBean.getPort()
					+ "/" + connBean.getDatabase(); 
				
				System.out.println("JDBC URL: " + url);
				connection = 
				  DriverManager.getConnection(url,connBean.getUserName(),
						 connBean.getPassword());
			} catch (SQLException sqle){
				System.out.println("SQLException error occured - " 
					+ sqle.getMessage());
			} catch (ClassNotFoundException nfe){
				System.out.println("ClassNotFoundException error occured - " 
			        + nfe.getMessage());
			}
		} else {
			System.err.println("ConnectionBean is NULL.");
		}
			
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {	
			if (connection != null) {
				Statement stmt = connection.createStatement();
				ResultSet rs = stmt.executeQuery("select * from employee");	
				System.out.println("Successful connection");
				
				request.setAttribute("dbRecord", rs);
				
				RequestDispatcher dispatcher = 
						request.getRequestDispatcher("displayrecords.jsp");
				dispatcher.forward(request, response);
			} else {
				response.sendRedirect("error.jsp");
			}
		} catch (SQLException sqle){
			System.out.println("SQLException error occured - " 
				+ sqle.getMessage());
			response.sendRedirect("error.jsp");
		}	
	}

}
