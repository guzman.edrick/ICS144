package edu.ust.utility;

import java.beans.XMLDecoder;

import java.beans.XMLEncoder;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.*;

import edu.ust.model.ConnectionBean;


public class DatabaseUtility {

	private ConnectionBean connBean;
	
	public  void createXMLDatabaseConfiguration() {
		try {
			connBean.setDatabase("practice");
			connBean.setDriverClass("com.mysql.jdbc.Driver");
			connBean.setJdbcPartURL("jdbc:mysql:");
			connBean.setPassword("p@ssword");
			connBean.setPort("3306");
			connBean.setServer("localhost"); //or an IP Address if connected to network
			connBean.setUserName("root");
			
			//create an XML configuration file
			XMLEncoder encoder = new XMLEncoder(
                new BufferedOutputStream(
                new FileOutputStream("C:\\Users\\Edrick\\Desktop\\newWorkspace\\db_connection.xml")));
			encoder.writeObject(connBean);
			encoder.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	} 
	
	public ConnectionBean getConnectionBeanFromXMLDatabaseConfiguration() {
		try {
			XMLDecoder decoder = new XMLDecoder(
	                new BufferedInputStream(
	                    new FileInputStream("C:\\Users\\Yves\\Downloads\\ICS114\\db_connection_xml_encrypt\\dbconfig.xml")));
			connBean = (ConnectionBean)decoder.readObject();
			decoder.close();
		} catch (Exception e) {
			System.out.println("Exception e");
		}
		return connBean;
	}
}
