package utility;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class writePropertiesFile {
	public static void main(String[] args) {
		try {
			Properties properties = new Properties();
			properties.setProperty("driver", "com.mysql.jdbc.Driver");
			properties.setProperty("username", "root");
			properties.setProperty("password", "p@ssword");
			properties.setProperty("URL", "jdbc:mysql://localhost:3306/");
			properties.setProperty("database", "kusinaatkultura");
			properties.setProperty("menuTable", "kk_menu");
			properties.setProperty("transactionTable", "kk_transactions");
			properties.setProperty("accountsTable", "kk_accounts");
			properties.setProperty("inventoryTable", "kk_inventory");
			properties.setProperty("reservationsTable", "kk_reservations");
			properties.setProperty("customersTable", "kk_customers");
			File file = new File("resources/config.properties");
			FileOutputStream fileOut = new FileOutputStream(file);
			properties.store(fileOut, "Property:");
			fileOut.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}

