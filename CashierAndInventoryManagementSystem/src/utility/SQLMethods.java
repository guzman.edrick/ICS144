package utility;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTable;

import utilities.sql;

public class SQLMethods {
		
	private String id;
	private String classification;
	private double price;
	public DefaultListModel populateJList(PreparedStatement pstmt, DefaultListModel dlm, JList list, String query){
		sql sb = new sql();
		Connection conn = sb.getDBConnection();
		ResultSet rs = null;
		try 
		{			
			pstmt = conn.prepareStatement(query);
			rs = pstmt.executeQuery();
			while (rs.next()) {
	        	dlm.addElement(rs.getString(1));
			}
			
			
		}
		catch(Exception e){
			JOptionPane.showMessageDialog(null, e);
			e.printStackTrace();
		}finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (pstmt != null) pstmt.close(); } catch (Exception e) {};	
		    try { if (conn != null) conn.close(); } catch (Exception e) {};
		}
		
		return dlm;		
	}
	
	public String setCashierLabels(JLabel id, JLabel name, JLabel price, JLabel description, PreparedStatement pstmt, 
								String query, String anotherID, String classification, ArrayList<String> idArray,
								 ArrayList<Double> priceArrayzz){
		sql sb = new sql();
		
		
		Connection conn = sb.getDBConnection();
		ResultSet rs = null;
		
		try {
			pstmt = conn.prepareStatement(query);
			rs= pstmt.executeQuery();
			while(rs.next())
			{
				id.setText("ID: " + rs.getString("menu_id"));
				name.setText("Name: " + rs.getString("menu_name"));
				price.setText("Price: " + rs.getDouble("price"));
				description.setText(rs.getString("description"));	
				setPrice(rs.getDouble("price"));	
				setId(rs.getString("menu_id"));
				setClassification(rs.getString("classification"));
				idArray.add(rs.getString("menu_id"));
			}

			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, e);
			e.printStackTrace();
		} catch (NullPointerException e){
			JOptionPane.showMessageDialog(null, e);
			e.printStackTrace();
		}
		return anotherID;
		
	}
	
	public void updateCashier(Connection conn, String query){
		try {
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(query);
			JOptionPane.showMessageDialog(null, "Update Completed");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
		public void setId(String id){
			this.id = id;
		}
		public String getId(){
			return id;
		}
		public void setClassification(String classification){
			this.classification = classification;
		}
		public String getClassification(){
			return classification;
		}

		public double getPrice() {
			return price;
		}

		public void setPrice(double price) {
			this.price = price;
		}
		
		
	
}
