package core;

import java.awt.BorderLayout;

import utilities.sql;
import utility.SQLCommands;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.JButton;

import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.border.TitledBorder;
import javax.swing.SwingConstants;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;
import java.awt.Color;
import java.awt.SystemColor;

import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;

public class CashierPage extends JFrame {

	private JPanel contentPane;
	public static FileInputStream fileInput;;
	public static Properties properties = new Properties();
	sql sb = new sql();
	Connection conn = sb.getDBConnection();
	Connection conn2 = sb.getDBConnection();
	String queryKus, queryKul, queryDar, querySpe;
	PreparedStatement pstmt1,pstmt2,pstmt3,pstmt4 = null;
	PreparedStatement pstmt_kultura,pstmt_kusina,pstmt_special,pstmt_dar = null;
	String id, classification;
	double totalNum = 0;
	double price = 0;
	utility.centerWindow ucw = new utility.centerWindow();
	ArrayList<String> idArray = new ArrayList<String>();
	ArrayList<Double> priceArray = new ArrayList<Double>();
	ArrayList<Integer> quantityArray = new ArrayList<Integer>();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CashierPage frame = new CashierPage();
					fileInput = new FileInputStream("resources/config.properties");
					properties.load(fileInput);
					frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CashierPage() {
		try {
			fileInput = new FileInputStream("resources/config.properties");
			try {
				properties.load(fileInput);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		final utility.SQLMethods sm = new utility.SQLMethods();
		final utility.miscMethods mm = new utility.miscMethods();
		setTitle("Cashier Page");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 1370, 727);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.windowBorder);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		final JSpinner quantityOfDisplay = new JSpinner();
		quantityOfDisplay.setFont(new Font("Tahoma", Font.PLAIN, 17));
		quantityOfDisplay.setBounds(1021, 431, 109, 51);
		contentPane.add(quantityOfDisplay);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBackground(SystemColor.textHighlight);
		panel_4.setBorder(new TitledBorder(null, "Kusina", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_4.setBounds(18, 76, 254, 326);
		contentPane.add(panel_4);
		panel_4.setLayout(null);
		
		//JList (kusina)
		final JList kusina_list = new JList();
		kusina_list.setFont(new Font("Tahoma", Font.PLAIN, 20));
		kusina_list.setBounds(6, 16, 242, 303);
		panel_4.add(kusina_list);
		kusina_list.setBackground(new Color(255, 255, 255));
		DefaultListModel dlmKusina = new DefaultListModel();		
		queryKus = "SELECT * FROM " + properties.getProperty("menuTable") + " WHERE classification = 'kusina'";
		kusina_list.setModel(sm.populateJList(pstmt1, dlmKusina, kusina_list, queryKus));
		
		final JLabel total = new JLabel("0.0");
		total.setForeground(SystemColor.text);
		total.setFont(new Font("Tahoma", Font.PLAIN, 90));
		total.setBounds(611, 454, 490, 227);
		contentPane.add(total);
		
		JPanel panel_5 = new JPanel();
		panel_5.setBackground(SystemColor.textHighlight);
		panel_5.setBorder(new TitledBorder(null, "Specials", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_5.setBounds(18, 459, 255, 145);
		contentPane.add(panel_5);
		panel_5.setLayout(null);
		
		//JList (specials)
		final JList specials_list = new JList();
		specials_list.setFont(new Font("Tahoma", Font.PLAIN, 20));
		specials_list.setBounds(6, 16, 243, 122);
		panel_5.add(specials_list);		
		DefaultListModel dlmSpecials = new DefaultListModel();
		querySpe = "SELECT * FROM " + properties.getProperty("menuTable") + " WHERE classification = 'specials'";
		specials_list.setModel(sm.populateJList(pstmt2, dlmSpecials, specials_list, querySpe));
		
		JPanel panel_3 = new JPanel();
		panel_3.setBackground(SystemColor.textHighlight);
		panel_3.setBorder(new TitledBorder(null, "Kultura", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_3.setBounds(285, 76, 254, 326);
		contentPane.add(panel_3);
		panel_3.setLayout(null);
		
		//JList (kultura)
		final JList kultura_list = new JList();
		kultura_list.setFont(new Font("Tahoma", Font.PLAIN, 20));
		kultura_list.setBounds(6, 16, 242, 303);
		panel_3.add(kultura_list);
		DefaultListModel dlmKultura = new DefaultListModel();
		queryKul = "SELECT * FROM " + properties.getProperty("menuTable") + " WHERE classification = 'kultura'";		
		kultura_list.setModel(sm.populateJList(pstmt3, dlmKultura, kultura_list, queryKul));
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(new Color(51, 153, 255));
		panel_2.setBorder(new TitledBorder(null, "Desserts/Drinks", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_2.setBounds(547, 76, 254, 326);
		contentPane.add(panel_2);
		panel_2.setLayout(null);
			
		//JList (Drinks and Refreshments)
		final JList dar_list = new JList();
		dar_list.setFont(new Font("Tahoma", Font.PLAIN, 20));
		DefaultListModel dlmDar = new DefaultListModel();
		dar_list.setBounds(6, 16, 242, 303);
		panel_2.add(dar_list);
		queryDar = "SELECT * FROM " + properties.getProperty("menuTable") + " WHERE classification = 'drinks' OR classification = 'dessert'";		
		dar_list.setModel(sm.populateJList(pstmt4, dlmDar, dar_list, queryDar));
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(SystemColor.textHighlight);
		panel_1.setBorder(new TitledBorder(null, "Description", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.setBounds(859, 10, 462, 354);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(6, 16, 450, 333);
		panel_1.add(panel);
		panel.setLayout(null);
		
		final JLabel id_label = new JLabel("ID: ");
		id_label.setFont(new Font("Tahoma", Font.PLAIN, 30));
		id_label.setBounds(10, 11, 430, 33);
		panel.add(id_label);
		
		final JLabel name_label = new JLabel("NAME:");
		name_label.setFont(new Font("Tahoma", Font.PLAIN, 25));
		name_label.setBounds(10, 50, 430, 63);
		panel.add(name_label);
		
		final JLabel price_label = new JLabel("PRICE:");
		price_label.setFont(new Font("Tahoma", Font.PLAIN, 25));
		price_label.setBounds(10, 99, 200, 50);
		panel.add(price_label);
		
		JLabel label_desc = new JLabel("DESCRIPTION:");
		label_desc.setFont(new Font("Tahoma", Font.PLAIN, 20));
		label_desc.setBounds(10, 140, 200, 50);
		panel.add(label_desc);
		
		final JLabel label_description = new JLabel("(Description here)");
		label_description.setFont(new Font("Tahoma", Font.PLAIN, 15));
		label_description.setVerticalAlignment(SwingConstants.TOP);
		label_description.setBounds(10, 182, 430, 132);
		panel.add(label_description);
		
		final JButton confirm = new JButton("CONFIRM");
		confirm.setFont(new Font("Tahoma", Font.PLAIN, 33));
		confirm.setBounds(1130, 431, 191, 51);
		contentPane.add(confirm);
		confirm.setEnabled(false);
		
		JLabel lblQuantity = new JLabel("Quantity:");
		lblQuantity.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblQuantity.setForeground(SystemColor.textHighlightText);
		lblQuantity.setBounds(869, 431, 147, 45);
		contentPane.add(lblQuantity);
		
		JButton add = new JButton("Add Menu");
		add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AddMenu am = new AddMenu();	
				ucw.centerWindow(am);				
				am.setVisible(true);						
			}
		});
		add.setForeground(new Color(0, 0, 0));
		add.setBackground(new Color(50, 205, 50));
		add.setFont(new Font("Tahoma", Font.PLAIN, 20));
		add.setBounds(18, 10, 254, 36);
		contentPane.add(add);
		add.setVisible(false);
		
		JButton delete = new JButton("Delete Menu");
		delete.setBackground(new Color(255, 0, 0));
		delete.setForeground(new Color(0, 0, 0));
		delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DeleteMenu dm = new DeleteMenu();
				ucw.centerWindow(dm);
				dm.setVisible(true);
			}
		});
		delete.setFont(new Font("Tahoma", Font.PLAIN, 20));
		delete.setBounds(285, 10, 254, 36);
		contentPane.add(delete);
		delete.setVisible(false);
		
		JLabel lblTotal = new JLabel("TOTAL:");
		lblTotal.setForeground(Color.WHITE);
		lblTotal.setFont(new Font("Tahoma", Font.PLAIN, 90));
		lblTotal.setBounds(276, 454, 567, 214);
		contentPane.add(lblTotal);
		
		JButton btnNewButton = new JButton("Finalize");
		btnNewButton.setBackground(SystemColor.textHighlight);
		
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 50));
		btnNewButton.setBounds(902, 623, 442, 69);
		contentPane.add(btnNewButton);
		
		JButton btnSetupCashier = new JButton("Setup Cashier");
		btnSetupCashier.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Cashier cash = new Cashier();
				ucw.centerWindow(cash);
				cash.setVisible(true);
			}
		});
		btnSetupCashier.setFont(new Font("Tahoma", Font.PLAIN, 50));
		btnSetupCashier.setBackground(Color.YELLOW);
		btnSetupCashier.setBounds(444, 623, 442, 69);
		contentPane.add(btnSetupCashier);
		btnSetupCashier.setVisible(false);
		
		
		kusina_list.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				int num = kusina_list.getSelectedIndex();
				String query ="select * from kk_menu WHERE menu_id = '" + kusina_list.getSelectedValue().toString() + "'";					
				sm.setCashierLabels(id_label, name_label, price_label, label_description,  pstmt_kusina, query, id, classification, idArray, priceArray);
				id = sm.getClassification();
				classification = sm.getClassification();
				price = sm.getPrice();
				confirm.setEnabled(true);
				
			}
		});
		
		kultura_list.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				int num = kultura_list.getSelectedIndex();
				String query ="select * from kk_menu WHERE menu_id = '" + kultura_list.getSelectedValue().toString() + "'";		
				sm.setCashierLabels(id_label, name_label, price_label, label_description, pstmt_kultura, query, id,classification, idArray, priceArray);
				id = sm.getClassification();
				classification = sm.getClassification();
				price = sm.getPrice();
				confirm.setEnabled(true);
			}
		});
		
		dar_list.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				int num = dar_list.getSelectedIndex();
				String query ="select * from kk_menu WHERE menu_id = '" + dar_list.getSelectedValue().toString() + "'";	
				sm.setCashierLabels(id_label, name_label, price_label, label_description, pstmt_dar, query, id,classification, idArray, priceArray);
				id = sm.getClassification();
				classification = sm.getClassification();
				price = sm.getPrice();
				confirm.setEnabled(true);
			}
		});
		specials_list.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				int num = specials_list.getSelectedIndex();
				String query ="select * from kk_menu WHERE menu_id = '" + specials_list.getSelectedValue().toString() + "'";
				sm.setCashierLabels(id_label, name_label, price_label, label_description, pstmt_special, query, id, classification, idArray, priceArray);
				id = sm.getClassification();
				classification = sm.getClassification();
				price = sm.getPrice();
				confirm.setEnabled(true);
			}
		});




		confirm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int	quantity = (Integer)quantityOfDisplay.getValue();
				quantityArray.add(quantity);
				if(quantity == 0){
					totalNum = totalNum + 0;
				}
				else{
					totalNum = totalNum + (price * quantity);
					priceArray.add(price*quantity);
				}
				quantityOfDisplay.setValue(0);
			
				id_label.setText("ID: ");
				name_label.setText("Name: ");
				price_label.setText("Price: ");
				label_description.setText("Insert description here");
				confirm.setEnabled(false);
				total.setText(Double.toString(totalNum));
				JOptionPane.showMessageDialog(null, quantity + " order(s) placed");			
			}
		});
		
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String money = JOptionPane.showInputDialog("Input customer payment amount");
				double change =  (Double.parseDouble(money)) - totalNum;
				core.ComputeChange ccc = new core.ComputeChange(totalNum, money, change);
				
				ucw.centerWindow(ccc);
				ccc.setVisible(true);
			}
		});
		
		
		
	}
}
