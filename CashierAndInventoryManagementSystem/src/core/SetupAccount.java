package core;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;

import java.awt.Color;

import javax.swing.DefaultComboBoxModel;

import utilities.sql;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.swing.JPasswordField;



public class SetupAccount extends JFrame {

	private JPanel contentPane;
	private JTextField username;
	private JTextField name;
	
	sql sb = new sql();
	Connection conn = sb.getDBConnection();
	private JPasswordField password;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SetupAccount frame = new SetupAccount();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SetupAccount() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 401, 331);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblCreateAccount = new JLabel("Create Account");
		lblCreateAccount.setFont(new Font("Times New Roman", Font.BOLD, 18));
		lblCreateAccount.setBounds(125, 10, 152, 44);
		contentPane.add(lblCreateAccount);
		
		JLabel lblUsername = new JLabel("Username:");
		lblUsername.setFont(new Font("Times New Roman", Font.BOLD, 15));
		lblUsername.setBounds(10, 65, 103, 25);
		contentPane.add(lblUsername);
		
		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setFont(new Font("Times New Roman", Font.BOLD, 15));
		lblPassword.setBounds(10, 101, 103, 26);
		contentPane.add(lblPassword);
		
		JLabel lblPosition = new JLabel("Position:");
		lblPosition.setFont(new Font("Times New Roman", Font.BOLD, 15));
		lblPosition.setBounds(10, 138, 80, 31);
		contentPane.add(lblPosition);
		
		JLabel lblName = new JLabel("Name:");
		lblName.setFont(new Font("Times New Roman", Font.BOLD, 15));
		lblName.setBounds(10, 180, 80, 29);
		contentPane.add(lblName);
		
		username = new JTextField();
		username.setBounds(115, 65, 230, 25);
		contentPane.add(username);
		username.setColumns(10);
		
		name = new JTextField();
		name.setColumns(10);
		name.setBounds(115, 180, 230, 25);
		contentPane.add(name);
		
		final JComboBox position = new JComboBox();
		position.setModel(new DefaultComboBoxModel(new String[] {"", "admin", "cashier"}));
		position.setBounds(115, 144, 230, 25);
		contentPane.add(position);
		
		JButton btnNewButton = new JButton("Create");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String user = username.getText();
				String pass = password.getText();
				String pos = position.getSelectedItem().toString();
				String n = name.getText();
				String query = "INSERT INTO `kusinaatkultura`.`kk_accounts` (`username`, `pass`, `position`, `name`) VALUES (?, ?, ?, ?)";
				try{
					PreparedStatement pstmt = conn.prepareStatement(query);
					pstmt.setString(1, user);
					pstmt.setString(2, pass);
					pstmt.setString(3, pos);
					pstmt.setString(4, n);
					pstmt.executeUpdate();
					pstmt.close();
					JOptionPane.showMessageDialog(null, "Account of " + n + " is successfully created");
				}catch(SQLException e){
					JOptionPane.showMessageDialog(null, e);
				}
			}
		});
		btnNewButton.setBackground(Color.GREEN);
		btnNewButton.setFont(new Font("Times New Roman", Font.BOLD, 15));
		btnNewButton.setBounds(40, 230, 117, 38);
		contentPane.add(btnNewButton);
		
		JButton btnReturn = new JButton("Return");
		btnReturn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		btnReturn.setFont(new Font("Times New Roman", Font.BOLD, 15));
		btnReturn.setBackground(Color.RED);
		btnReturn.setBounds(211, 230, 117, 38);
		contentPane.add(btnReturn);
		
		password = new JPasswordField();
		password.setBounds(115, 101, 230, 24);
		contentPane.add(password);
	}
}
