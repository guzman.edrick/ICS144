package core;

import java.awt.BorderLayout;




import utilities.sql;
import model.setQueryLogin;

import java.awt.EventQueue;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

	public class DeleteMenu extends JFrame {
	
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private JPanel contentPane;
		public static FileInputStream fileInput;;
		public static Properties properties = new Properties();
		static String user;
		static String pass;
		String name;
		static sql sb = new sql();
		static Connection conn = sb.getDBConnection();
		private JTextField menu_id;
		/**
		 * Launch the application.
		 */
		public static void main(String[] args) {
			EventQueue.invokeLater(new Runnable() {
				public void run() {
					try {
						fileInput = new FileInputStream("resources/config.properties");
						properties.load(fileInput);
						DeleteMenu frame = new DeleteMenu();
						frame.setVisible(true);
						sql sb = new sql();
						sb.getDBConnection();
	
						utility.centerWindow cw = new utility.centerWindow();
						cw.centerWindow(frame);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		}
	
		
		/**
		 * Create the frame.
		 */
		public DeleteMenu() {
		
			setTitle("Add Menu");
			setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			setBounds(100, 100, 346, 248);
			contentPane = new JPanel();
			contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
			setContentPane(contentPane);
			contentPane.setLayout(null);
			
			JLabel lblMenuId = new JLabel("Menu ID:");
			lblMenuId.setFont(new Font("Tahoma", Font.PLAIN, 15));
			lblMenuId.setBounds(22, 68, 101, 25);
			contentPane.add(lblMenuId);
			
			JLabel lblClassification = new JLabel("Classification:");
			lblClassification.setFont(new Font("Tahoma", Font.PLAIN, 15));
			lblClassification.setBounds(22, 26, 139, 31);
			contentPane.add(lblClassification);
			
			menu_id = new JTextField();
			menu_id.setBounds(234, 70, 41, 25);
			contentPane.add(menu_id);
			menu_id.setColumns(10);
			
			final JLabel label_prefix = new JLabel("(PREFIX)");
			label_prefix.setBounds(133, 70, 77, 25);
			contentPane.add(label_prefix);
			
			JButton add = new JButton("DELETE MENU");
			add.setFont(new Font("Tahoma", Font.PLAIN, 16));
			add.setBounds(80, 124, 148, 35);
			contentPane.add(add);
			
			final JComboBox menu_classification = new JComboBox();
			menu_classification.setModel(new DefaultComboBoxModel(new String[] {"", "kusina", "kultura", "dessert", "drinks", "specials"}));
			menu_classification.setBounds(171, 23, 136, 34);
			contentPane.add(menu_classification);
			
			JButton btnImDone = new JButton("I'M DONE");
			btnImDone.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					core.AdminCashierPage cac = new core.AdminCashierPage();
					dispose();
					java.awt.Window win[] = java.awt.Window.getWindows(); 
					for(int i=0;i<win.length;i++){ 
					win[i].dispose(); 
					} 
					
					cac.setExtendedState(JFrame.MAXIMIZED_BOTH);
					cac.setVisible(true);
					
				}
			});
			btnImDone.setBounds(109, 170, 101, 23);
			contentPane.add(btnImDone);
			
			try{
				String query = "SELECT name FROM kk_accounts WHERE username = '" + user + "'";
				
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(query);
				while(rs.next()){
					name = rs.getString(1);
				}
			
			}catch(Exception e){
				JOptionPane.showMessageDialog(null,e);
			}
			
	
			menu_classification.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent arg0) {
					String classification = menu_classification.getSelectedItem().toString();				
					String prefix;
					if(classification.equals("kusina")){
						prefix = "KSN";
						label_prefix.setText(prefix);
					}
					else if(classification.equals("kultura")){
						prefix = "KTR";
						label_prefix.setText(prefix);
					}
					else if(classification.equals("drinks")){
						prefix = "DRI";
						label_prefix.setText(prefix);
					}
					else if(classification.equals("dessert")){
						prefix = "DES";
						label_prefix.setText(prefix);
					}
					else if(classification.equals("specials")){
						prefix = "SPE";					
						label_prefix.setText(prefix);
					}
				}
			});
			add.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					String classification = menu_classification.getSelectedItem().toString();									
					String prefix;
					String suffix = menu_id.getText();
					String id = " ", query;
					PreparedStatement pstmt;
					if(classification.equals("kusina")){
						prefix = "KSN";
						label_prefix.setText(prefix);
						id = prefix+suffix;	
					}
					else if(classification.equals("kultura")){
						prefix = "KTR";
						label_prefix.setText(prefix);
						id = prefix+suffix;
					}
					else if(classification.equals("drinks")){
						prefix = "DRI";
						label_prefix.setText(prefix);
						id = prefix+suffix;
					}
					else if(classification.equals("dessert")){
						prefix = "DES";
						label_prefix.setText(prefix);
						id = prefix+suffix;
					}
					else if(classification.equals("specials")){
						prefix = "SPE";					
						label_prefix.setText(prefix);
						id = prefix+suffix;
					}
					
					query = "DELETE FROM `kk_menu` WHERE classification = ? AND menu_id = ?";
					try {
						pstmt = conn.prepareStatement(query);
						pstmt.setString(1, classification);
						pstmt.setString(2, id);					
						
						pstmt.executeUpdate();
						JOptionPane.showMessageDialog(null,id + " was deleted from the menu");
						pstmt.close();
						
						
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						JOptionPane.showMessageDialog(null, e);
						e.printStackTrace();
					}
				}
					
			});
		}
	}
