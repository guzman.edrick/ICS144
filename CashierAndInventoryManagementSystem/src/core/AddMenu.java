package core;

import java.awt.BorderLayout;




import utilities.sql;
import model.setQueryLogin;

import java.awt.EventQueue;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class AddMenu extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	public static FileInputStream fileInput;;
	public static Properties properties = new Properties();
	static String user;
	static String pass;
	String name;
	static sql sb = new sql();
	static Connection conn = sb.getDBConnection();
	private JTextField menu_description;
	private JTextField menu_id;
	private JTextField menu_name;
	private JTextField menu_price;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					fileInput = new FileInputStream("resources/config.properties");
					properties.load(fileInput);
					AddMenu frame = new AddMenu();
					frame.setVisible(true);
					sql sb = new sql();
					sb.getDBConnection();

					utility.centerWindow cw = new utility.centerWindow();
					cw.centerWindow(frame);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	/**
	 * Create the frame.
	 */
	public AddMenu() {
	
		setTitle("Add Menu");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 544, 368);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblMenuId = new JLabel("Menu ID:");
		lblMenuId.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblMenuId.setBounds(22, 99, 101, 25);
		contentPane.add(lblMenuId);
		
		JLabel lblMenuName = new JLabel("Menu Name:");
		lblMenuName.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblMenuName.setBounds(22, 68, 109, 20);
		contentPane.add(lblMenuName);
		
		JLabel lblPrice = new JLabel("Price:");
		lblPrice.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblPrice.setBounds(317, 23, 84, 34);
		contentPane.add(lblPrice);
		
		JLabel lblClassification = new JLabel("Classification:");
		lblClassification.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblClassification.setBounds(22, 26, 139, 31);
		contentPane.add(lblClassification);
		
		JLabel lblDescription = new JLabel("Description:");
		lblDescription.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblDescription.setBounds(22, 124, 143, 25);
		contentPane.add(lblDescription);
		
		menu_description = new JTextField();
		menu_description.setBounds(22, 160, 482, 100);
		contentPane.add(menu_description);
		menu_description.setColumns(10);
		
		menu_id = new JTextField();
		menu_id.setBounds(216, 99, 41, 25);
		contentPane.add(menu_id);
		menu_id.setColumns(10);
		
		final JLabel label_prefix = new JLabel("(PREFIX)");
		label_prefix.setBounds(123, 99, 77, 25);
		contentPane.add(label_prefix);
		
		JButton add = new JButton("ADD MENU");
		add.setFont(new Font("Tahoma", Font.PLAIN, 16));
		add.setBounds(181, 271, 148, 35);
		contentPane.add(add);
		
		menu_name = new JTextField();
		menu_name.setBounds(131, 68, 373, 20);
		contentPane.add(menu_name);
		menu_name.setColumns(10);
		
		final JComboBox menu_classification = new JComboBox();
		menu_classification.setModel(new DefaultComboBoxModel(new String[] {"", "kusina", "kultura", "dessert", "drinks", "specials"}));
		menu_classification.setBounds(171, 23, 136, 34);
		contentPane.add(menu_classification);
		
		menu_price = new JTextField();
		menu_price.setBounds(411, 23, 91, 34);
		contentPane.add(menu_price);
		menu_price.setColumns(10);
		
		JButton btnImDone = new JButton("I'M DONE");
		btnImDone.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				core.AdminCashierPage cac = new core.AdminCashierPage();
				dispose();
				java.awt.Window win[] = java.awt.Window.getWindows(); 
				for(int i=0;i<win.length;i++){ 
				win[i].dispose(); 
				} 
				
				cac.setExtendedState(JFrame.MAXIMIZED_BOTH);
				cac.setVisible(true);
				
			}
		});
		btnImDone.setBounds(429, 299, 89, 23);
		contentPane.add(btnImDone);
		
		try{
			String query = "SELECT name FROM kk_accounts WHERE username = '" + user + "'";
			
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while(rs.next()){
				name = rs.getString(1);
			}
		
		}catch(Exception e){
			JOptionPane.showMessageDialog(null,e);
		}
		

		menu_classification.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				String classification = menu_classification.getSelectedItem().toString();				
				String prefix;
				if(classification.equals("kusina")){
					prefix = "KSN";
					label_prefix.setText(prefix);
				}
				else if(classification.equals("kultura")){
					prefix = "KTR";
					label_prefix.setText(prefix);
				}
				else if(classification.equals("drinks")){
					prefix = "DRI";
					label_prefix.setText(prefix);
				}
				else if(classification.equals("desserts")){
					prefix = "DES";
					label_prefix.setText(prefix);
				}
				else if(classification.equals("specials")){
					prefix = "SPE";					
					label_prefix.setText(prefix);
				}
			}
		});
		add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String classification = menu_classification.getSelectedItem().toString();
				Double price = Double.parseDouble(menu_price.getText().toString());
				String name = menu_name.getText();
				String prefix;
				String suffix = menu_id.getText();
				String description = menu_description.getText();
				String id = " ", query;
				PreparedStatement pstmt;
				if(classification.equals("kusina")){
					prefix = "KSN";
					label_prefix.setText(prefix);
					id = prefix+suffix;	
				}
				else if(classification.equals("kultura")){
					prefix = "KTR";
					label_prefix.setText(prefix);
					id = prefix+suffix;
				}
				else if(classification.equals("drinks")){
					prefix = "DRI";
					label_prefix.setText(prefix);
					id = prefix+suffix;
				}
				else if(classification.equals("desserts")){
					prefix = "DES";
					label_prefix.setText(prefix);
					id = prefix+suffix;
				}
				else if(classification.equals("desserts")){
					prefix = "SPE";					
					label_prefix.setText(prefix);
					id = prefix+suffix;
				}
				
				query = "INSERT INTO `kk_menu` (`menu_id`, `menu_name`, `price`, `classification`, `description`) VALUES (?,?,?,?,?)";
				try {
					pstmt = conn.prepareStatement(query);
					pstmt.setString(1, id);
					pstmt.setString(2, name);
					pstmt.setDouble(3, price);
					pstmt.setString(4, classification);
					pstmt.setString(5, description);
					pstmt.executeUpdate();
					JOptionPane.showMessageDialog(null,id + ": " + name + " was successfully added to the menu");
					pstmt.close();
					
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(null, e);
					e.printStackTrace();
				}
			}
				
		});
	}
}
