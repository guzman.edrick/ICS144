package core;

import java.awt.BorderLayout;


import utilities.sql;
import model.setQueryLogin;

import java.awt.EventQueue;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AdminPage extends JFrame {

	private JPanel contentPane;
	public static FileInputStream fileInput;;
	public static Properties properties = new Properties();
	static String user;
	static String pass;
	String name;
	sql sb = new sql();
	Connection conn = sb.getDBConnection();
	utility.centerWindow ucw = new utility.centerWindow();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					fileInput = new FileInputStream("resources/config.properties");
					properties.load(fileInput);
					AdminPage frame = new AdminPage(user, pass);
					frame.setVisible(true);
					
					utility.centerWindow cw = new utility.centerWindow();
					cw.centerWindow(frame);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	/**
	 * Create the frame.
	 */
	public AdminPage(String user, String pass) {
		this.user=user;
		this.pass=pass;
		setTitle("Admin Page");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton = new JButton("Manage Inventory");
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 13));
		btnNewButton.setBounds(74, 54, 284, 23);
		contentPane.add(btnNewButton);
		
		JButton btnManageTransactions = new JButton("Manage Transactions");
		btnManageTransactions.setFont(new Font("Tahoma", Font.PLAIN, 13));
		btnManageTransactions.setBounds(74, 88, 284, 23);
		contentPane.add(btnManageTransactions);
		
		JButton btnSetReservationcatering = new JButton("Set reservation/catering");
		btnSetReservationcatering.setFont(new Font("Tahoma", Font.PLAIN, 13));
		btnSetReservationcatering.setBounds(74, 122, 284, 23);
		contentPane.add(btnSetReservationcatering);
		
		JButton btnSetupAccount = new JButton("Set-up account");
		btnSetupAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				SetupAccount sa = new SetupAccount();
				ucw.centerWindow(sa);
				sa.setVisible(true);
			}
		});
		btnSetupAccount.setFont(new Font("Tahoma", Font.PLAIN, 13));
		btnSetupAccount.setBounds(74, 156, 284, 23);
		contentPane.add(btnSetupAccount);
		
		JLabel hello = new JLabel();
		hello.setFont(new Font("Times New Roman", Font.PLAIN, 24));
		hello.setBounds(143, 11, 184, 32);
		contentPane.add(hello);
		
		try{
			String query = "SELECT name FROM kk_accounts WHERE username = '" + user + "'";
			
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while(rs.next()){
				name = rs.getString(1);
			}
			hello.setText("Hello: " + name);
		
		}catch(Exception e){
			JOptionPane.showMessageDialog(null,e);
		}
		
		
		
		JButton btnOpenCashier = new JButton("Open Cashier");
		btnOpenCashier.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				core.AdminCashierPage acp = new core.AdminCashierPage();
				ucw.centerWindow(acp);
				acp.setVisible(true);
				dispose();
			}
		});
		btnOpenCashier.setFont(new Font("Tahoma", Font.PLAIN, 13));
		btnOpenCashier.setBounds(74, 190, 284, 23);
		contentPane.add(btnOpenCashier);
		
		JButton btnLogout = new JButton("Logout");
		btnLogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			    main m = new main();
			    ucw.centerWindow(m);
			    m.setVisible(true);
			}
		});
		btnLogout.setBounds(324, 231, 89, 23);
		contentPane.add(btnLogout);
	}
}
