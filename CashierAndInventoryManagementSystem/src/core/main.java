package core;

import java.awt.BorderLayout;

import utility.centerWindow;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.Window;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JPasswordField;

import net.proteanit.sql.DbUtils;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Properties;




import utilities.sql;


public class main extends JFrame {

	private JPanel contentPane;
	private JTextField usernameField;
	private JPasswordField passwordField;

	
	//property file 
	static FileInputStream fileInput;;
	static Properties properties = new Properties();
	static Connection conn;
	
	
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					fileInput = new FileInputStream("resources/config.properties");
					properties.load(fileInput);
					main frame = new main();
					frame.setVisible(true);
					utility.centerWindow cw = new utility.centerWindow();
					cw.centerWindow(frame);
					sql sb= new sql();						
					conn = sb.getDBConnection();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public main() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 555, 298);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel username_label = new JLabel("USERNAME:");
		username_label.setFont(new Font("Tahoma", Font.PLAIN, 34));
		username_label.setBounds(20, 76, 207, 49);
		contentPane.add(username_label);
		
		JLabel password_label = new JLabel("PASSWORD:");
		password_label.setFont(new Font("Tahoma", Font.PLAIN, 34));
		password_label.setBounds(20, 136, 212, 49);
		contentPane.add(password_label);
		
		usernameField = new JTextField();
		usernameField.setFont(new Font("Tahoma", Font.PLAIN, 34));
		usernameField.setBounds(237, 76, 279, 49);
		contentPane.add(usernameField);
		usernameField.setColumns(10);
		
		JButton loginButton = new JButton("Login");
		loginButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String username = usernameField.getText();
				String password = passwordField.getText();
				String query;
				utility.centerWindow ucw = new utility.centerWindow();
				model.setQueryLogin sql = new model.setQueryLogin();
				sql.setUsername(username);
				sql.setPassword(password);
				
				try {
					query =  "SELECT position, name FROM " + properties.getProperty("accountsTable") 
							+ " WHERE username = '" + sql.getUsername() + "' AND pass = '" + sql.getPassword() + "'";					
					sql.setQuery(query);
					PreparedStatement pstmt = conn.prepareStatement(query);					
					ResultSet rs = pstmt.executeQuery();
					
					if (!rs.isBeforeFirst() ) {    //account checker conditional stmt
					    JOptionPane.showMessageDialog(null, "Account does not exist");
					} 
					while(rs.next()){
						sql.setAccount_pos(rs.getString(1));
						sql.setAccount_name(rs.getString(2));							
					}
					//if else statement to decide which jframe to open next
					
					if(sql.getAccount_pos().equals("admin")){
						AdminPage ap = new AdminPage(sql.getUsername(), sql.getPassword());						
						ap.setVisible(true);
						ucw.centerWindow(ap);
						
						dispose();
					}
					else if(sql.getAccount_pos().equals("cashier")){
						CashierPage cp = new CashierPage();	
						cp.setExtendedState(JFrame.MAXIMIZED_BOTH);
						cp.setVisible(true);						
						dispose();
					}

				} catch (Exception e) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(null, e);
				}
				
			}
		});
		loginButton.setFont(new Font("Microsoft PhagsPa", Font.BOLD, 30));
		loginButton.setBounds(161, 196, 207, 49);
		contentPane.add(loginButton);
		
		passwordField = new JPasswordField();
		passwordField.setFont(new Font("Tahoma", Font.PLAIN, 34));
		passwordField.setBounds(237, 136, 279, 49);
		contentPane.add(passwordField);
		
		JLabel label = new JLabel("");
		Image img = new ImageIcon(this.getClass().getResource("/transbg.png")).getImage();
		label.setIcon(new ImageIcon(img));
		label.setBounds(161, 11, 207, 54);
		contentPane.add(label);
	}
}
