package model;

public class setQueryLogin {

	String username;
	String password;
	String query;
	String account_pos;
	String account_name;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getQuery() {
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}
	public String getAccount_name() {
		return account_name;
	}
	public void setAccount_name(String account_name) {
		this.account_name = account_name;
	}
	public String getAccount_pos() {
		return account_pos;
	}
	public void setAccount_pos(String account_pos) {
		this.account_pos = account_pos;
	}
	
	
	
}
