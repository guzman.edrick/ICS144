package model;

public class generateResultsModel {

	String companyName;
	String psicCode;
	String yearStart;
	String yearEnd;
	String query;
	
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getPsicCode() {
		return psicCode;
	}
	public void setPsicCode(String psicCode) {
		this.psicCode = psicCode;
	}
	public String getYearStart() {
		return yearStart;
	}
	public void setYearStart(String yearStart) {
		this.yearStart = yearStart;
	}
	public String getYearEnd() {
		return yearEnd;
	}
	public void setYearEnd(String yearEnd) {
		this.yearEnd = yearEnd;
	}
	public void setQuery(String query){
		this.query = query;
	}
	public String getQuery(){
		return query;
	}
	
	
}
