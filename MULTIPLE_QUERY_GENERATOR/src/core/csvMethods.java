package core;

import java.io.File;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class csvMethods {
	
			private static void generateCsvFile(String sFileName)
			  {
				try
				{
				    FileWriter writer = new FileWriter(sFileName);		  				
				    writer.flush();
				    writer.close();
				}
				catch(IOException e)
				{
				     e.printStackTrace();
				} 
			  }
			
			public static boolean writeData(String data,String strFilePath, File file)
		  {
		      PrintWriter csvWriter;
		      
		      try
		      {
		       
		          if(!file.exists()){
		              file = new File(strFilePath);
		          }
		          csvWriter = new  PrintWriter(new FileWriter(file,true));
		         
		          csvWriter.print(data+"DONE,");
		          csvWriter.append('\n');
		          csvWriter.close();
		      }
		      catch (Exception e)
		      {
		          e.printStackTrace();
		      }
				return true;
		  }
			
			public static boolean headerWriteData(String headerData,String strFilePath, File file)
		  {
		      PrintWriter csvWriter;
		      
		      try
		      {
		       
		          if(!file.exists()){
		              file = new File(strFilePath);
		          }
		          csvWriter = new  PrintWriter(new FileWriter(file,true));
		         
		          csvWriter.print(headerData+"DONE,");
		          csvWriter.append('\n');
		          csvWriter.close();
		      }
		      catch (Exception e)
		      {
		          e.printStackTrace();     
		      }
				return true;
		  }


}
