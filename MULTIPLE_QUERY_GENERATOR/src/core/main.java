package core;

import java.awt.BorderLayout;



import model.queryModel;
import net.proteanit.sql.DbUtils;

import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.jdesktop.swingx.prompt.PromptSupport;

import utility.populateJTable;

import java.awt.EventQueue;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.FlowLayout;

import javax.swing.JMenuBar;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JMenu;
import javax.swing.border.TitledBorder;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Toolkit;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.Timer;
import javax.swing.JFormattedTextField;
import javax.swing.JComboBox;
import javax.swing.ImageIcon;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JPasswordField;
import javax.swing.JTable;

import java.awt.Color;

import javax.swing.border.BevelBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.JScrollPane;

import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class main extends JFrame {

	private JPanel contentPane;
	private JTextField hostname;
	private JTextField port;
	private JTextField dbName;
	private JTextField userName;
	private JTextField queryTextField;
	private JPasswordField pw;
	private Connection conn;
	private JTable table;
	int size = 0;
	private JFrame frame;
	String user, pass, database, portNumber, query, host;		//for dbConnection
	int limitStart, limitEnd = 0; 
	populateJTable pjt = new populateJTable();
	String userQuery;
	String query2; 												//query for getting count
	int count = 0; 												//to be updated during method call
	int startLimitForMethods = 1;							    //moving of pages(one at a time)
	int startLimitForPopulate =0; 								//start limit of query
	double newCount = 0; 										//no use, only to be parsed to toReturn
	int toReturn = 0; 											//limitEnd when value dropdown changed
	Object[][] data = new Object[0][0];
	boolean result;

	/**
	 * Launch the application.
	 */

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					main frame = new main();
					frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
					
					
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public main() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Edrick\\Desktop\\Untitled.png"));
		setTitle("DQL Editor");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		contentPane = new JPanel();
		contentPane.setBorder(null);
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(null, "Information", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_2.setBounds(10, 61, 1346, 126);
		contentPane.add(panel_2);
		panel_2.setLayout(null);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(6, 16, 1326, 39);
		panel_2.add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblDbName = new JLabel("Hostname");
		lblDbName.setBounds(319, 8, 89, 14);
		panel_1.add(lblDbName);
		
		hostname = new JTextField();
		hostname.setText("localhost");
		hostname.setBounds(395, 5, 86, 20);
		hostname.setColumns(10);
		panel_1.add(hostname);
		
		JLabel label_1 = new JLabel("Port");
		label_1.setBounds(485, 8, 31, 14);
		panel_1.add(label_1);
		
		port = new JTextField();
		port.setText("3306");
		port.setBounds(513, 5, 86, 20);
		port.setColumns(10);
		panel_1.add(port);
		
		JLabel lblDbName_1 = new JLabel("DB Name");
		lblDbName_1.setBounds(603, 8, 73, 14);
		panel_1.add(lblDbName_1);
		
		dbName = new JTextField();
		dbName.setText("newdb");
		dbName.setBounds(664, 5, 86, 20);
		dbName.setColumns(10);
		panel_1.add(dbName);
		
		JLabel label_3 = new JLabel("Username");
		label_3.setBounds(752, 8, 67, 14);
		panel_1.add(label_3);
		
		userName = new JTextField();
		userName.setText("root");
		userName.setBounds(817, 5, 86, 20);
		userName.setColumns(10);
		panel_1.add(userName);
		
		JLabel label_4 = new JLabel("Password");
		label_4.setBounds(908, 8, 61, 14);
		panel_1.add(label_4);
		
		pw = new JPasswordField();
		pw.setToolTipText("");
		pw.setBounds(968, 5, 73, 20);
		pw.setText("p@ssword");
		panel_1.add(pw);
		
		
		
		JButton btnNewButton_1 = new JButton("");  
		btnNewButton_1.setIcon(new ImageIcon("C:\\Users\\Edrick\\Desktop\\newWorkspace\\MYSQLEditor\\src\\1.png"));
		btnNewButton_1.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				pw.setEchoChar((char)0);
				Timer t = new Timer(1200, new ActionListener()                  //method for viewing password
				{
					public void actionPerformed(ActionEvent e)
					{
		            	pw.setEchoChar('*');
		            }
				});
				t.start();
			}      
		});
		
		btnNewButton_1.setBounds(1051, 2, 31, 23);
		panel_1.add(btnNewButton_1);
		
		JButton checkConnectionButton = new JButton("Check Connection");
		
		checkConnectionButton.setBounds(1092, 4, 145, 23);
		panel_1.add(checkConnectionButton);
		
		JPanel panel = new JPanel();
		panel.setBounds(16, 66, 1326, 48);
		panel_2.add(panel);
		panel.setLayout(null);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBounds(10, 0, 1346, 62);
		contentPane.add(panel_3);
		panel_3.setLayout(null);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBorder(new TitledBorder(null, "Action", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_4.setBounds(0, 0, 1346, 56);
		panel_3.add(panel_4);
		panel_4.setLayout(null);
		
		JButton btnSettings = new JButton("Settings");
		btnSettings.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				SettingsPage sp = new SettingsPage();
				sp.setVisible(true);
				
			}
		});
		btnSettings.setBounds(10, 24, 89, 23);
		panel_4.add(btnSettings);
		
		JButton btnExportAsCsv = new JButton("Export as CSV");
		btnExportAsCsv.setBounds(97, 24, 180, 23);
		panel_4.add(btnExportAsCsv);
		
		final JLabel checkLabel = new JLabel("You must check connection first");
		checkLabel.setForeground(Color.RED);
		checkLabel.setFont(new Font("Tahoma", Font.BOLD, 13));
		checkLabel.setBounds(536, 28, 247, 28);
		panel_4.add(checkLabel);
		
		queryTextField = new JTextField();
		queryTextField.setFont(new Font("Tahoma", Font.PLAIN, 15));
		queryTextField.setBounds(10, 198, 1348, 78);
		contentPane.add(queryTextField);
		queryTextField.setColumns(10);
		PromptSupport.setPrompt("Input Query here", queryTextField);
		PromptSupport.setFocusBehavior(PromptSupport.FocusBehavior.HIDE_PROMPT, queryTextField);
		PromptSupport.setFontStyle(Font.ITALIC, queryTextField);
		
		final JButton executeQueryButton = new JButton("Execute Query");
		executeQueryButton.setFont(new Font("Tahoma", Font.PLAIN, 25));
		executeQueryButton.setBounds(10, 286, 253, 30);
		contentPane.add(executeQueryButton);
		executeQueryButton.setEnabled(false);
		
		JPanel panel_5 = new JPanel();
		panel_5.setBorder(new TitledBorder(null, null, TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_5.setBounds(273, 287, 1081, 30);
		contentPane.add(panel_5);
		panel_5.setLayout(null);
		
		final JLabel messageLabel = new JLabel("null");
		messageLabel.setBounds(10, 0, 975, 30);
		panel_5.add(messageLabel);
		
		final JPanel bottomPanel = new JPanel();
		bottomPanel.setBounds(10, 327, 1361, 370);
		contentPane.add(bottomPanel);
		bottomPanel.setLayout(null);
		
		JPanel panel_7 = new JPanel();
		panel_7.setBorder(new TitledBorder(null, null, TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_7.setBounds(4, 11, 1340, 54);
		bottomPanel.add(panel_7);
		panel_7.setLayout(null);
		
		JPanel panel_6 = new JPanel();
		panel_6.setBounds(10, 11, 1330, 32);
		panel_7.add(panel_6);
		panel_6.setLayout(null);
		
		
		DefaultTableModel model;
		table = new JTable();
		JTableHeader header = table.getTableHeader();
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setEnabled(false);
		scrollPane.setBounds(14, 76, 1330, 283);
		bottomPanel.add(scrollPane);
		
		
		//extension of check connection button
		checkConnectionButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				String username = userName.getText();
				String password = pw.getText();
				String portNum = port.getText();
				String hostName = hostname.getText();
				String databaseName = dbName.getText();
				try{
					model.DBConnection db = new model.DBConnection();
					db.setDbName(databaseName);
					db.setHostname(hostName);
					db.setPort(portNum);
					db.setPassword(password);
					db.setUserName(username);
					user = db.getUserName();
					pass = db.getPassword();
					portNumber = db.getPort();
					host = db.getHostname();
					database = db.getDbName();
					Class.forName("com.mysql.jdbc.Driver");
					conn = DriverManager.getConnection("jdbc:mysql://" + db.getHostname() +
														":"+ db.getPort() + "/" + db.getDbName(),
														db.getUserName(), db.getPassword());
					JOptionPane.showMessageDialog(null, "Connection Successful");
					executeQueryButton.setEnabled(true);
					
					checkLabel.setVisible(false);
				}catch(Exception e){
					JOptionPane.showMessageDialog(null, e);
				}
			}
		});
		
		//extension of check connection button
		executeQueryButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) {
				 model.queryModel qm = new model.queryModel();
				 methods.methodsForMain mfm = new methods.methodsForMain();
				 ArrayList<String> list_of_queries =  new ArrayList<String>();
				 ArrayList<String> columns_of_queries = new ArrayList<String>();
				 ArrayList<String> contents_of_resultset =new ArrayList<String>();
				 
				 Set<String> hash_set_for_removing_duplicates = new HashSet<>();
				 
				 Map<String, String> map = new HashMap<String, String>();
				 qm.setQuery(queryTextField.getText());
				 userQuery = qm.getQuery();
				 result = mfm.checkStringForSemiColon(userQuery);
				 if(result == true)
				 {
					mfm.separateQueries(userQuery, list_of_queries);
					mfm.getAllColumns(conn, columns_of_queries, list_of_queries);
					mfm.checkListForDuplicates(hash_set_for_removing_duplicates, columns_of_queries);
					Object[] array_for_table_column = new Object[hash_set_for_removing_duplicates.size()];
					mfm.populateColumnArray(columns_of_queries, array_for_table_column);
			        mfm.populateMap(map, array_for_table_column);
			        data = new Object[list_of_queries.size()][columns_of_queries.size()];
			        mfm.populateTableRows(data, list_of_queries, map, array_for_table_column,contents_of_resultset, conn, columns_of_queries);
			        table = new JTable(data, array_for_table_column);
					JScrollPane scrollPane = new JScrollPane(table);
					scrollPane.setEnabled(false);
					scrollPane.setBounds(14, 76, 1330, 283);
					bottomPanel.add(scrollPane);
			        setTitle("Table");
			        //mfm.populateTableRows(data, list_of_queries, columns_of_queries, contents_of_resultset, conn);
				 }
				 else if(result == false){
					 query = userQuery;		 
					 pjt.populateJTablePstmt(conn, query, messageLabel, size, table); 
				 }
				 
				 JOptionPane.showMessageDialog(null, data[0][1]);
				 
			}
		});

		btnExportAsCsv.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0)
			{
			
				String s = (String)JOptionPane.showInputDialog(frame,"Input file name");
				model.DBConnection db = new model.DBConnection();
				
				model.generateResultsModel grm = new model.generateResultsModel();
				String newFileName = s + ".csv";
				File newFile = new File(newFileName);
				
				//If a string was returned, say so.
				if ((s != null) && (s.length() > 0)) 
				{
					if(result == true){
						utility.toCSV.writeToCsvFromTable(s, table,data);
						System.out.println(query);
					   
					}
					else if(result == false)
					{
						utility.toCSV.writeToCsv(s, host, database, portNumber, user, pass, query);
						System.out.println(query);
					    					}
				}

			}
		});
		
		
		
		
		
		
	}
}
