package methods;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.JOptionPane;

public class methodsForMain {
	
	public boolean checkStringForSemiColon(String query)
	{
		boolean result = false;
		 char[] checkSemiColon = query.toCharArray();
		 String output = "";
		 for(int i = 0; i <= checkSemiColon.length-1;)
		 {
			if(checkSemiColon[i] == ';'){
				i++;
				result = true;
			}
			else{	
				i++;
			}
		 }
		 return result;
	}
	
	public void separateQueries(String query, ArrayList<String> list_of_queries)
	{
		 String[] arrayForQueries = query.split(";");
		 for(int i = 0; i <= arrayForQueries.length-1; i++)
		 {
			 list_of_queries.add(arrayForQueries[i]);
		 }
		
	}
	
	public void getAllColumns(Connection conn, ArrayList<String> columns_of_queries, 
								ArrayList<String> list_of_queries)
	{
		Statement stmt = null;
		ResultSet rs = null;
		ResultSetMetaData rsmd = null;
		int i = 0;
		while(i != list_of_queries.size())
		{
			try
			{
				stmt = conn.createStatement();
				rs = stmt.executeQuery(list_of_queries.get(i));
				rsmd = rs.getMetaData();
				int number_of_columns = rsmd.getColumnCount();

				for(int j = 1; j<= number_of_columns;j++)
				
				{
					columns_of_queries.add(rsmd.getColumnName(j));
					
				}
				rs.close();
				stmt.close();
				
			}
			catch (SQLException e) 
			{
				
				e.printStackTrace();
			}
		i++;
		}
	}
	
	public void checkListForDuplicates(Set<String> hash_set_for_removing_duplicates,
									   ArrayList<String> columns_of_queries)
	{
		 hash_set_for_removing_duplicates.addAll(columns_of_queries);
		 columns_of_queries.clear();
		 columns_of_queries.addAll(hash_set_for_removing_duplicates);
	}
	
	public void populateColumnArray(ArrayList<String> columns_of_queries,
									Object[] array_for_table_column)
	{
		for(int j=0; j<=array_for_table_column.length-1; j++){
			 array_for_table_column[j] = columns_of_queries.get(j);
		 }
	}
	
	public void populateTableRows(Object[][] data, ArrayList<String> list_of_queries, Map map, 
			Object[] array_for_table_column,ArrayList<String> contents_of_resultset, Connection conn,ArrayList<String> columns_of_queries){
		
		Statement stmt = null;
		ResultSet rs = null;
		ResultSetMetaData rsmd = null;
		int i = 0;
		int j=1;
		int r =0;
		while(i != list_of_queries.size())
		{
			try {
				stmt = conn.createStatement();
				rs = stmt.executeQuery(list_of_queries.get(i));
				rsmd = rs.getMetaData();
				int number_of_columns = rsmd.getColumnCount();
				while (rs.next())
				{
				     populateMap(map, array_for_table_column);
				     for(j=1; j<=number_of_columns; j++)
				     {           
				      map.put(rsmd.getColumnName(j),rs.getObject(j));				      
				     }
				     Object[] newArray = new Object[columns_of_queries.size()];
				         for (int c=1; c<=map.size(); c++)
				         {
				        	
				        		 newArray[c-1] = (Object) map.get(columns_of_queries.get(c-1));
				        	 
				        	 
				        	 
				         }
				         data[r] = newArray;
				     r++;
				     break;
				      
				 }
				stmt.close();
				rs.close();
			} 
			catch (SQLException e)
			{
				JOptionPane.showMessageDialog(null,e);
				e.printStackTrace();
			}
			i++;
		}	
	}
	
	public void populateMap(Map map, Object[] array_for_table_column)
	{
		
				
					for(int i = 0; i<= array_for_table_column.length-1; i++)
					{
					map.put(array_for_table_column[i], "null");
					}

	}
			

	
}


