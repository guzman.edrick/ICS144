package utility;

import java.sql.Connection;
import java.lang.StringBuilder;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;

import net.proteanit.sql.DbUtils;

public class populateJTable {
	
	public void populateJTablePstmt(Connection conn, String query, JLabel sizeLabel, int size, JTable table)
	{
		

		try {
			PreparedStatement pstmt = conn.prepareStatement(query);					
			ResultSet rs = pstmt.executeQuery();
			ResultSetMetaData rsmd = rs.getMetaData();
			String[] array = pstmt.toString().split(": ");
			
			table.setModel(DbUtils.resultSetToTableModel(rs));
			rs.last();
			size = rs.getRow();
			sizeLabel.setText(size + " row(s) returned");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, e);
		}
		

	}
	
	public void populateJTablePreparedStatement(Connection conn, String query, String setOne, int limitStart,
												int limitEnd, JTable table, JLabel sizeLabel, int size)
	{
	
		try {
			PreparedStatement pstmt = conn.prepareStatement(query);		
			pstmt.setString(1, setOne);
			pstmt.setInt(2, limitStart);
			pstmt.setInt(3, limitEnd);
			ResultSet rs = pstmt.executeQuery();
			JOptionPane.showMessageDialog(null, pstmt);
			String[] array = pstmt.toString().split(": ");
			;
			table.setModel(DbUtils.resultSetToTableModel(rs));
			rs.last();
			size = rs.getRow();
			sizeLabel.setText(size + " row(s) returned");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, e);
		}
		

		
	}
	
	public void populateJTableForElse(Connection conn, JTable table,int limitStart, int limitEnd, 
										JLabel sizeLabel,int size, String yearstart, String yearend, String query)
	{
		
		try {

				PreparedStatement pstmt = conn.prepareStatement(query);	
				pstmt.setString(1, yearstart);
				pstmt.setString(2, yearend);
				pstmt.setInt(3, limitStart);
				pstmt.setInt(4, limitEnd);
				ResultSet rs = pstmt.executeQuery();

				table.setModel(DbUtils.resultSetToTableModel(rs));
				rs.last();
				size = rs.getRow();
				sizeLabel.setText(size + " row(s) returned");
			}
			
			
		catch (SQLException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, e);
		}
		
	}
	
	public int getCount(Connection conn, JLabel label, String query, String find, JLabel from, int startLimit){
		int count = 1;
		try {
			PreparedStatement pstmt = conn.prepareStatement(query);
			pstmt.setString(1, find);		
			ResultSet rs = pstmt.executeQuery();
			JOptionPane.showMessageDialog(null, pstmt.toString());
			while(rs.next()){
				label.setText("of {" + rs.getInt(1) + "}");
				count = rs.getInt(1);
			}
			rs.close();
			if(count == 0) {
				from.setText("{" + 0 + "}");
			}
			else if(count != 0){
				from.setText("{" + startLimit + "}");
			}
			
			
			
		}catch(SQLException e){
			JOptionPane.showMessageDialog(null, e);
		}
		return count;
	}
	
	public int getCount(Connection conn, JLabel label, String query, String start, String end, JLabel from, int startLimit){
		int count = 1;
		try {
			PreparedStatement pstmt = conn.prepareStatement(query);
			pstmt.setString(1, start);		
			pstmt.setString(2, end);
			ResultSet rs = pstmt.executeQuery();			
			while(rs.next()){
				label.setText("of {" + rs.getInt(1) + "}");
				count = rs.getInt(1);
			}
			if(count == 0) {
				from.setText("{" + 0 + "}");
			}
			else if(count != 0){
				from.setText("{" + startLimit + "}");
			}
			
			rs.close();	
		}catch(SQLException e){
			JOptionPane.showMessageDialog(null, e);
		}
		return count;
	}
	
	public int getCountVoidOne_new(Connection conn, JLabel label, JLabel from,  String query, String find, int parseEnd, int count, int startLimit, int toReturn){
		
		
		final int prevCount =count;
		double newCount = 0;
		
		String newQuery = query.concat(" LIMIT " + startLimit + "," + parseEnd);
		
		try {
			PreparedStatement pstmt = conn.prepareStatement(newQuery);
			pstmt.setString(1, find);		
			ResultSet rs = pstmt.executeQuery();
			from.setText("{" + startLimit + "}");
			while(rs.next()){
				label.setText("of {" + rs.getInt(1) + "}");
				count = rs.getInt(1);
			}
			if(count == 0) {
				from.setText("{" + 0 + "}");
			}
			else if(count != 0){
				from.setText("{" + startLimit + "}");
			}
			if(parseEnd  != 1){
				newCount = (count / parseEnd) + .5;
				toReturn  = (int)Math.ceil(newCount);
				label.setText("of {" + toReturn + "}");
			}
			else if(parseEnd == 1){
				newCount = (count / parseEnd);
				label.setText("of {" + prevCount + "}");
			}
			
			rs.close();
			
			
		}catch(SQLException e){
			JOptionPane.showMessageDialog(null, e);
		}
		return toReturn;
	}
	
	

	public void getCountVoid(Connection conn, JLabel label, JLabel from,  String query, String start, int parseEnd, int count, String end, int startLimit){
		
		final int prevCount = count;
		double newCount = 0;
		int toReturn = 0;
		String newQuery = query.concat(" LIMIT " + startLimit + "," + parseEnd);
		try {
			PreparedStatement pstmt = conn.prepareStatement(newQuery);
			pstmt.setString(1, start);	
			pstmt.setString(2, end);
			ResultSet rs = pstmt.executeQuery();
			;
			
			from.setText("{" + startLimit + "}");
			while(rs.next()){
				label.setText("of {" + rs.getInt(1) + "}");
				count = rs.getInt(1);
			}
			if(count == 0) {
				from.setText("{" + 0 + "}");
			}
			else if(count != 0){
				from.setText("{" + startLimit + "}");
			}
			if(parseEnd  != 1){ //no. of pages
				newCount = (count / parseEnd) + .5;
				toReturn  = (int)Math.ceil(newCount);
				label.setText("of {" + toReturn + "}");
			}
			else if(parseEnd == 1){
				newCount = (count / parseEnd);
				label.setText("of {" + prevCount + "}");
			}
			rs.close();
			
			
		}catch(SQLException e){
			JOptionPane.showMessageDialog(null, e);
		}
	
	}
	public void getCountVoid_new(Connection conn, JLabel label, JLabel from,  String query, String start, int parseEnd, int count, String end, int startLimit, int toReturn){
		
		final int prevCount = count;
		double newCount = 0;
		
		String newQuery = query.concat(" LIMIT " + startLimit + "," + parseEnd);
		
		try {
			PreparedStatement pstmt = conn.prepareStatement(newQuery);
			pstmt.setString(1, start);	
			pstmt.setString(2, end);
			ResultSet rs = pstmt.executeQuery();
			;
			
			from.setText("{" + startLimit + "}");
			while(rs.next()){
				label.setText("of {" + rs.getInt(1) + "}");
				count = rs.getInt(1);
			}
			if(count == 0) {
				from.setText("{" + 0 + "}");
			}
			else if(count != 0){
				from.setText("{" + startLimit + "}");
			}
			if(parseEnd  != 1){ //no. of pages
				newCount = (count / parseEnd) + .5;
				toReturn  = (int)Math.ceil(newCount);
				label.setText("of {" + toReturn + "}");
			}
			else if(parseEnd == 1){
				newCount = (count / parseEnd);
				label.setText("of {" + prevCount + "}");
			}
			rs.close();
			
			
		}catch(SQLException e){
			JOptionPane.showMessageDialog(null, e);
		}
	
	}
	
	public int getToReturn(int count, int limitEnd){
		
		int toReturn = 0;
		double newCount = 0;
		if(limitEnd  != 1){ //no. of pages
			newCount = (count / limitEnd) + .5;
			toReturn  = (int)Math.ceil(newCount);
			
		}
		return toReturn;
		
	}

}
