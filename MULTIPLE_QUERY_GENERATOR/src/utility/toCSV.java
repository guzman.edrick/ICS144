package utility;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;

import core.csvMethods;

public class toCSV {

	 public static void writeToCsv(String file,String hostname, String dbname, String port, String username, String password, String query){
		    Connection conn;
			String newFileName = file + ".csv";
			File newFile = new File(newFileName);
			
			try 
			{
				conn = DriverManager.getConnection("jdbc:mysql://" + hostname +":"+ port + "/" + dbname, username, password);
														
				Statement stmt=conn.createStatement();  
				ResultSet rs=stmt.executeQuery(query);
				ResultSetMetaData rsmd = rs.getMetaData();
				int columnsNumber = rsmd.getColumnCount();
				StringBuilder sb = new StringBuilder();
				StringBuilder sbForRecords = new StringBuilder();
				
						for(int i = 1; i<= columnsNumber; i++)
						{
							if(i == columnsNumber){
								sb.append(rsmd.getColumnName(i));
							}
							else{
								sb.append(rsmd.getColumnName(i)+ ",");
							}
						}
						csvMethods.headerWriteData(sb.toString(),newFileName,newFile);
						
						while(rs.next())
						{
								for(int j = 1; j <= columnsNumber; j++){
									if(rsmd.getColumnClassName(j).toString() == "java.lang.Integer"){
									
										if(j == columnsNumber){
											sbForRecords.append("'" + rs.getInt(j) + "'");
										}
										else{
											sbForRecords.append("'" + rs.getInt(j)+ "'" + ",");								
										}
										
									}
									else{
										
										if(j == columnsNumber){
											sbForRecords.append("'" + rs.getString(j) + "'");
										}
										else{
											sbForRecords.append("'" + rs.getString(j)+ "'" + ",");									
										}
									}			
								}
								csvMethods.writeData(sbForRecords.toString(), newFileName, newFile);	
								sbForRecords.delete(0, sbForRecords.length());
						}
				
				JOptionPane.showMessageDialog(null, "CSV File " + newFileName+ " created.");
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		}
	 
	 public void generateCSV(String host, String database, String portNumber, String user, String pass, String query, JFrame frame){
		 String s = (String)JOptionPane.showInputDialog(frame,"Input file name");
			model.DBConnection db = new model.DBConnection();
			model.generateResultsModel grm = new model.generateResultsModel();
			String newFileName = s + ".csv";
			File newFile = new File(newFileName);
			
			//If a string was returned, say so.
			if ((s != null) && (s.length() > 0)) {
				utility.toCSV.writeToCsv(s, host, database, portNumber, user, pass, query);
			    JOptionPane.showMessageDialog(null, s + ".csv file was succeessfully created");			 
			    return;
			}
	 }
	 
	 public static void writeToCsvFromTable(String file, JTable table, Object[][] data){
		    Connection conn;
			String newFileName = file + ".csv";
			File newFile = new File(newFileName);

				int columnsNumber = table.getColumnCount();
				int rowNumber = table.getRowCount();
				StringBuilder sb = new StringBuilder();
				StringBuilder sbForRecords = new StringBuilder();
				
						for(int i = 0; i<= columnsNumber-1; i++)
						{
							String replaced = table.getColumnName(i);
							replaced=replaced.replace(',', ';');
							if(i == columnsNumber){
								sb.append(table.getColumnName(i));
								
							}
							else{
								sb.append(table.getColumnName(i)+ ",");
							}
						}
					
						csvMethods.headerWriteData(sb.toString(),newFileName,newFile);
						
							for(int k = 0;k<= rowNumber-1; k++)
							{
								for(int j = 0; j <= columnsNumber-1; j++)
								{
									
									String replaced = (data[k][j]).toString();
									replaced=replaced.replace(',', ';');
									
										if(j == columnsNumber){
											sbForRecords.append(replaced);
										}
										else{
											sbForRecords.append(replaced + ",");
										}
										
										
										
								}
								
								csvMethods.writeData(sbForRecords.toString(), newFileName, newFile);	
								sbForRecords.delete(0, sbForRecords.length());
						}
				
				JOptionPane.showMessageDialog(null, "CSV File " + newFileName+ " created.");
				
			
		}
}





