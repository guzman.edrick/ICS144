<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<jsp:useBean id="dbRecord" type="java.sql.ResultSet" scope="request" />    
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Employee List</title>
</head>
<body>
	<table border="1">
		<tr>
			<th>ID</th>
			<th>Last Name</th>
			<th>First Name</th>
			<th>Position</th>
			<th>Department</th>
		</tr>
		<%
			while (dbRecord.next()) {
		%>
			<tr>
				<td><%=dbRecord.getInt("id") %></td>
				<td><%=dbRecord.getString("lastName") %></td>
				<td><%=dbRecord.getString("firstName") %></td>
				<td><%=dbRecord.getString("position") %></td>
				<td><%=dbRecord.getString("department") %></td>
			</tr>			
		<%  }%>
	</table>
	
</body>
</html>