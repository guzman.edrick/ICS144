package edu.ust.utility;

import java.beans.XMLEncoder;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;

import edu.ust.model.ConnectionBean;

public class DatabaseUtility {

	public void createXMLDatabaseConfiguration() {
		try {
			ConnectionBean connBean = new ConnectionBean();
			connBean.setDatabase("practice");
			connBean.setDriverClass("com.mysql.jdbc.Driver");
			connBean.setJdbcPartURL("jdbc:mysql://");
			connBean.setPassword("p@ssword");
			connBean.setPort("3306");
			connBean.setServer("localhost"); //or an IP Address if connected to network
			connBean.setUserName("root");
			
			//create an XML configuration file
			XMLEncoder encoder = new XMLEncoder(
                new BufferedOutputStream(
                new FileOutputStream("C:\\Users\\Edrick\\Desktop\\newWorkspace\\db_connection.xml")));
			encoder.writeObject(connBean);
			encoder.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		
	}
	
	 
}
