package edu.ust.controller;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.sql.*;

import edu.ust.utility.DatabaseUtility;
@WebServlet("/index.html")
public class DatabaseControllerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static Connection connection;
	
	@PostConstruct
	public void databaseAdminTask() {
		DatabaseUtility dbUtilityTask = new DatabaseUtility();
		dbUtilityTask.createXMLDatabaseConfiguration();
	}
	
	public void init(ServletConfig config) throws ServletException {
		// TODO This is where we will perform database connection
		try {	
			
			Class.forName("com.mysql.jdbc.Driver");
			String username = "root";
			String password = "p@ssword";
			String url = "jdbc:mysql://localhost:3306/practice"; 
			
			connection = DriverManager.getConnection(url,username,password);
		} catch (SQLException sqle){
			System.out.println("SQLException error occured - " 
				+ sqle.getMessage());
		} catch (ClassNotFoundException cnfe){
			System.out.println("ClassNotFoundException error occured - " 
		        + cnfe.getMessage());
		}	
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {	
			if (connection != null) {
				Statement stmt = connection.createStatement();
				ResultSet rs = stmt.executeQuery("select * from employee");	
				System.out.println("Successful connection");
				
				request.setAttribute("dbRecord", rs);
				
				RequestDispatcher dispatcher = 
						request.getRequestDispatcher("displayrecords.jsp");
				dispatcher.forward(request, response);
			} else {
				response.sendRedirect("error.jsp");
			}
		} catch (SQLException sqle){
			System.out.println("SQLException error occured - " 
				+ sqle.getMessage());
			response.sendRedirect("error.jsp");
		}	
	}

}
