package edu.ust.model;

import java.io.Serializable;

public class ConnectionBean implements Serializable {

	private String driverClass;
	private String userName;
	private String password;
	private String jdbcPartURL;
	private String server;
	private String port;
	private String database;
	
	public String getDriverClass() {
		return driverClass;
	}
	public void setDriverClass(String driverClass) {
		this.driverClass = driverClass;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getJdbcPartURL() {
		return jdbcPartURL;
	}
	public void setJdbcPartURL(String jdbcPartURL) {
		this.jdbcPartURL = jdbcPartURL;
	}
	public String getServer() {
		return server;
	}
	public void setServer(String server) {
		this.server = server;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getDatabase() {
		return database;
	}
	public void setDatabase(String database) {
		this.database = database;
	}
}
