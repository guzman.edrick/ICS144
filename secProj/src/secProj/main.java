package secProj;

import java.io.File;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;

import secProj.logger;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;





public class main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Properties properties = new Properties();
		
		try 
		{	
			FileInputStream fileInput;
			try {
				fileInput = new FileInputStream("resources/config.properties");
				try {
					properties.load(fileInput);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					String ex = e.toString();
					logger.writeLog("ERROR DESCRIPTION: " + ex);
				}
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				String ex = e1.toString();
				logger.writeLog("ERROR DESCRIPTION: " + ex);
			}
			
			try 
			{				
				logger.writeLog("Program started");
				//START OF DECLARATIONS
			
				Class.forName(properties.getProperty("driver"));
				Connection conn = DriverManager.getConnection(properties.getProperty("URL")+
															properties.getProperty("database"),
															properties.getProperty("username"),
															properties.getProperty("password"));
				Statement stmtForDateEffective = conn.createStatement();
				Statement stmtForDateRegistered= conn.createStatement();
				Statement insertStatement =conn.createStatement();
				ResultSet rs1,rs2;
				ArrayList<String> dateEffective = new ArrayList<String>();
				ArrayList<Date> dateEffectiveParsed = new ArrayList<Date>();
				ArrayList<String> dateRegistered = new ArrayList<String>();
				ArrayList<Date> dateRegisteredParsed = new ArrayList<Date>();
				ArrayList<String> difference = new ArrayList<String>();
				int y = 0;							
				rs1 = stmtForDateEffective.executeQuery("SELECT DATEEFFECTIVE FROM " + properties.getProperty("tableName"));
				rs2 = stmtForDateRegistered.executeQuery("SELECT DATEREGISTERED FROM " + properties.getProperty("tableName"));
				// END OF DECLARATION
				
				
				
				while(rs1.next())								//get DATEEFFECTIVE from db
				{ 
					try
					{
						dateEffective.add(rs1.getString(1));
					}
					catch(Exception ex)
					{
						dateEffective.add("");
					}
					
					
				}
				logger.writeLog("DATEEFFECTIVE obtained from DB succcessfully");
				while(rs2.next())								 //get DATEEREGISTERED from db
				{
					try
					{
						dateRegistered.add(rs1.getString(1));
					}
					catch(Exception ex)
					{
						dateRegistered.add("");
					}
					
					//dateRegistered.add(rs2.getString(1));
				}
				logger.writeLog("DATEREGISTERED obtained from DB succcessfully");
				for(int i = 0; i < dateEffective.size(); i++)	// to obtain all values from DATEREGISTERED and DATEEFFECTIVE
				{ 
					System.out.println(i);
					String date = dateEffective.get(i);
					String date2 = dateRegistered.get(i);
					Date d;
					Date d1;
					
						try 
						{
							System.out.println(i);
							if((date == null || date == "") || (date2 == null || date2 == "")){
								dateEffectiveParsed.add(null);
								dateRegisteredParsed.add(null);
								
							}
							else if((date != null || date != "") && (date2 != null || date2 != "")){
								d = new SimpleDateFormat("M/dd/yyyy").parse(date);
								d1 = new SimpleDateFormat("M/dd/yyyy").parse(date2);
								dateEffectiveParsed.add(d);
								dateRegisteredParsed.add(d1);
							}
						} catch (ParseException e) 
						{	
							dateEffectiveParsed.add(null);
							dateRegisteredParsed.add(null);
							e.printStackTrace();
							String ex = e.toString();
							logger.writeLog("ERROR DESCRIPTION: " + ex);	
						}
				}
				logger.writeLog("Data from DATEEFFECTIVE and DATEREGISTERED were parsed to Date format successfully");
				
//				logger lg = new logger("log.log");
//				lg.writeLog("", "");
//				lg.thistry("", "");
//				lg.locationlog = "";
//				logger.writeLog("");
//				lg.writeLog("", "");
				
				for(int j = 0; j < dateEffective.size(); j++)	// get the difference of all the dates
				{ 
					Date endDate = dateEffectiveParsed.get(j);
					Date startDate = dateRegisteredParsed.get(j);
					
						try
						{	
							if((endDate == null) || (startDate == null)){
								difference.add(null);
		
							}
							else if((endDate != null) || (startDate != null)){								
							long diff = Math.abs(endDate.getTime() - startDate.getTime());
							long diffDays = diff / (24 * 60 * 60 * 1000);
							difference.add(String.valueOf(diffDays));
							}
						}
						catch(Exception e)
						{
							System.out.println(e);
							String ex = e.toString();
							logger.writeLog("ERROR DESCRIPTION: " + ex);	
						}		
				}
				logger.writeLog("Number of days in between DATEEFFECTIVE and DATEREGISTERED were successfully computed");
				
				
				//int r = 0;
				while(y != difference.size())					//maglalagay ng effectivity age
				{ 
					System.out.println("Updating line = " + y);
					insertStatement.executeUpdate("UPDATE " + properties.getProperty("tableName") + " SET EFFECTIVITYAGE = '" + 
													difference.get(y)+ "' WHERE DATEEFFECTIVE = '"
												  + dateEffective.get(y) + "' AND DATEREGISTERED = '"
													+ dateRegistered.get(y) + "'");					
					y++;
				}
				
				
				System.out.println("DONE");
				logger.writeLog("Effectivityage values were successfully updated");
				
			} catch (ClassNotFoundException e) 
			{
				e.printStackTrace();
				String ex = e.toString();
				logger.writeLog("ERROR DESCRIPTION: " + ex);	
			}			
		} 
		catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			String ex = e.toString();
			logger.writeLog("ERROR DESCRIPTION: " + ex);	
		}	
	}

}
