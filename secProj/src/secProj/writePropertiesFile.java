package secProj;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class writePropertiesFile {
	public static void main(String[] args) {
		try {
			Properties properties = new Properties();
			properties.setProperty("driver", "com.mysql.jdbc.Driver");
			properties.setProperty("username", "root");
			properties.setProperty("password", "");
			properties.setProperty("URL", "jdbc:mysql://192.168.1.50:3306/");
			properties.setProperty("database", "SEC_Migration_DEV");
			properties.setProperty("tableName", "TBL_V2_PROFILE");

			File file = new File("resources/config.properties");
			FileOutputStream fileOut = new FileOutputStream(file);
			properties.store(fileOut, "Property:");
			fileOut.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}

