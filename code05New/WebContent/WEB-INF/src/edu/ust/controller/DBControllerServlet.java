package edu.ust.controller;



import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.IOException;



import javax.annotation.PostConstruct;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.sql.*;
import java.io.*;

import edu.ust.utility.Security;
import edu.ust.model.ConnectionBean;

@WebServlet(urlPatterns = "/index.html", name = "DBControllerServlet")
public class DBControllerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public Connection connection;
	//private String password;
	private ConnectionBean connBean;
	
	@PostConstruct
	public void readXMLDatabaseConfiguration() {
		//ConnectionBean connBean = new ConnectionBean();
		connBean.setUserName("root");
		connBean.setPassword("p@ssword");
		connBean.setPort("3036");
		connBean.setDatabase("practice");
		connBean.setServer("localhost");
		connBean.setDriverName("com.mysql.jdbc.Driver");
		
		try {
			XMLEncoder encoder = new XMLEncoder(
					new BufferedOutputStream(
		            new FileOutputStream("C:\\Users/Edrick/Desktop/newWorkspace/db_connection.xml")));
			encoder.writeObject(connBean);
			encoder.close();
		} catch (Exception e) {
		}
		
		try {
			XMLDecoder decoder = new XMLDecoder(
	                new BufferedInputStream(
	                    new FileInputStream("C:\\Users/Edrick/Desktop/newWorkspace/db_connection.xml")));
			 connBean = (ConnectionBean) decoder.readObject();
			decoder.close();
		} catch (Exception e) {
		}	
	}
	
	public void init(ServletConfig config) throws ServletException {
		if (connBean == null) {
			System.err.println("connBean is NULL");
		}
		
		try {	
			Class.forName(connBean.getDriverName());
			String url = 
			  "jdbc:mysql://" + 
			  connBean.getServer() + ":" +connBean.getPort() + "/" + connBean.getDatabase(); 
			
			connection = 
			  DriverManager.getConnection(url,connBean.getUserName(),connBean.getPassword());
		} catch (ClassNotFoundException cnfe) {
			System.err.println(cnfe.getMessage());
			System.out.println("ClassNotFoundException occured");
		} catch (SQLException sqle) {
			System.err.println(sqle.getMessage());
			System.out.println("SQLException occured");
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {	
			if (connection != null) {
				Statement stmt = connection.createStatement();
				ResultSet rs = stmt.executeQuery("select * from employee");	
				System.out.println("Successful connection");
				//stmt.close();
				//connection.close();
				
				//perform binding to HttpSession
				HttpSession session = request.getSession();
				session.setAttribute("records", rs);
				
				RequestDispatcher dispatcher = 
					request.getRequestDispatcher("displayrecords.jsp");
				dispatcher.forward(request, response);
			} else {
				System.err.println("Connection is NULL.");
			}
		} catch (SQLException sqle){
			System.out.println("SQLException error occured - " 
				+ sqle.getMessage());
		}	
	}
}
