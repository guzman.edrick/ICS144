package model;

public class mainVariablesModel {
	//1st Step = find what needs to be replaced by taylors (ex. e^(x^2))
	//2nd Step = get the parameter of that term (ex. x^2)
	//3rd Step = obtain the translated taylors numerator
	//4th Step = get converted numerator
	//5th Step = eliminate factorial
	//6th Step = distribute operators
	//7th Step = subtract what needs to be subtracted
	
	double limit,answer;
	
	String numerator, denominator, polynomial;
	int highestDegree;
	String firstStep,secondStep,thirdStep,fourthStep,fifthStep,sixthStep,seventhStep;
	
	
	public double getAnswer() {
		return answer;
	}
	public void setAnswer(double answer) {
		this.answer = answer;
	}
	public String getFifthStep() {
		return fifthStep;
	}
	public String getSixthStep() {
		return sixthStep;
	}
	public void setSixthStep(String sixthStep) {
		this.sixthStep = sixthStep;
	}
	public void setFifthStep(String fifthStep) {
		this.fifthStep = fifthStep;
	}
	public String getFirstStep() {
		return firstStep;
	}
	public void setFirstStep(String firstStep) {
		this.firstStep = firstStep;
	}
	public String getSecondStep() {
		return secondStep;
	}
	public String getSeventhStep() {
		return seventhStep;
	}
	public void setSeventhStep(String seventhStep) {
		this.seventhStep = seventhStep;
	}
	public void setSecondStep(String secondStep) {
		this.secondStep = secondStep;
	}
	public String getThirdStep() {
		return thirdStep;
	}
	public void setThirdStep(String thirdStep) {
		this.thirdStep = thirdStep;
	}
	public String getFourthStep() {
		return fourthStep;
	}
	public void setFourthStep(String fourthStep) {
		this.fourthStep = fourthStep;
	}
	public int getHighestDegree() {
		return highestDegree;
	}
	public void setHighestDegree(int highestDegree) {
		this.highestDegree = highestDegree;
	}
	public double getLimit() {
		return limit;
	}
	public void setLimit(double limit) {
		this.limit = limit;
	}
	public String getNumerator() {
		return numerator;
	}
	public void setNumerator(String numerator) {
		this.numerator = numerator;
	}
	public String getDenominator() {
		return denominator;
	}
	public void setDenominator(String denominator) {
		this.denominator = denominator;
	}
	public String getPolynomial() {
		return polynomial;
	}
	public void setPolynomial(String polynomial) {
		this.polynomial = polynomial;
	}
	
}
