package model;

public class DBConnection {
	String userName;
    String pw;
    String dbName;
    String hostname;
	String port;
	
	public void setUserName(String userName){
		this.userName = userName;
	}
	public void setPassword(String pw){
		this.pw = pw;
	}
	
	public String getPassword(){
		return pw;
	}
	public String getUserName(){
		return userName;
	}
	public String getDbName() {
		return dbName;
	}
	public void setDbName(String dbName) {
		this.dbName = dbName;
	}
	public String getHostname() {
		return hostname;
	}
	public  void setHostname(String hostname) {
		this.hostname = hostname;
	}
	
	public void setPort(String port) {
		this.port = port;
	}
	public String getPort() {
		return port;
	}
	
}
