//package methods;
//
//import java.sql.Connection;
//
//import javax.swing.JButton;
//import javax.swing.JComboBox;
//import javax.swing.JLabel;
//import javax.swing.JTable;
//import javax.swing.JTextField;
//
//import utilities.sql;
//
//public class methodsForCore {
//	model.generateResultsModel grm = new model.generateResultsModel();
//	sql sb  = new sql();
//	utility.populateJTable pjt = new utility.populateJTable();
//	
//	
//	public void methodForGenerateResults(Connection conn,String option_selected_item, JTextField change, int ending_limit, 
//										int last_page_label_value, JLabel endPage, JLabel startPage, int starting_limit, JTable table, JLabel messageLabel,
//										int size, JComboBox yearStart, JComboBox yearEnd, JComboBox limit, JButton next, JButton maxNext)
//	{	
//		String query_for_populate;
//		String query_for_get_count;
//			if(option_selected_item.equals("Company Name"))
//			{
//				String cName = change.getText();
//				grm.setCompanyName(cName);	
//				grm.setQuery("SELECT * FROM mysqleditor_table WHERE COMPANYNAME = ? LIMIT ?,?");
//				query_for_populate = grm.getQuery();
//				query_for_get_count = "SELECT COUNT(*) FROM mysqleditor_table WHERE COMPANYNAME = ?";
//				last_page_label_value = pjt.getCount(conn, endPage, query_for_get_count, cName, startPage,starting_limit);
//				query_for_populate = pjt.populateJTablePreparedStatement(conn, query_for_populate, cName, starting_limit, ending_limit, table, messageLabel, size);
//			}
//			else if(option_selected_item.equals("PSIC Code"))
//			{
//				String psicCode = change.getText();
//				grm.setPsicCode(psicCode);			
//				grm.setQuery("SELECT * FROM mysqleditor_table WHERE PSICCODE = ?");
//				query_for_get_count = "SELECT COUNT(*) FROM mysqleditor_table WHERE PSICCODE = ?";
//				last_page_label_value = pjt.getCount(conn, endPage, query_for_get_count, psicCode, startPage,starting_limit);
//				query_for_populate = grm.getQuery();
//				query_for_populate = pjt.populateJTablePreparedStatement(conn, query_for_populate, psicCode, starting_limit, ending_limit, table, messageLabel, size);
//			}
//			else if(option_selected_item.equals("Year of Date Registered"))
//			{
//				String yearstart = yearStart.getSelectedItem().toString();
//				String yearend = yearEnd.getSelectedItem().toString();				
//				grm.setYearStart(yearstart);	
//				grm.setYearEnd(yearend);
//				yearstart= grm.getYearStart();
//				yearend =grm.getYearEnd();
//				
//					if(yearEnd.getSelectedItem().toString().equals(""))
//					{
//						grm.setQuery("SELECT * FROM mysqleditor_table WHERE EXTRACT(year from DATEREGISTERED) = ? LIMIT ?,?");									 
//						query_for_populate = grm.getQuery();							
//						query_for_get_count = "SELECT COUNT(*) FROM mysqleditor_table WHERE EXTRACT(year from DATEREGISTERED) = ?";
//						last_page_label_value = pjt.getCount(conn, endPage, query_for_get_count, yearstart, startPage,starting_limit);
//						query_for_populate = pjt.populateJTablePreparedStatement(conn, query_for_populate, yearstart, starting_limit, ending_limit, table, messageLabel, size);
//					}
//					else {
//						grm.setQuery("SELECT * FROM mysqleditor_table WHERE EXTRACT(year from DATEREGISTERED) >= ? AND EXTRACT(year from DATEREGISTERED) <= ? LIMIT ?, ?" );
//						query_for_populate = grm.getQuery();
//						query_for_get_count = "SELECT COUNT(*) FROM mysqleditor_table WHERE EXTRACT(year from DATEREGISTERED) >= ? AND EXTRACT(year from DATEREGISTERED) <= ?";
//						last_page_label_value = pjt.getCount(conn, endPage, query_for_get_count, yearstart, yearend, startPage,starting_limit);
//						query_for_populate = pjt.populateJTableForElse(conn, table, starting_limit, ending_limit, messageLabel, size, yearstart, yearend, query_for_populate);
//					}
//					
//					
//				
//			}
//		
//		
//				else if(option_selected_item.equals("Year of Date Effective"))
//				{
//					
//					String yearstart = yearStart.getSelectedItem().toString();
//					String yearend = yearEnd.getSelectedItem().toString();
//					
//					grm.setYearStart(yearstart);	
//					grm.setYearEnd(yearend);
//					if(yearEnd.getSelectedItem().toString().equals(""))
//					{
//						grm.setQuery("SELECT * FROM mysqleditor_table WHERE EXTRACT(year from DATEEFFECTIVE) = ? LIMIT ?,?");					
//						query_for_populate = grm.getQuery();
//						query_for_get_count = "SELECT COUNT(*) FROM mysqleditor_table WHERE EXTRACT(year from DATEEFFECTIVE) = ?";
//						last_page_label_value = pjt.getCount(conn, endPage, query_for_get_count, yearstart,startPage,starting_limit);
//						query_for_populate = pjt.populateJTablePreparedStatement(conn, query_for_populate, yearstart, starting_limit, ending_limit, table, messageLabel, size);
//					}
//					else 
//					{
//						grm.setQuery("SELECT * FROM mysqleditor_table WHERE EXTRACT(year from DATEEFFECTIVE) >= ? AND EXTRACT(year from DATEEFFECTIVE) <= ? LIMIT ?, ?" );	
//						query_for_get_count = "SELECT COUNT(*) FROM mysqleditor_table WHERE EXTRACT(year from DATEEFFECTIVE) >= ? AND EXTRACT(year from DATEREGISTERED) <= ?";
//						last_page_label_value = pjt.getCount(conn, endPage, query_for_get_count, yearstart, yearend,startPage, starting_limit);
//						query_for_populate = grm.getQuery();
//						query_for_populate = pjt.populateJTableForElse(conn, table, starting_limit, ending_limit, messageLabel, size, yearstart, yearend, query_for_populate);
//					}
//				}
//				limit.setSelectedIndex(0);
//				maxNext.setEnabled(true);
//				next.setEnabled(true);
//					
//			
//			}
//
//}
