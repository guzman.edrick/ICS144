package core;

import java.awt.BorderLayout;
import core.main;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.border.TitledBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class SettingsPage extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JPasswordField passwordField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JPasswordField passwordField_1;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SettingsPage frame = new SettingsPage();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SettingsPage() {
		
		setTitle("Setting");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblUser = new JLabel("User");
		lblUser.setBounds(33, 28, 59, 14);
		contentPane.add(lblUser);
		
		JLabel lblNewLabel = new JLabel("Password");
		lblNewLabel.setBounds(10, 55, 60, 14);
		contentPane.add(lblNewLabel);
		
		textField = new JTextField();
		textField.setBounds(86, 25, 148, 17);
		contentPane.add(textField);
		textField.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(86, 53, 103, 20);
		contentPane.add(passwordField);
		
		JButton btnNewButton = new JButton("");
		btnNewButton.setIcon(new ImageIcon("C:\\Users\\Edrick\\Desktop\\newWorkspace\\DQLEDITOR\\src\\1.png"));
		btnNewButton.setBounds(197, 51, 38, 23);
		contentPane.add(btnNewButton);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "Documentum Server", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.setBounds(14, 89, 403, 116);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(6, 16, 391, 93);
		panel_1.add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel_1 = new JLabel("Address");
		lblNewLabel_1.setBounds(22, 27, 64, 14);
		panel.add(lblNewLabel_1);
		
		JLabel lblRepository = new JLabel("Repository");
		lblRepository.setBounds(22, 54, 76, 14);
		panel.add(lblRepository);
		
		textField_1 = new JTextField();
		textField_1.setBounds(96, 24, 142, 17);
		panel.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(96, 51, 142, 17);
		panel.add(textField_2);
		
		JLabel lblPort = new JLabel("Port");
		lblPort.setBounds(248, 27, 49, 14);
		panel.add(lblPort);
		
		passwordField_1 = new JPasswordField();
		passwordField_1.setBounds(280, 24, 70, 17);
		panel.add(passwordField_1);
		
		final JButton btnSave = new JButton("Save");
		btnSave.setBounds(33, 216, 89, 23);
		contentPane.add(btnSave);
		btnSave.setEnabled(false);
		
		JButton btnNewButton_1 = new JButton("OK");
		btnNewButton_1.setBounds(195, 216, 89, 23);
		contentPane.add(btnNewButton_1);
		
	
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				dispose();
			}
		});
		btnCancel.setBounds(294, 216, 89, 23);
		contentPane.add(btnCancel);
	}

}
