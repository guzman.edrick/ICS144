package core;

import java.awt.BorderLayout;

import model.queryModel;
import net.proteanit.sql.DbUtils;

import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.jdesktop.swingx.prompt.PromptSupport;

import utility.populateJTable;

import java.awt.EventQueue;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.FlowLayout;

import javax.swing.JMenuBar;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JMenu;
import javax.swing.border.TitledBorder;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import javax.swing.Timer;
import javax.swing.JFormattedTextField;
import javax.swing.JComboBox;
import javax.swing.ImageIcon;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JPasswordField;
import javax.swing.JTable;

import java.awt.Color;

import javax.swing.border.BevelBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.JScrollPane;

import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class main extends JFrame {

	private JPanel contentPane;
	private JTextField hostname;
	private JTextField port;
	private JTextField dbName;
	private JTextField userName;
	private JTextField queryTextField;
	private JTextField change;
	private JPasswordField pw;
	private Connection conn;
	private JTable table;
	int size = 0;
	private JFrame frame;
	String user, pass, database, portNumber, query, host;		//for dbConnection
	int limitStart, limitEnd = 0; 
	populateJTable pjt = new populateJTable();
	String userQuery;								
	int startLimitForMethods = 1;
	model.generateResultsModel grm = new model.generateResultsModel();
	model.queryModel qm = new model.queryModel();
	static Properties properties = new Properties();
	
	/**
	 * Launch the application.
	 */

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					main frame = new main();
					frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
					
					
					
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public main() {
		FileInputStream fileInput;

		try {
			try {
				fileInput = new FileInputStream("resources/config.properties");
				properties.load(fileInput);
			} catch (FileNotFoundException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
		}catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Edrick\\Desktop\\Untitled.png"));
		setTitle("DQL Editor");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		contentPane = new JPanel();
		contentPane.setBorder(null);
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(null, "Information", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_2.setBounds(10, 61, 1346, 126);
		contentPane.add(panel_2);
		panel_2.setLayout(null);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(6, 16, 1326, 39);
		panel_2.add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblDbName = new JLabel("Hostname");
		lblDbName.setBounds(319, 8, 89, 14);
		panel_1.add(lblDbName);
		
		hostname = new JTextField();
		hostname.setText(properties.getProperty("hostname"));
		hostname.setBounds(395, 5, 86, 20);
		hostname.setColumns(10);
		panel_1.add(hostname);
		
		JLabel label_1 = new JLabel("Port");
		label_1.setBounds(485, 8, 31, 14);
		panel_1.add(label_1);
		
		port = new JTextField();
		port.setText(properties.getProperty("port"));
		port.setBounds(513, 5, 86, 20);
		port.setColumns(10);
		panel_1.add(port);
		
		JLabel lblDbName_1 = new JLabel("DB Name");
		lblDbName_1.setBounds(603, 8, 73, 14);
		panel_1.add(lblDbName_1);
		
		dbName = new JTextField();
		dbName.setText(properties.getProperty("database"));
		dbName.setBounds(664, 5, 86, 20);
		dbName.setColumns(10);
		panel_1.add(dbName);
		
		JLabel label_3 = new JLabel("Username");
		label_3.setBounds(752, 8, 67, 14);
		panel_1.add(label_3);
		
		userName = new JTextField();
		userName.setText(properties.getProperty("username"));
		userName.setBounds(817, 5, 86, 20);
		userName.setColumns(10);
		panel_1.add(userName);
		
		JLabel label_4 = new JLabel("Password");
		label_4.setBounds(908, 8, 61, 14);
		panel_1.add(label_4);
		
		pw = new JPasswordField();
		pw.setToolTipText("");
		pw.setBounds(968, 5, 73, 20);
		pw.setText(properties.getProperty("password"));
		panel_1.add(pw);
		
		JButton btnNewButton_1 = new JButton("");  
		btnNewButton_1.setIcon(new ImageIcon("C:\\Users\\Edrick\\Desktop\\newWorkspace\\MYSQLEditor\\src\\1.png"));
		btnNewButton_1.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				pw.setEchoChar((char)0);
				Timer t = new Timer(1200, new ActionListener()                  //method for viewing password
				{
					public void actionPerformed(ActionEvent e)
					{
		            	pw.setEchoChar('*');
		            }
				});
				t.start();
			}      
		});
		
		btnNewButton_1.setBounds(1051, 2, 31, 23);
		panel_1.add(btnNewButton_1);
		
		JButton checkConnectionButton = new JButton("Check Connection");
		
		checkConnectionButton.setBounds(1092, 4, 145, 23);
		panel_1.add(checkConnectionButton);
		
		JPanel panel = new JPanel();
		panel.setBounds(16, 66, 1326, 48);
		panel_2.add(panel);
		panel.setLayout(null);
		
		change = new JTextField();
		change.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
			}
		});
		change.setColumns(10);
		change.setBounds(500, 11, 473, 28);
		panel.add(change);
		

		
		final JComboBox yearStart = new JComboBox();
		yearStart.setModel(new DefaultComboBoxModel(new String[] {"", "2016", "2015", "2014", "2013", "2012", "2011", "2010", "2009", "2008", "2007", "2006", "2005", "2004", "2003", "2002", "2001", "2000", "1999", "1998", "1997", "1996", "1995", "1994", "1993", "1992", "1991", "1990", "1989", "1988", "1987", "1986", "1985", "1984", "1983", "1982", "1981", "1980", "1979", "1978", "1977", "1976", "1975", "1974", "1973", "1972", "1971", "1970", "1969", "1968", "1967", "1966", "1965", "1964", "1963", "1962", "1961", "1960", "1959", "1958", "1957", "1956", "1955", "1954", "1953", "1952", "1951", "1950", "1949", "1948", "1947", "1946", "1945", "1944", "1943", "1942", "1941", "1940", "1939", "1938", "1937", "1936", "1935", "1934", "1933", "1932", "1931", "1930", "1929", "1928", "1927", "1926", "1925", "1924", "1923", "1922", "1921", "1920", "1919", "1918", "1917", "1916", "1915", "1914", "1913", "1912", "1911", "1910", "1909", "1908", "1907", "1906", "1905", "1904", "1903", "1902", "1901", "1900"}));
		yearStart.setBounds(570, 11, 138, 28);
		yearStart.setVisible(false);
		panel.add(yearStart);
		
		
		
		final JButton generateResultsButton = new JButton("Generate Results");
		generateResultsButton.setBounds(990, 11, 326, 26);
		panel.add(generateResultsButton);
		generateResultsButton.setEnabled(false);
		
		final JComboBox yearEnd = new JComboBox();
		yearEnd.setModel(new DefaultComboBoxModel(new String[] {"", "2016", "2015", "2014", "2013", "2012", "2011", "2010", "2009", "2008", "2007", "2006", "2005", "2004", "2003", "2002", "2001", "2000", "1999", "1998", "1997", "1996", "1995", "1994", "1993", "1992", "1991", "1990", "1989", "1988", "1987", "1986", "1985", "1984", "1983", "1982", "1981", "1980", "1979", "1978", "1977", "1976", "1975", "1974", "1973", "1972", "1971", "1970", "1969", "1968", "1967", "1966", "1965", "1964", "1963", "1962", "1961", "1960", "1959", "1958", "1957", "1956", "1955", "1954", "1953", "1952", "1951", "1950", "1949", "1948", "1947", "1946", "1945", "1944", "1943", "1942", "1941", "1940", "1939", "1938", "1937", "1936", "1935", "1934", "1933", "1932", "1931", "1930", "1929", "1928", "1927", "1926", "1925", "1924", "1923", "1922", "1921", "1920", "1919", "1918", "1917", "1916", "1915", "1914", "1913", "1912", "1911", "1910", "1909", "1908", "1907", "1906", "1905", "1904", "1903", "1902", "1901", "1900"}));
		yearEnd.setBounds(796, 11, 138, 28);
		yearEnd.setVisible(false);
		panel.add(yearEnd);
		
		final JLabel yearFromLabel = new JLabel("From:");
		yearFromLabel.setBounds(500, 18, 46, 14);
		yearFromLabel.setVisible(false);
		panel.add(yearFromLabel);
		
		final JLabel yearToLabel = new JLabel("To:");
		yearToLabel.setBounds(740, 18, 46, 14);
		yearToLabel.setVisible(false);
		panel.add(yearToLabel);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBounds(10, 0, 1346, 62);
		contentPane.add(panel_3);
		panel_3.setLayout(null);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBorder(new TitledBorder(null, "Action", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_4.setBounds(0, 0, 1346, 56);
		panel_3.add(panel_4);
		panel_4.setLayout(null);
		
		JButton btnSettings = new JButton("Settings");
		btnSettings.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				SettingsPage sp = new SettingsPage();
				sp.setVisible(true);
				
			}
		});
		btnSettings.setBounds(10, 24, 89, 23);
		panel_4.add(btnSettings);
		
		JButton btnExportAsCsv = new JButton("Export as CSV");
		btnExportAsCsv.setBounds(97, 24, 180, 23);
		panel_4.add(btnExportAsCsv);
		
		final JLabel checkLabel = new JLabel("You must check connection first");
		checkLabel.setForeground(Color.RED);
		checkLabel.setFont(new Font("Tahoma", Font.BOLD, 13));
		checkLabel.setBounds(536, 28, 247, 28);
		panel_4.add(checkLabel);
		
		queryTextField = new JTextField();
		queryTextField.setFont(new Font("Tahoma", Font.PLAIN, 15));
		queryTextField.setBounds(10, 198, 1348, 78);
		contentPane.add(queryTextField);
		queryTextField.setColumns(10);
		PromptSupport.setPrompt("Input Query here", queryTextField);
		PromptSupport.setFocusBehavior(PromptSupport.FocusBehavior.HIDE_PROMPT, queryTextField);
		PromptSupport.setFontStyle(Font.ITALIC, queryTextField);
		
		
		
		
		final JComboBox options = new JComboBox();
		options.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(options.getSelectedItem().toString().equals("Year of Date Registered") ||
					     options.getSelectedItem().toString().equals("Year of Date Effective")){
					change.setVisible(false);
					yearStart.setVisible(true);
					yearEnd.setVisible(true);
					yearFromLabel.setVisible(true);
					yearToLabel.setVisible(true);
					
					
				}
				else{
					change.setVisible(true);
					yearStart.setVisible(false);
					yearEnd.setVisible(false);
					yearFromLabel.setVisible(false);
					yearToLabel.setVisible(false);
				}
			}
		});
		options.setModel(new DefaultComboBoxModel(new String[] {"(Select option here)", "Company Name", "Year of Date Registered", "Year of Date Effective", "PSIC Code"}));
		options.setBounds(10, 11, 473, 28);
		panel.add(options);
		
		//extension of options combobox for generate results
		options.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				if(options.getSelectedItem().toString().equals("Company Name")) 					     
				{
					PromptSupport.setPrompt("Input company name here", change);
					PromptSupport.setFocusBehavior(PromptSupport.FocusBehavior.HIDE_PROMPT, change);
					PromptSupport.setFontStyle(Font.ITALIC, change);
				}
				else if(options.getSelectedItem().toString().equals("PSIC Code"))
				{
					PromptSupport.setPrompt("Input PSIC Code here", change);
					PromptSupport.setFocusBehavior(PromptSupport.FocusBehavior.HIDE_PROMPT, change);
					PromptSupport.setFontStyle(Font.ITALIC, change);
				}
				else if(options.getSelectedItem().toString().equals("(Select option here)")){
					PromptSupport.setPrompt("", change);
				}
			}
		});
		
		final JButton executeQueryButton = new JButton("Execute Query");
		executeQueryButton.setFont(new Font("Tahoma", Font.PLAIN, 25));
		executeQueryButton.setBounds(10, 286, 253, 30);
		contentPane.add(executeQueryButton);
		executeQueryButton.setEnabled(false);
		
		JPanel panel_5 = new JPanel();
		panel_5.setBorder(new TitledBorder(null, null, TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_5.setBounds(273, 287, 1081, 30);
		contentPane.add(panel_5);
		panel_5.setLayout(null);
		
		final JLabel messageLabel = new JLabel("null");
		messageLabel.setBounds(10, 0, 975, 30);
		panel_5.add(messageLabel);
		
		JPanel bottomPanel = new JPanel();
		bottomPanel.setBounds(10, 327, 1361, 370);
		contentPane.add(bottomPanel);
		bottomPanel.setLayout(null);
		
		JPanel panel_7 = new JPanel();
		panel_7.setBorder(new TitledBorder(null, null, TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_7.setBounds(4, 11, 1340, 54);
		bottomPanel.add(panel_7);
		panel_7.setLayout(null);
		
		JPanel panel_6 = new JPanel();
		panel_6.setBounds(10, 11, 1330, 32);
		panel_7.add(panel_6);
		panel_6.setLayout(null);
		
		JLabel lblItems = new JLabel("Items");
		lblItems.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblItems.setBounds(375, 0, 46, 30);
		panel_6.add(lblItems);
		
		final JComboBox limit = new JComboBox();
		
		limit.setModel(new DefaultComboBoxModel(new String[] {"1", "10", "100", "1000", "All"}));
		limit.setBounds(431, 0, 87, 30);
		panel_6.add(limit);
		
		JLabel lblOutputTo = new JLabel("Output to");
		lblOutputTo.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblOutputTo.setBounds(528, 0, 87, 30);
		panel_6.add(lblOutputTo);
		
		JComboBox comboBox_2 = new JComboBox();
		comboBox_2.setModel(new DefaultComboBoxModel(new String[] {"Viewer", "CSV"}));
		comboBox_2.setBounds(625, 0, 104, 30);
		panel_6.add(comboBox_2);
		
		
		DefaultTableModel model;
		table = new JTable();
		JTableHeader header = table.getTableHeader();
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setEnabled(false);
		scrollPane.setBounds(14, 76, 1330, 283);
		bottomPanel.add(scrollPane);
		
		yearStart.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				if(yearStart.getSelectedItem().toString().equals(""))
				{
					yearEnd.setEnabled(false);
				}
				else
				{
					yearEnd.setEnabled(true);
				}
			}
		});
		
		//extension of check connection button
		checkConnectionButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				String username = userName.getText();
				String password = pw.getText();
				String portNum = port.getText();
				String hostName = hostname.getText();
				String databaseName = dbName.getText();
				try{
					model.DBConnection db = new model.DBConnection();
					db.setDbName(databaseName);
					db.setHostname(hostName);
					db.setPort(portNum);
					db.setPassword(password);
					db.setUserName(username);
					user = db.getUserName();
					pass = db.getPassword();
					portNumber = db.getPort();
					host = db.getHostname();
					database = db.getDbName();
					Class.forName("com.mysql.jdbc.Driver");
					conn = DriverManager.getConnection("jdbc:mysql://" + db.getHostname() +
														":"+ db.getPort() + "/" + db.getDbName(),
														db.getUserName(), db.getPassword());
					JOptionPane.showMessageDialog(null, "Connection Successful");
					executeQueryButton.setEnabled(true);
					generateResultsButton.setEnabled(true);
					checkLabel.setVisible(false);
				}catch(Exception e){
					JOptionPane.showMessageDialog(null, e);
				}
			}
		});
		
		//extension of check connection button
		executeQueryButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) {
				
				 qm.setQuery(queryTextField.getText());
				 userQuery = qm.getQuery();
				 query = userQuery;		 
				 grm.setQuery(pjt.populateJTablePstmt(conn, query, messageLabel, size, table));
				 
			}
		});
		//extension of generate results button
		generateResultsButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) {
				
				if(options.getSelectedItem().toString().equals("Company Name"))
				{
					String cName = change.getText();
					grm.setCompanyName(cName);	
					limitEnd = Integer.parseInt(limit.getSelectedItem().toString());
					grm.setQuery("SELECT * FROM mysqleditor_table WHERE COMPANYNAME = ? LIMIT ?,?");
					query = grm.getQuery();
//					query2 = "SELECT COUNT(*) FROM mysqleditor_table WHERE COMPANYNAME = ?";
					
					grm.setQuery(pjt.populateJTablePreparedStatement(conn, query, cName, limitStart, limitEnd, table, messageLabel, size));
				}
				else if(options.getSelectedItem().toString().equals("PSIC Code"))
				{
					String psicCode = change.getText();
					grm.setPsicCode(psicCode);		
					limitEnd = Integer.parseInt(limit.getSelectedItem().toString());
					grm.setQuery("SELECT * FROM mysqleditor_table WHERE PSICCODE = ?");
//					query2 = "SELECT COUNT(*) FROM mysqleditor_table WHERE PSICCODE = ?";
					query = grm.getQuery();
					grm.setQuery(pjt.populateJTablePreparedStatement(conn, query, psicCode, limitStart, limitEnd, table, messageLabel, size));
				}
				else if(options.getSelectedItem().toString().equals("Year of Date Registered"))
				{
					String yearstart = yearStart.getSelectedItem().toString();
					String yearend = yearEnd.getSelectedItem().toString();
					limitEnd = Integer.parseInt(limit.getSelectedItem().toString());
					grm.setYearStart(yearstart);	
					grm.setYearEnd(yearend);
					yearstart= grm.getYearStart();
					yearend =grm.getYearEnd();
					
						if(yearEnd.getSelectedItem().toString().equals(""))
						{
							grm.setQuery("SELECT * FROM mysqleditor_table WHERE EXTRACT(year from DATEREGISTERED) = ? LIMIT ?,?");									 
							query = grm.getQuery();							
//							query2 = "SELECT COUNT(*) FROM mysqleditor_table WHERE EXTRACT(year from DATEREGISTERED) = ?";
							grm.setQuery(pjt.populateJTablePreparedStatement(conn, query, yearstart, limitStart, limitEnd, table, messageLabel, size));
						}
						else {
							grm.setQuery("SELECT * FROM mysqleditor_table WHERE EXTRACT(year from DATEREGISTERED) >= ? AND EXTRACT(year from DATEREGISTERED) <= ? LIMIT ?, ?" );
							query = grm.getQuery();
//							query2 = "SELECT COUNT(*) FROM mysqleditor_table WHERE EXTRACT(year from DATEREGISTERED) >= ? AND EXTRACT(year from DATEREGISTERED) <= ?";
							grm.setQuery(pjt.populateJTableForElse(conn, table, limitStart, limitEnd, messageLabel, size, yearstart, yearend, query));
						}
						
						
					
				}
			
			
					else if(options.getSelectedItem().toString().equals("Year of Date Effective"))
					{
						
						String yearstart = yearStart.getSelectedItem().toString();
						String yearend = yearEnd.getSelectedItem().toString();
						limitEnd = Integer.parseInt(limit.getSelectedItem().toString());
						grm.setYearStart(yearstart);	
						grm.setYearEnd(yearend);
						if(yearEnd.getSelectedItem().toString().equals(""))
						{
							grm.setQuery("SELECT * FROM mysqleditor_table WHERE EXTRACT(year from DATEEFFECTIVE) = ? LIMIT ?,?");					
							query = grm.getQuery();
//							query2 = "SELECT COUNT(*) FROM mysqleditor_table WHERE EXTRACT(year from DATEEFFECTIVE) = ?";
							grm.setQuery(pjt.populateJTablePreparedStatement(conn, query, yearstart, limitStart, limitEnd, table, messageLabel, size));
						}
						else 
						{
							grm.setQuery("SELECT * FROM mysqleditor_table WHERE EXTRACT(year from DATEEFFECTIVE) >= ? AND EXTRACT(year from DATEEFFECTIVE) <= ? LIMIT ?, ?" );	
//							query2 = "SELECT COUNT(*) FROM mysqleditor_table WHERE EXTRACT(year from DATEEFFECTIVE) >= ? AND EXTRACT(year from DATEREGISTERED) <= ?";
							query = grm.getQuery();
							grm.setQuery(pjt.populateJTableForElse(conn, table, limitStart, limitEnd, messageLabel, size, yearstart, yearend, query));
						}
					}
					limit.setSelectedIndex(0);
						
				
				}
		});	

		// generate csv code
		btnExportAsCsv.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0)
			{
//				
				query = grm.getQuery();
				JOptionPane.showMessageDialog(null,query);
				String s = (String)JOptionPane.showInputDialog(frame,"Input file name");
				model.DBConnection db = new model.DBConnection();
				
				String newFileName = s + ".csv";
				File newFile = new File(newFileName);
				
				//If a string was returned, say so.
				if ((s != null) && (s.length() > 0)) 
				{
					utility.toCSV.writeToCsv(s, host, database, portNumber, user, pass, query);
					System.out.println(query);
				    return;
				}

			}
		});
		
		//no. of items per output
		limit.addItemListener(new ItemListener() 
		{
			public void itemStateChanged(ItemEvent arg0) 
			{
				
				startLimitForMethods =1;
				if(limit.getSelectedItem().toString() == "All")
				{
					if(options.getSelectedItem().toString().equals("Company Name"))
					{
						
						String cName = change.getText();
						grm.setCompanyName(cName);	
						limitEnd = 9999999;
						grm.setQuery("SELECT * FROM mysqleditor_table WHERE COMPANYNAME = ? LIMIT ?,?");
						query = grm.getQuery();
//						query2 = "SELECT COUNT(*) FROM mysqleditor_table WHERE COMPANYNAME = ?";
						grm.setQuery(pjt.populateJTablePreparedStatement(conn, query, cName, limitStart, limitEnd, table, messageLabel, size));
					}
					else if(options.getSelectedItem().toString().equals("PSIC Code"))
					{
						String psicCode = change.getText();
						grm.setPsicCode(psicCode);		
						limitEnd = 9999999;
						grm.setQuery("SELECT * FROM mysqleditor_table WHERE PSICCODE = ?");
//						query2 = "SELECT COUNT(*) FROM mysqleditor_table WHERE PSICCODE = ?";
						query = grm.getQuery();
						grm.setQuery(pjt.populateJTablePreparedStatement(conn, query, psicCode, limitStart, limitEnd, table, messageLabel, size));
					}
					else if(options.getSelectedItem().toString().equals("Year of Date Registered"))
					{
						String yearstart = yearStart.getSelectedItem().toString();
						String yearend = yearEnd.getSelectedItem().toString();
						limitEnd = 9999999;
						grm.setYearStart(yearstart);	
						grm.setYearEnd(yearend);
						yearstart= grm.getYearStart();
						yearend =grm.getYearEnd();
						
							if(yearEnd.getSelectedItem().toString().equals(""))
							{
								grm.setQuery("SELECT * FROM mysqleditor_table WHERE EXTRACT(year from DATEREGISTERED) = ? LIMIT ?,?");									 
								query = grm.getQuery();							
//								query2 = "SELECT COUNT(*) FROM mysqleditor_table WHERE EXTRACT(year from DATEREGISTERED) = ?";
								grm.setQuery(pjt.populateJTablePreparedStatement(conn, query, yearstart, limitStart, limitEnd, table, messageLabel, size));
							}
							else {
								grm.setQuery("SELECT * FROM mysqleditor_table WHERE EXTRACT(year from DATEREGISTERED) >= ? AND EXTRACT(year from DATEREGISTERED) <= ? LIMIT ?, ?" );
								query = grm.getQuery();
//								query2 = "SELECT COUNT(*) FROM mysqleditor_table WHERE EXTRACT(year from DATEREGISTERED) >= ? AND EXTRACT(year from DATEREGISTERED) <= ?";
								grm.setQuery(pjt.populateJTableForElse(conn, table, limitStart, limitEnd, messageLabel, size, yearstart, yearend, query));
							}
							
							
						
					}
				
		
				
				
						else if(options.getSelectedItem().toString().equals("Year of Date Effective"))
						{
							
							String yearstart = yearStart.getSelectedItem().toString();
							String yearend = yearEnd.getSelectedItem().toString();
							limitEnd = 9999999;
							grm.setYearStart(yearstart);	
							grm.setYearEnd(yearend);
							if(yearEnd.getSelectedItem().toString().equals(""))
							{
								grm.setQuery("SELECT * FROM mysqleditor_table WHERE EXTRACT(year from DATEEFFECTIVE) = ? LIMIT ?,?");					
								query = grm.getQuery();
//								query2 = "SELECT COUNT(*) FROM mysqleditor_table WHERE EXTRACT(year from DATEEFFECTIVE) = ?";
								grm.setQuery(pjt.populateJTablePreparedStatement(conn, query, yearstart, limitStart, limitEnd, table, messageLabel, size));
							}
							else 
							{
								grm.setQuery("SELECT * FROM mysqleditor_table WHERE EXTRACT(year from DATEEFFECTIVE) >= ? AND EXTRACT(year from DATEEFFECTIVE) <= ? LIMIT ?, ?" );	
//								query2 = "SELECT COUNT(*) FROM mysqleditor_table WHERE EXTRACT(year from DATEEFFECTIVE) >= ? AND EXTRACT(year from DATEREGISTERED) <= ?";
								query = grm.getQuery();
								grm.setQuery(pjt.populateJTableForElse(conn, table, limitStart, limitEnd, messageLabel, size, yearstart, yearend, query));
							}
						}
						
				}
			
				
				else {
					
				if(options.getSelectedItem().toString().equals("Company Name"))
				{
					String cName = change.getText();
					grm.setCompanyName(cName);	
					limitEnd = Integer.parseInt(limit.getSelectedItem().toString());
					grm.setQuery("SELECT * FROM mysqleditor_table WHERE COMPANYNAME = ? LIMIT ?,?");
					query = grm.getQuery();
//					query2 = "SELECT COUNT(*) FROM mysqleditor_table WHERE COMPANYNAME = ?";
					grm.setQuery(pjt.populateJTablePreparedStatement(conn, query, cName, limitStart, limitEnd, table, messageLabel, size));
				}
				else if(options.getSelectedItem().toString().equals("PSIC Code"))
				{
					String psicCode = change.getText();
					grm.setPsicCode(psicCode);		
					limitEnd = Integer.parseInt(limit.getSelectedItem().toString());
					grm.setQuery("SELECT * FROM mysqleditor_table WHERE PSICCODE = ?");
//					query2 = "SELECT COUNT(*) FROM mysqleditor_table WHERE PSICCODE = ?";
					query = grm.getQuery();
					grm.setQuery(pjt.populateJTablePreparedStatement(conn, query, psicCode, limitStart, limitEnd, table, messageLabel, size));
				}
				else if(options.getSelectedItem().toString().equals("Year of Date Registered"))
				{
					String yearstart = yearStart.getSelectedItem().toString();
					String yearend = yearEnd.getSelectedItem().toString();
					limitEnd = Integer.parseInt(limit.getSelectedItem().toString());
					grm.setYearStart(yearstart);	
					grm.setYearEnd(yearend);
					yearstart= grm.getYearStart();
					yearend =grm.getYearEnd();
					
						if(yearEnd.getSelectedItem().toString().equals(""))
						{
							grm.setQuery("SELECT * FROM mysqleditor_table WHERE EXTRACT(year from DATEREGISTERED) = ? LIMIT ?,?");									 
							query = grm.getQuery();							
//							query2 = "SELECT COUNT(*) FROM mysqleditor_table WHERE EXTRACT(year from DATEREGISTERED) = ?";
							grm.setQuery(pjt.populateJTablePreparedStatement(conn, query, yearstart, limitStart, limitEnd, table, messageLabel, size));
						}
						else {
							grm.setQuery("SELECT * FROM mysqleditor_table WHERE EXTRACT(year from DATEREGISTERED) >= ? AND EXTRACT(year from DATEREGISTERED) <= ? LIMIT ?, ?" );
							query = grm.getQuery();
//							query2 = "SELECT COUNT(*) FROM mysqleditor_table WHERE EXTRACT(year from DATEREGISTERED) >= ? AND EXTRACT(year from DATEREGISTERED) <= ?";
							grm.setQuery(pjt.populateJTableForElse(conn, table, limitStart, limitEnd, messageLabel, size, yearstart, yearend, query));
						}
						
						
					
				}
			
			
					else if(options.getSelectedItem().toString().equals("Year of Date Effective"))
					{
						
						String yearstart = yearStart.getSelectedItem().toString();
						String yearend = yearEnd.getSelectedItem().toString();
						limitEnd = Integer.parseInt(limit.getSelectedItem().toString());
						grm.setYearStart(yearstart);	
						grm.setYearEnd(yearend);
						if(yearEnd.getSelectedItem().toString().equals(""))
						{
							grm.setQuery("SELECT * FROM mysqleditor_table WHERE EXTRACT(year from DATEEFFECTIVE) = ? LIMIT ?,?");					
							query = grm.getQuery();
//							query2 = "SELECT COUNT(*) FROM mysqleditor_table WHERE EXTRACT(year from DATEEFFECTIVE) = ?";
							pjt.populateJTablePreparedStatement(conn, query, yearstart, limitStart, limitEnd, table, messageLabel, size);
						}
						else 
						{
							grm.setQuery("SELECT * FROM mysqleditor_table WHERE EXTRACT(year from DATEEFFECTIVE) >= ? AND EXTRACT(year from DATEEFFECTIVE) <= ? LIMIT ?, ?" );	
//							query2 = "SELECT COUNT(*) FROM mysqleditor_table WHERE EXTRACT(year from DATEEFFECTIVE) >= ? AND EXTRACT(year from DATEREGISTERED) <= ?";
							query = grm.getQuery();
							pjt.populateJTableForElse(conn, table, limitStart, limitEnd, messageLabel, size, yearstart, yearend, query);
						}
					}
					
						
				}
				}
			});
		
		
		
		
		
		
	}
}
