package web.utility;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;

import web.model.UserBean;
import web.model.ConnectionBean;
import web.utility.SQLCommands;
public class SQLOperations implements SQLCommands{
	private static Connection connection;

	public static Connection getDBInstanceConnection() {
		
				try 
				{	
					ConnectionBean connBean = new ConnectionBean();
					String propFile = "resources/config.properties";
					FileInputStream fis = new FileInputStream(propFile);
					Properties property = new Properties();
					property.load(fis);
					
					connBean.setDatabase(property.getProperty("database"));
					connBean.setDriverName(property.getProperty("driverName"));
					connBean.setPassword(property.getProperty("password"));
					connBean.setPort(property.getProperty("port"));
					connBean.setUserName(property.getProperty("userName"));
					connBean.setPartURL(property.getProperty("jdbcPartURL"));
					connBean.setServer(property.getProperty("server"));
						
					//now instantiates the connection object
					
					
						Class.forName(connBean.getDriverName());
						String url = 
						  connBean.getPartURL()+"//" + 
						  connBean.getServer() + ":" 
						+connBean.getPort() + "/" + 
						  connBean.getDatabase(); 
						
						connection = 
						  DriverManager.getConnection(url,connBean.getUserName(),connBean.getPassword());
					} catch (ClassNotFoundException cnfe) {
						System.err.println(cnfe.getMessage());
					} catch (SQLException sqle) {
						System.err.println(sqle.getMessage());
					}
					catch (FileNotFoundException fnfe) {
						System.err.println(fnfe.getMessage());
					}
					catch (Exception e) {
						System.err.println(e.getMessage());
					}
					return connection;
			}
			public static Connection getConnection()
			{
				return (connection!=null ?connection:getDBInstanceConnection());
			}	
			
			
			
			public static boolean addUser(UserBean user, 
					Connection connection) {
					
					try {
				        PreparedStatement pstmt = connection.prepareStatement(INSERT_USER);
				        pstmt.setString(1, user.getUsername()); 
				        pstmt.setString(2, user.getPassword());
				        pstmt.setString(3, user.getFirstName());
				        pstmt.setString(4, user.getLastName());   
				        pstmt.setString(5, user.getEmail());  
				        pstmt.setString(6, user.getMobileNum());  
				        pstmt.setString(7, user.getAddress());  
				        pstmt.executeUpdate(); // execute insert statement  
					} catch (SQLException sqle) {
						System.out.println("SQLException - addUser: " + sqle.getMessage());
						return false; 
					}	
					return true;
				}
	
}

