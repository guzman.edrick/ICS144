package web.utility;

public interface SQLCommands {

	String INSERT_USER = "insert into signup(username, password, firstName, lastName, email, mobileNum, address) values(?,?,?,?,?,?,?)";
	String GET_ALL_EMPLOYEES = "select * from signup";
	
	String SEARCH_EMPLOYEE = "select * from signup where id=?";
	String UPDATE_EMPLOYEE = "update signup set lastname = ?, firstname = ?, email=?, username=? where id = ?";
	
	String DELETE_EMPLOYEE = "delete from signup where id=?";
}