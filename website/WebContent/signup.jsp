<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="net.tanesha.recaptcha.ReCaptcha" %>
<%@ page import="net.tanesha.recaptcha.ReCaptchaFactory" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>  
    <head>  
         <title>Signup</title>  
        <link type = "text/css" rel = "stylesheet" href = "css/practice.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/font-awesome.css">
        <script type="text/javascript" src="js/jquery.min.js">   </script> 
        <link rel="stylesheet" href="css/bootstrap.min.css">        
        <script src="bootstrap.min.js"></script>
        <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
        <script src='https://www.google.com/recaptcha/api.js'></script>
        <script>
        $(document).ready(function(){
             $("#home").hover(function(){
                $(".HomeToggle").slideToggle("slow")
            });
        });
        </script>
        </head>
    <body style = "background-color: #F26B38;">
        <header>
            <div class = "topmost" style = "display: inline;">
                <!-- For signup,login search bar -->
                <form action="action_page.php" class = "formSearch" style = "margin-left:70%;">
                      <input type="search" name="googlesearch" placeholder = "Search website" style = "border-radius: 5%;">
                      <input type="submit">&nbsp;&nbsp;
                   
                      <a href = "signup.jsp" class = "signup">Sign Up</a> <span>| </span>   
                      <a href = "login.html" class = "login">Login</a> 
                </form>
                
            </div>
            
            <div class="wrap"></div>
            
            <div class = "toolbar" style = "position: inline;">
                <!-- Home -->
                <a href = "index.jsp" class="toolbarText" id ="home"><i class="fa fa-home" style= "font-size:18px;color:white;"></i> Home <i class="fa fa-caret-down" aria-hidden="true"></i></a>
                <!-- About us -->
                <a href = "home.jsp" class="toolbarText"><i class="fa fa-phone" style = "font-size:16px;color:white;"></i> About Us <i class="fa fa-caret-down" aria-hidden="true"></i></a>
                <!-- Gallery -->
                <a href = "home.jsp" class="toolbarText"><i class= "fa-picture" style = "font-size:16px;color:white;"></i> Gallery <i class="fa fa-caret-down" aria-hidden="true"></i></a>
                <!-- Contact us -->
                <a href = "home.jsp" class="toolbarText"><i class="fa fa-phone" style = "font-size:16px;color:white;"></i> Contact Us <i class="fa fa-caret-down" aria-hidden="true"></i></a>
               
            </div>
        </header>
        
            <div class = "formBar" style = "padding:1em; background-color:white;"></div>
         <div style = "padding:2em; background-color: #363636;height: auto; margin: auto; width:800px;position: relative;margin-top:40px; border-radius:5%;">
         
            <form action = "login.jsp" method = "post" style = "display:inline;">
                <h3 style = "color: white; text-align: center;font-family: verdana;">Sign up form</h3>
                <p style = "color: white;font-size:12px; text-align: center;font-family: verdana;">Please fill up all required fields*</p>
            <br>
            	<!-- Username, password -->
                <div>
                        <p style = "color: white; font-size:12px;text-align: left;font-family: verdana;">Username:<span style = "margin-left: 10.5em;">Password:</span></p>
                        <input type = "text" placeholder="Username" id = "username" style= "border-radius:5%;" required/>

                        <span style = "color:black;margin-left: 1em;font-size:12px;"><input type = "password" placeholder="Password" id = "password" style= "border-radius:5%;"required/></span>
                </div><br>
                <br>
                <!-- First and last name -->
                <div>
                        <p style = "color: white; text-align: left;font-family: verdana;font-size:12px;">First Name:<span style = "margin-left: 10em;font-size:12px;">Last Name:</span></p>
                        <input type = "text" placeholder="First Name" id = "fName" style= "border-radius:5%;" required/>

                        <span style = "color:black;margin-left: 1em;font-size:12px;"><input type = "text" placeholder="Last Name" id = "lName" style= "border-radius:5%;" required/></span>
                </div><br>
                <br>
                <!-- email ad, contact no -->
                <div style = "display:inline;">
                        <p style = "color: white; text-align: left;font-family: verdana;font-size:12px;">Email Address:<span style = "margin-left: 8.5em;font-size:12px;">Mobile #:</span></p>
                        	<input type = "email" placeholder="Email address" id = "emailAd" style= "border-radius:5%;" required/>
                        <span style = "margin-left: 1em;font-size:12px; color:black;">
                        	<input type = "text"  placeholder = "Contact No." id = "contact" style= "border-radius:5%;" required/>
                        </span>
                </div><br><br>
                <!-- address -->
                		<p style = "color: white; text-align: left;font-family: verdana;font-size:12px;">Address:</p>
                			<input type = "text" placeholder="Address" id ="address" style  = "border-radius:5%; width: 25em;" required/>
                <br><br>
                <!-- recaptcha -->
                		<p style ="font-size:12px;color: white; text-align: left;font-family: verdana; margin-left:11em;">Are you a robot?</p>
                <div class="g-recaptcha" data-sitekey="6LdrSSQTAAAAAFHqhUMvpa_8YAXZSsc_X2_jS0wU" style = "margin-left: 2em;"></div>
            	<br>
            				<input type ="submit" value = "Sign up" style = "margin-left:10.5em;"/>
        	
            </form>
            <div style ="height:auto; width:auto;position:absolute; margin-left:50%; margin-top:-35em; ">
	            <form action = "hello" method = "post" style = "background-color: black;padding: 2em;border-radius:5%; width: 25em;height:auto;">
	            <p style = "color: white; text-align: center;font-family: verdana;">Already have an account?</p>
	            <h5 style = "color: white; text-align: center;font-family: verdana; font-size:12px;">Sign in</h5>
	            <br>
	            <!-- Username -->
	            <p style = "color: white; text-align: left;font-family: verdana;margin-left:8em;">Username:</p>
	            <input type = "text" placeholder="Enter username" id = "user" style= "border-radius:5%;margin-left:4.5em;" required/><br>
	            <br>
	            <!-- Password -->
	            <p style = "color: white; text-align: left;font-family: verdana;margin-left:8em;">Password:</p>
	            <input type = "password" placeholder="Enter password" id = "pass" style= "border-radius:5%;margin-left:4.5em;" required/> 
	            <br><br><input type ="submit" value = "Sign in" style = "margin-left:8em;"/>  
	            </form>
            </div>
            </div>
            
        
            
        

        
    </body>
</html>