package model;

public class DBConnection {
	static String userName;
	static String pw;
	
	public void setUserName(String userName){
		this.userName = userName;
	}
	public void setPassword(String pw){
		this.pw = pw;
	}
	
	public static String getPassword(){
		return pw;
	}
	public static String getUserName(){
		return userName;
	}
	
}
