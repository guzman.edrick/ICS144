package core;

import java.awt.EventQueue;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.File;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Locale;

import javax.swing.JLabel;
import javax.swing.JTextField;

import core.main;

public class oraclePage extends main{

	private JFrame frmOracleCsvGenerator;
	private JTextField fileName;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					oraclePage window = new oraclePage();
					window.frmOracleCsvGenerator.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public oraclePage() {
		initialize(model.DBConnection.getUserName(), model.DBConnection.getPassword());
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(String username, final String pw) {
		frmOracleCsvGenerator = new JFrame();
		frmOracleCsvGenerator.setTitle("Oracle CSV generator");
		frmOracleCsvGenerator.setBounds(100, 100, 450, 300);
		frmOracleCsvGenerator.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmOracleCsvGenerator.getContentPane().setLayout(null);
		
		final JList list = new JList();
		list.setBounds(223, 26, 186, 164);
		frmOracleCsvGenerator.getContentPane().add(list);
		
		
		final JLabel fileName = new JLabel("Filename:");
		fileName.setBounds(10, 97, 68, 50);
		frmOracleCsvGenerator.getContentPane().add(fileName);
		
		JButton load = new JButton("LOAD DATA");
		load.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DefaultListModel dlm = new DefaultListModel();
				Locale.setDefault(Locale.ENGLISH);
				
				
				try {
					Locale.setDefault(Locale.ENGLISH);
					Class.forName("oracle.jdbc.driver.OracleDriver");
					conn = DriverManager.getConnection("jdbc:oracle:thin:@//localhost:1521/XE",model.DBConnection.getUserName(), model.DBConnection.getPassword());
					DatabaseMetaData meta =conn.getMetaData();
					Statement stmt = conn.createStatement();
					ResultSet res = stmt.executeQuery("SELECT table_name FROM user_tables");
					      
					while (res.next()) {
				        	dlm.addElement(res.getString("TABLE_NAME"));
				      }
					list.setModel(dlm);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(null, e);
				
				}
			   
			}
		});
		load.setBounds(33, 51, 119, 23);
		frmOracleCsvGenerator.getContentPane().add(load);
		
		JButton btnGenerateCsv = new JButton("Generate");
		btnGenerateCsv.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) {
				String select = list.getSelectedValue().toString();
				String file = textField.getText();
				String newFileName = file + ".csv";
				File newFile = new File(newFileName);
				try {
					conn = DriverManager.getConnection("jdbc:oracle:thin:@//localhost:1521/XE",model.DBConnection.getUserName(), model.DBConnection.getPassword());
					Statement stmt=conn.createStatement();  
					ResultSet rs=stmt.executeQuery("select * from " + list.getSelectedValue().toString());
					ResultSetMetaData rsmd = rs.getMetaData();
					int columnsNumber = rsmd.getColumnCount();
					StringBuilder sb = new StringBuilder();
					StringBuilder sbForRecords = new StringBuilder();
					
					for(int i = 1; i<= columnsNumber; i++){
						if(i == columnsNumber){
							sb.append(rsmd.getColumnName(i));
						}
						else{
							sb.append(rsmd.getColumnName(i)+ ",");
						}
					}
					csvMethods.headerWriteData(sb.toString(),newFileName,newFile);
					
					while(rs.next())
					{
							for(int j = 1; j <= columnsNumber; j++){
								if(rsmd.getColumnClassName(j).toString() == "java.lang.Integer"){
								
									if(j == columnsNumber){
										sbForRecords.append(rs.getInt(j));
									}
									else{
										sbForRecords.append(rs.getInt(j)+ ",");								
									}
									
								}
								else{
									
									if(j == columnsNumber){
										sbForRecords.append(rs.getString(j));
									}
									else{
										sbForRecords.append(rs.getString(j)+ ",");									
									}
								}			
							}
							csvMethods.writeData(sbForRecords.toString(), newFileName, newFile);	
							sbForRecords.delete(0, sbForRecords.length());
					}
					
					JOptionPane.showMessageDialog(null, "CSV File " + newFileName+ " created.");
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
				
			
			}
		});
		btnGenerateCsv.setBounds(33, 153, 119, 23);
		frmOracleCsvGenerator.getContentPane().add(btnGenerateCsv);
		
		JLabel lblGenerateCsv = new JLabel("Generate CSV:");
		lblGenerateCsv.setBounds(57, 69, 200, 50);
		frmOracleCsvGenerator.getContentPane().add(lblGenerateCsv);
		
		textField = new JTextField();
		textField.setBounds(76, 112, 119, 20);
		frmOracleCsvGenerator.getContentPane().add(textField);
		textField.setColumns(10);
	}
	void setVisible(boolean b){
		try {
			oraclePage window = new oraclePage();
			window.frmOracleCsvGenerator.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
