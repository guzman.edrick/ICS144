package core;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.swing.JOptionPane;

public class dbConnect {
	 	
	   
	    private static Connection con;
	  

	    public static Connection getConnection(String url, String driverName, String username, String password) {
	        try {
	            Class.forName(driverName);
	            try {
	                con = DriverManager.getConnection(url, username, password);
	            } catch (SQLException ex) {
	                // log an exception. from example:
	            	JOptionPane.showMessageDialog(null, "Failed to create the database connection.");
	               
	            }
	        } catch (ClassNotFoundException ex) {
	            // log an exception. for example:z
	            System.out.println("Driver not found."); 
	        }
	        return con;
	    }
	
}
