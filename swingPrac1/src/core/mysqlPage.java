package core;


import java.awt.EventQueue;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;

import java.awt.BorderLayout;

import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.File;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Locale;

import javax.swing.JLabel;
import javax.swing.JTextField;

import core.csvMethods;
import core.main;

public class mysqlPage extends main {

	private JFrame frmMysqlCsvGenerator;
	private JTextField fileName;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					mysqlPage window = new mysqlPage();
					window.frmMysqlCsvGenerator.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public mysqlPage() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmMysqlCsvGenerator = new JFrame();
		frmMysqlCsvGenerator.setTitle("MYSQL CSV Generator");
		frmMysqlCsvGenerator.setBounds(100, 100, 450, 300);
		frmMysqlCsvGenerator.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMysqlCsvGenerator.getContentPane().setLayout(null);
		
		final JList list = new JList();
		list.setBounds(220, 27, 183, 195);
		frmMysqlCsvGenerator.getContentPane().add(list);
		
		JButton load = new JButton("Load Data");
		load.addActionListener(new ActionListener() 
		{
			
			public void actionPerformed(ActionEvent arg0) 
			{
				DefaultListModel dlm = new DefaultListModel();
				try
					{
						Class.forName("com.mysql.jdbc.Driver");
						conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/practice",model.DBConnection.getUserName(), model.DBConnection.getPassword());
						DatabaseMetaData meta =conn.getMetaData();
						ResultSet res = meta.getTables(null, null, null, 
						         new String[] {"TABLE"});
						      
						while (res.next()) {
					        	dlm.addElement(res.getString("TABLE_NAME"));
					      }
						list.setModel(dlm);
						
						
					
					} 
				catch (Exception e) 
					{
				// TODO Auto-generated catch block
						JOptionPane.showMessageDialog(null, e);
			
					}
			}
		});
		load.setBounds(39, 45, 126, 23);
		frmMysqlCsvGenerator.getContentPane().add(load);
		
		JButton generate = new JButton("Generate CSV");
		generate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String select = list.getSelectedValue().toString();
				String file = fileName.getText();
				String newFileName = file + ".csv";
				File newFile = new File(newFileName);
				try 
				{
					conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/practice",model.DBConnection.getUserName(), model.DBConnection.getPassword());
					Statement stmt=conn.createStatement();  
					ResultSet rs=stmt.executeQuery("select * from " + list.getSelectedValue().toString());
					ResultSetMetaData rsmd = rs.getMetaData();
					int columnsNumber = rsmd.getColumnCount();
					StringBuilder sb = new StringBuilder();
					StringBuilder sbForRecords = new StringBuilder();
					
							for(int i = 1; i<= columnsNumber; i++)
							{
								if(i == columnsNumber){
									sb.append(rsmd.getColumnName(i));
								}
								else{
									sb.append(rsmd.getColumnName(i)+ ",");
								}
							}
							csvMethods.headerWriteData(sb.toString(),newFileName,newFile);
							
							while(rs.next())
							{
									for(int j = 1; j <= columnsNumber; j++){
										if(j == columnsNumber){
											sbForRecords.append(rs.getObject(j).toString());
										}
										else{
											sbForRecords.append(rs.getObject(j).toString()+ ",");								
										}
//										if(rsmd.getColumnClassName(j).toString() == "java.lang.Integer"){
//										
//											if(j == columnsNumber){
//												sbForRecords.append(rs.getInt(j));
//											}
//											else{
//												sbForRecords.append(rs.getInt(j)+ ",");								
//											}
//											
//										}
//										else{
//											
//											if(j == columnsNumber){
//												sbForRecords.append(rs.getString(j));
//											}
//											else{
//												sbForRecords.append(rs.getString(j)+ ",");									
//											}
//										}			
									}
									csvMethods.writeData(sbForRecords.toString(), newFileName, newFile);	
									sbForRecords.delete(0, sbForRecords.length());
							}
					
					JOptionPane.showMessageDialog(null, "CSV File " + newFileName+ " created.");
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
				
			}
		});
		generate.setBounds(39, 143, 126, 23);
		frmMysqlCsvGenerator.getContentPane().add(generate);
		
		JLabel lblGenerateCsv = new JLabel("Generate CSV:");
		lblGenerateCsv.setBounds(66, 72, 115, 50);
		frmMysqlCsvGenerator.getContentPane().add(lblGenerateCsv);
		
		JLabel lblFilename = new JLabel("Filename:");
		lblFilename.setBounds(10, 96, 70, 50);
		frmMysqlCsvGenerator.getContentPane().add(lblFilename);
		
		fileName = new JTextField();
		fileName.setBounds(83, 111, 127, 20);
		frmMysqlCsvGenerator.getContentPane().add(fileName);
		fileName.setColumns(10);
		
		JButton btnQuery = new JButton("QUERY");
		btnQuery.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String s = (String)JOptionPane.showInputDialog(
	                    frame,
	                    "Input your query",
	                    "Input your Query",
	                    JOptionPane.PLAIN_MESSAGE,
	                    null,
	                    null,
	                    "Select");

	//If a string was returned, say so.
				if ((s != null) && (s.length() > 0)) {
						JOptionPane.showMessageDialog(null, "Input proper query");
						
				}

			}
		});
		btnQuery.setBounds(10, 177, 200, 50);
		frmMysqlCsvGenerator.getContentPane().add(btnQuery);
	}

	public void setVisible(boolean b) {
		// TODO Auto-generated method stub
		try {
			mysqlPage window = new mysqlPage();
			window.frmMysqlCsvGenerator.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
