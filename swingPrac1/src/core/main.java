package core;
import java.util.*;
import model.DBConnection;
import java.awt.EventQueue;
import java.awt.Window;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;

import java.awt.Font;

import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;

import com.sun.corba.se.pept.transport.Connection;

import core.oraclePage;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class main {

	 JFrame frame;
	 JTextField username;
	 JPasswordField password;
	 java.sql.Connection conn;
	 Statement stmt;
	 ResultSet rs;
	 ButtonGroup bg;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					main window = new main();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public main() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblPoweredByInfobuilder = new JLabel("Powered by: InfoBuilder");
		lblPoweredByInfobuilder.setBounds(296, 251, 138, 14);
		frame.getContentPane().add(lblPoweredByInfobuilder);
		bg = new ButtonGroup();
		
		final JRadioButton radioOracle = new JRadioButton("Oracle");
		radioOracle.setBounds(137, 63, 64, 23);
		frame.getContentPane().add(radioOracle);
		
		
		final JRadioButton radioMysql = new JRadioButton("MySql");
		radioMysql.setBounds(213, 63, 109, 23);
		frame.getContentPane().add(radioMysql);
		
		bg.add(radioOracle);
		bg.add(radioMysql);
		JLabel lblDatabase = new JLabel("Database:");
		lblDatabase.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblDatabase.setBounds(176, 41, 70, 14);
		frame.getContentPane().add(lblDatabase);
		
		username = new JTextField();
		username.setBounds(176, 108, 146, 20);
		frame.getContentPane().add(username);
		username.setColumns(10);
		
		password = new JPasswordField();
		password.setBounds(176, 139, 146, 20);
		frame.getContentPane().add(password);
		
		JLabel lblUsername = new JLabel("Username:");
		lblUsername.setBounds(84, 111, 82, 14);
		frame.getContentPane().add(lblUsername);
		
		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setBounds(84, 142, 64, 14);
		frame.getContentPane().add(lblPassword);
		
		JButton submit = new JButton("SUBMIT");
		submit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				oraclePage op = new oraclePage();
				mysqlPage mp = new mysqlPage();
				
				String userName = username.getText();
				String pw = password.getText();
				String oracle = radioOracle.getText();
				String mysql = radioMysql.getText();
				model.DBConnection db = new model.DBConnection();
				db.setPassword(pw);
				db.setUserName(userName);
				try{
					if(radioOracle.isSelected() == true){
						Locale.setDefault(Locale.ENGLISH);
						Class.forName("oracle.jdbc.driver.OracleDriver");
						 conn = DriverManager.getConnection("jdbc:oracle:thin:@//localhost:1521/XE", model.DBConnection.getUserName(), model.DBConnection.getPassword());
						 
						 JOptionPane.showMessageDialog(null, "Connected to Oracle Database");
						
						 frame.setVisible(false);
						 frame.dispose();
						 op.setVisible(true);
						
					}
					else if(radioMysql.isSelected() == true){
						Class.forName("com.mysql.jdbc.Driver");
						conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/practice",model.DBConnection.getUserName(), model.DBConnection.getPassword());
						stmt = conn.createStatement();
						JOptionPane.showMessageDialog(null, "Connected to MySQL Database");
						conn.close();
						frame.setVisible(false);
						frame.dispose();
						mp.setVisible(true);
					}
					
				}catch(Exception e){
					JOptionPane.showMessageDialog(null, e);
				}
			}
		});
		submit.setBounds(117, 184, 89, 23);
		frame.getContentPane().add(submit);
		
		JButton clear = new JButton("CLEAR");
		clear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				username.setText("");
				password.setText("");
			}
		});
		clear.setBounds(233, 184, 89, 23);
		frame.getContentPane().add(clear);
	}
	
	
	
	
}
