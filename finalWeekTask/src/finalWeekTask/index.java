package finalWeekTask;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Scanner;

import utilities.sql;

public class index {
	private static Scanner cin;
	

	public static void main(String[] args) 
	{
		sql sa = new sql();
		Connection conn = sa.getDBConnection();
		methodsForIndex mfi = new methodsForIndex();
		ArrayList<String> secno = new ArrayList<String>();
		ArrayList<String> listOfTables = new ArrayList<String>();
		cin = new Scanner(System.in);
		String confirmation_answer = "";
		

		
		System.out.println("********************************************************************************");
		System.out.println("*    **   **   *******  **        **       *******      **  **  **  **  **     *");
		System.out.println("*    **   **   **       **        **       **   **      **  **  **  **  **     *");
		System.out.println("*    *******   *******  **        **       **   **      **  **  **  **  **     *");
		System.out.println("*    **   **   **       **        **       **   **                             *");
		System.out.println("*    **   **   *******  *******   *******  *******      **  **  **  **  **     *");
		System.out.println("********************************************************************************");
		System.out.println("\n WELCOME TO (INSERT PROGRAM NAME HERE)");
		
		
		String folderPath = mfi.inputValidation(confirmation_answer, cin, listOfTables, conn, secno);	
		String filePath = mfi.getFilePathFromTextFile();
		String queries = mfi.readText(filePath);
		String[] arrayOfQueries = mfi.populateArrayOfQueriesFromTxtFile(queries);
		ArrayList<String> updatedArrayOfQueries = mfi.reupdateArrayOfQueries(arrayOfQueries);
		mfi.logQueries(updatedArrayOfQueries);
		mfi.executeQueriesFromUpdatedArrayList(updatedArrayOfQueries, secno, conn, folderPath);
		
			
	}

}
