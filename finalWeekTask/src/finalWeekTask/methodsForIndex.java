package finalWeekTask;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Scanner;

import methods.toCSV;



public class methodsForIndex {
	
	
	public String inputValidation(String confirmation_answer, Scanner cin, 
			ArrayList<String> listOfTables, Connection conn, ArrayList<String> secno)
	{
		String startingDate = "";
		String endingDate = "";
		
		while(!(confirmation_answer.equals("Y")))
		{
				System.out.println("\n Please input the range of date here: (e.g. 1990)");
				System.out.print("\n Insert starting date here: ");
				startingDate = cin.next();
				System.out.print("\n Insert ending date here: ");
				endingDate = cin.next();
				try
				{
						if(Integer.parseInt(endingDate) >=  Integer.parseInt(startingDate)){
							System.out.println("CONFIRMATION[WRITE Y/N]: Starting year: " + startingDate+ "   Ending date: " + endingDate);
							confirmation_answer = cin.next();
						}
						else{
							System.out.println("ERROR: ending date lesser than starting date");
							confirmation_answer = "N";
						}
				}
				catch(Exception e)
				{
					System.out.println("Invalid input");
					confirmation_answer = "N";
				}		
				
		}
		getTablesOfDatabase(listOfTables, conn);
		populateArrayList(secno, listOfTables, conn, startingDate, endingDate);
		logLists(secno,listOfTables,startingDate,endingDate);
		String folderPath = createFolder(startingDate, endingDate);
		return folderPath;
		
		
	}
	
	public String createFolder(String startingDate, String endingDate)
	{

		FileInputStream fileInput;
		Properties properties = new Properties();
		String folderPath = "";
				
					try 
					{
						fileInput = new FileInputStream("resources/config.properties");
						try {
							properties.load(fileInput);
							folderPath = properties.getProperty("folderPath") + "SECNO_Year_" + startingDate + "-" + endingDate + "/";
							File file = new File(properties.getProperty("folderPath") + "SECNO_Year_" + startingDate + "-" + endingDate);
					        if (!file.exists()) {
					            if (file.mkdir()) {
					                System.out.println("Folder " + "SECNO_Year_" + startingDate + "-" + endingDate+ " is created!");
					            } else {
					                System.out.println("Failed to create directory!");
					            }
					        }
					        return folderPath;
						} catch (IOException e) {
							
							e.printStackTrace();							
						}
					} catch (FileNotFoundException e1)
					{
						// TODO Auto-generated catch block
						e1.printStackTrace();
						
					}
		return folderPath;
		
	}
	
	public void getTablesOfDatabase(ArrayList<String> listOfTables, Connection conn)
	{
		try
		{
			DatabaseMetaData meta =conn.getMetaData();
			ResultSet res = meta.getTables(null, null, null, 
			         new String[] {"TABLE"});
			      
			listOfTables.add("TBL_CD_PROFILE");
//			while (res.next()) 
//			{
//		        	listOfTables.add(res.getString("TABLE"));
//		    }
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void populateArrayList(ArrayList<String> secno, ArrayList<String> listOfTables, Connection conn,
									String startingDate, String endingDate)
	{
		try 
		{
			for(int i = 0; i <= listOfTables.size()-1;i++){
				String query = "SELECT SECNO FROM SEC_Migration_DEV." +listOfTables.get(i) + " WHERE EXTRACT(year from DATEREGISTERED) >= ? AND EXTRACT(year from DATEREGISTERED) <= ?";
				PreparedStatement pstmt = conn.prepareStatement(query);	
				pstmt.setString(1, startingDate);
				pstmt.setString(2, endingDate);
				ResultSet rs = pstmt.executeQuery();
				
				while(rs.next())
				{
					secno.add(rs.getString("SECNO"));
				}
				pstmt.close();
				rs.close();	
			}
			
		} 
		catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void logLists(ArrayList<String> secno, ArrayList<String> listOfTables, String startingDate, String endingDate)
	{
		int i = 0;
		System.out.println("\nLIST OF SECNOS gathered in year " + startingDate+ " to " + endingDate);
		while(i != secno.size()){
			System.out.println((i + 1) + ": " + secno.get(i));
			i++;
		}
	}
	
	
	public String getFilePathFromTextFile(){
		
		FileInputStream fileInput;
		Properties properties = new Properties();
		String filePath = "";
				
					try 
					{
						fileInput = new FileInputStream("resources/config.properties");
						try {
							properties.load(fileInput);
							filePath = properties.getProperty("queryFilePath");
						} catch (IOException e) {
							
							e.printStackTrace();							
						}
					} catch (FileNotFoundException e1)
					{
						// TODO Auto-generated catch block
						e1.printStackTrace();
						
					}
					return filePath;
				
	}
	
	public String readText(String filePath)
	{
		String queries = "";
		try (BufferedReader br = new BufferedReader(new FileReader(filePath)))
		{
			String str;
			StringBuilder sb = new StringBuilder();
		while((str = br.readLine()) != null){
			
			sb.append(str.trim());
		}
			queries = sb.toString();
			System.out.println(queries);
			return queries;

		} catch (IOException e) {
			e.printStackTrace();
		}
		return queries; 
	
	}
	
	public String[] populateArrayOfQueriesFromTxtFile(String queries)
	{
		FileInputStream fileInput;
		Properties properties = new Properties();
	
		String delimeter = "";
		String[] array = {};
					try 
					{
						fileInput = new FileInputStream("resources/config.properties");
						try {
							properties.load(fileInput);
							delimeter = properties.getProperty("delimeterOfTxt");
							array = queries.split(delimeter);
							return array;
						} catch (IOException e) {
							
							e.printStackTrace();							
						}
					} catch (FileNotFoundException e1)
					{
						// TODO Auto-generated catch block
						e1.printStackTrace();
						
					}
		
		return array;
	}
	
	public ArrayList<String> reupdateArrayOfQueries(String[] arrayOfQueries)

	{
		ArrayList<String> updatedQueries = new ArrayList<String>();
		for(int i = 0; i <= arrayOfQueries.length-1; i++)
		{
			String prefix = arrayOfQueries[i];
			updatedQueries.add(prefix.concat(String.format(" WHERE %1s = ?", "SECNO")));
		}
		
		return updatedQueries;
	
	}
	
	public void logQueries(ArrayList<String> updatedArrayOfQueries)
	{
		int i = 0;
		System.out.println("\nLIST OF queries for prepared statement:");
		while(i != updatedArrayOfQueries.size()){
			System.out.println("Query no. " + (i+1) + ": " + updatedArrayOfQueries.get(i));
			i++;
		}
	}
	
	
	
	
	
	
	
	
	
	//for csv writing
	
	public void executeQueriesFromUpdatedArrayList(ArrayList<String> updatedArrayOfQueries, ArrayList<String> secno, Connection conn, String folderPath)
	{
		toCSV tc = new toCSV();
		
		int j=0;
		String query = "";
		
		System.out.println("GENERATING CSV FILES.......");
		try
		{
			
				while(j != updatedArrayOfQueries.size())
				{
					try{
						int i=0;
						System.out.println("Index: " + j);
						
						query = updatedArrayOfQueries.get(j);
						
						while(i != secno.size())
						{
							System.out.println("Currently on: " + i);
						
							PreparedStatement pstmt = conn.prepareStatement(query);
							pstmt.setString(1, secno.get(i));
						    ResultSet rs = pstmt.executeQuery();
						    ResultSetMetaData rsmd = rs.getMetaData();
							int columnsNumber = rsmd.getColumnCount();
							System.out.println("Writing folder w/ csv file for SECNO: " + secno.get(i));
							System.out.println(pstmt);
							String subFolderFileName = folderPath + secno.get(i)+"/";
							File file = new File(subFolderFileName);
					        if (!file.exists()) {
					            if (file.mkdir()) {
					                System.out.println("Folder for " + secno.get(i) +" is created!");
					            } else {
					                System.out.println("Failed to create directory!");
					            }
					        }
							String fileName = subFolderFileName + secno.get(i)+ "_" + j + ".csv";
							File newFile = new File(fileName);
							    	
							    	StringBuilder sb = new StringBuilder();
							    		// Write Columns
											for(int x = 1; x<= columnsNumber; x++)
											{
												if(x == columnsNumber)
												{
													sb.append(rsmd.getColumnName(x));
												}
												else
												{
													sb.append(rsmd.getColumnName(x)+ ",");
												}
											}
											tc.headerWriteData(sb.toString(),fileName,newFile);
											
											StringBuilder sbForRecords = new StringBuilder();	
											for(int y = 1; y <= columnsNumber; y++)
											{
												try{
													try{
														if(y == columnsNumber){
															sbForRecords.append(rs.getObject(y).toString());
														}
														else{
															sbForRecords.append(rs.getObject(y).toString()+ ",");								
														}
													}catch(NullPointerException npe){
														if(y == columnsNumber){
															sbForRecords.append("null");;
														}
														else{
															sbForRecords.append("null,");;								
														}
														
													}catch(SQLException sql){
														if(y == columnsNumber){
															sbForRecords.append("null");;
														}
														else{
															sbForRecords.append("null,");;							
														}
													}	
												}finally{}
											
			
											}
											tc.writeData(sbForRecords.toString(), fileName, newFile);	
											  sbForRecords.delete(0, sbForRecords.length());
												System.out.println(secno.get(i) + ".csv is created");
											
							    //end of while loop for writing csv 
							  
					    pstmt.close();
					    rs.close();
					    
					    i++;
						}//end of while for # of secnos
					j++;
					}catch(com.mysql.jdbc.exceptions.MySQLSyntaxErrorException e){
						j++;
						e.printStackTrace();
					}	
				}//end of while for # of queries
			
		}
		catch(Exception e)
		{
			j++;
			e.printStackTrace();
		}
	}
	
	
}
