package edu.ust.controller;

import java.io.IOException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.annotation.PostConstruct;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;



	@WebServlet("/index.html")
	public class DBControllerServlet extends HttpServlet {
		private static final long serialVersionUID = 1L;

		public Connection connection;
		
		
		
		
		
		public void init(ServletConfig config) throws ServletException {
			try
			{
				Class.forName("net.sourceforge.jtds.jdbc.Driver");
				String username = "practice";
				String password = "practice";
				String url = "jdbc:jtds:sqlserver://localhost:1433/practice";
				
				connection = DriverManager.getConnection(url, username, password);
			}	catch (ClassNotFoundException cnfe) {
				System.err.println(cnfe.getMessage());
			}
			catch (SQLException sqle){
				System.err.println(sqle.getMessage());
			}
			
		}

		
		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			doPost(request, response);
		}

		
		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			try{
				if(connection != null){
					Statement stmt = connection.createStatement();
					ResultSet rs = stmt.executeQuery("select * from employee");
					System.out.println("Successful Connection");
					
					//stmt.close();
					//connection.close();
					
					//perform binding to HttpSession
					HttpSession session = request.getSession();
					session.setAttribute("records", rs);
					
					RequestDispatcher dispatcher =
							request.getRequestDispatcher("displayRecords.jsp");
					dispatcher.forward(request, response);				
				}
				else
				{
					System.err.println("Connection is NULL.");
				}
			}
			catch (SQLException sqle){
				System.err.println("SQLException error occured");
			}
		}

	
}
