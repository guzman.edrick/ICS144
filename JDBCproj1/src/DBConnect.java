import java.io.BufferedWriter;
import java.util.Properties;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Locale;
import java.util.Scanner;
public class DBConnect{

	private static String errorLog = " ";
	public static void main(String[] args) throws IOException 
	{
		
		// TODO Auto-generated method stub
		try
		{
			System.out.println("input file name");
			Scanner cin = new Scanner(System.in);
			String input = cin.nextLine();
			String newFileName = input + ".csv";
			File newFile = new File(newFileName);
			StringBuilder sb = new StringBuilder();
			StringBuilder sbForRecords = new StringBuilder();
			BufferedWriter writer = new BufferedWriter(new FileWriter(newFile));
			FileInputStream fileInput = new FileInputStream("resources/config/config.properties");
			Properties properties = new Properties();
			properties.load(fileInput);
			Class.forName(properties.getProperty("driver")); 
			Locale.setDefault(Locale.ENGLISH);
			Connection con=DriverManager.getConnection(properties.getProperty("URL")+
													properties.getProperty("database"),
													properties.getProperty("username"),
													properties.getProperty("password"));   
			Statement stmt=con.createStatement();   
			ResultSet rs=stmt.executeQuery("select * from " + properties.getProperty("tableName")); 
			ResultSetMetaData rsmd = rs.getMetaData();
			int columnsNumber = rsmd.getColumnCount();
			DatabaseMetaData meta = con.getMetaData();
		    ResultSet res = meta.getTables(null, null, null, new String[] {"TABLE"});
		    System.out.println("List of tables: "); 
		      while (res.next()) {
		         System.out.println(res.getString("TABLE_NAME"));
		         
		      }
		      res.close();
			
			
			
			

			for(int i = 1; i<= columnsNumber; i++){
				if(i == columnsNumber){
					sb.append(rsmd.getColumnName(i));
				}
				else{
					sb.append(rsmd.getColumnName(i)+ ",");
				}
			}
			headerWriteData(sb.toString(),newFileName,newFile);
			
			while(rs.next())
			{
					for(int j = 1; j <= columnsNumber; j++){
						if(rsmd.getColumnClassName(j).toString() == "java.lang.Integer"){
						
							if(j == columnsNumber){
								sbForRecords.append(rs.getInt(j));
							}
							else{
								sbForRecords.append(rs.getInt(j)+ ",");								
							}
							
						}
						else{
							
							if(j == columnsNumber){
								sbForRecords.append(rs.getString(j));
							}
							else{
								sbForRecords.append(rs.getString(j)+ ",");									
							}
						}			
					}
					writeData(sbForRecords.toString(), newFileName, newFile);	
					sbForRecords.delete(0, sbForRecords.length());
			}
			}
			catch(Exception e){ 
				e.printStackTrace();
				String newFileName2 = "log.log";
				File newFile2 = new File(newFileName2);
				String data1 = e.toString();
				writeLog(data1, newFileName2, newFile2);
		       
		    }
	
			 finally{
				System.out.println("Task Done.");
			}
	}
				  
		

			
	
				private static void generateCsvFile(String sFileName)
				  {
					try
					{
					    FileWriter writer = new FileWriter(sFileName);		  				
					    writer.flush();
					    writer.close();
					}
					catch(IOException e)
					{
					     e.printStackTrace();
					} 
				  }
				
				public static boolean writeData(String data,String strFilePath, File file)
			    {
			        PrintWriter csvWriter;
			        
			        try
			        {
			         
			            if(!file.exists()){
			                file = new File(strFilePath);
			            }
			            csvWriter = new  PrintWriter(new FileWriter(file,true));
			            System.out.println(data + " " + " is inserted in the csv file");
			            csvWriter.print(data+",");
			            csvWriter.append('\n');
			            csvWriter.close();
			        }
			        catch (Exception e)
			        {
			            e.printStackTrace();
			        }
					return true;
			    }
				
				public static boolean headerWriteData(String headerData,String strFilePath, File file)
			    {
			        PrintWriter csvWriter;
			        
			        try
			        {
			         
			            if(!file.exists()){
			                file = new File(strFilePath);
			            }
			            csvWriter = new  PrintWriter(new FileWriter(file,true));
			            System.out.println(headerData + " " + " is inserted in the csv file");
			            csvWriter.print(headerData+",");
			            csvWriter.append('\n');
			            csvWriter.close();
			        }
			        catch (Exception e)
			        {
			            e.printStackTrace();
			            errorLog = e.toString();
			            
			        }
					return true;
			    }

				public static boolean writeLog(String data,String strFilePath, File file)
			    {
			        BufferedWriter csvWriter;
			        
			        try
			        {
			        	 if(!file.exists()){
				                file = new File(strFilePath);
			        	 }
			            csvWriter = new  BufferedWriter(new FileWriter(strFilePath,true));
			            String time = LocalDateTime.now().toString();
			            csvWriter.newLine();
				        csvWriter.write("TIME: " + time + "\t" + "ERROR DESCRIPTION: " + data.toString() + "\n");
				        csvWriter.newLine();
				        csvWriter.close();
			            System.out.print("log file inserted");
			        }
			        catch (Exception e)
			        {
			            e.printStackTrace();
			        }
					return true;
			    }
}
