
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class WritePropertiesFile {
	public static void main(String[] args) {
		try {
			Properties properties = new Properties();
			properties.setProperty("driver", "com.mysql.jdbc.Driver");
			properties.setProperty("username", "root");
			properties.setProperty("password", "p@ssword");
			properties.setProperty("URL", "jdbc:mysql://localhost:3306/");
			properties.setProperty("database", "practice");
			properties.setProperty("tableName", "emp");

			File file = new File("resources/config/config.properties");
			FileOutputStream fileOut = new FileOutputStream(file);
			properties.store(fileOut, "Property:");
			fileOut.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}