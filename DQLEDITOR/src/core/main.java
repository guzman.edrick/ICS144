package core;

import java.awt.BorderLayout;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import org.jdesktop.swingx.prompt.PromptSupport;
import java.awt.EventQueue;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.FlowLayout;

import javax.swing.JMenuBar;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JMenu;
import javax.swing.border.TitledBorder;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Toolkit;
import javax.swing.Timer;

import javax.swing.JFormattedTextField;
import javax.swing.JComboBox;
import javax.swing.ImageIcon;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JPasswordField;
import javax.swing.JTable;

public class main extends JFrame {

	private JPanel contentPane;
	private JTextField txtSecdev;
	private JTextField textField_1;
	private JTextField txtDev;
	private JTextField txtDmadmin;
	private JTextField queryTextField;
	private JTextField change;
	private JPasswordField passwordField;
	private JTable table;
	private JTable table_1;

	/**
	 * Launch the application.
	 */

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					main frame = new main();
					frame.setExtendedState(JFrame.MAXIMIZED_BOTH); 
					
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public main() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Edrick\\Desktop\\Untitled.png"));
		setTitle("DQL Editor");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		contentPane = new JPanel();
		contentPane.setBorder(null);
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(null, "Information", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_2.setBounds(10, 61, 1346, 126);
		contentPane.add(panel_2);
		panel_2.setLayout(null);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(6, 16, 1326, 39);
		panel_2.add(panel_1);
		panel_1.setLayout(null);
		
		JLabel label = new JLabel("Server Name");
		label.setBounds(319, 8, 89, 14);
		panel_1.add(label);
		
		txtSecdev = new JTextField();
		txtSecdev.setText("SEC-dev");
		txtSecdev.setBounds(395, 5, 86, 20);
		txtSecdev.setColumns(10);
		panel_1.add(txtSecdev);
		
		JLabel label_1 = new JLabel("Port");
		label_1.setBounds(485, 8, 31, 14);
		panel_1.add(label_1);
		
		textField_1 = new JTextField();
		textField_1.setText("8081");
		textField_1.setBounds(513, 5, 86, 20);
		textField_1.setColumns(10);
		panel_1.add(textField_1);
		
		JLabel label_2 = new JLabel("Repository");
		label_2.setBounds(603, 8, 73, 14);
		panel_1.add(label_2);
		
		txtDev = new JTextField();
		txtDev.setText("dev");
		txtDev.setBounds(664, 5, 86, 20);
		txtDev.setColumns(10);
		panel_1.add(txtDev);
		
		JLabel label_3 = new JLabel("Username");
		label_3.setBounds(752, 8, 67, 14);
		panel_1.add(label_3);
		
		txtDmadmin = new JTextField();
		txtDmadmin.setText("dmadmin");
		txtDmadmin.setBounds(817, 5, 86, 20);
		txtDmadmin.setColumns(10);
		panel_1.add(txtDmadmin);
		
		JLabel label_4 = new JLabel("Password");
		label_4.setBounds(908, 8, 61, 14);
		panel_1.add(label_4);
		
		passwordField = new JPasswordField();
		passwordField.setToolTipText("");
		passwordField.setBounds(968, 5, 73, 20);
		passwordField.setText("dmadmin");
		panel_1.add(passwordField);
		
		JButton btnNewButton_1 = new JButton("");
		btnNewButton_1.setIcon(new ImageIcon("C:\\Users\\Edrick\\Desktop\\1.png"));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				passwordField.setEchoChar((char)0);
				Timer t = new Timer(1200, new ActionListener() {
					
		            public void actionPerformed(ActionEvent e) {
		            	
		            	passwordField.setEchoChar('*');
		            }
				});
				t.start();
			}
		        
		});
		btnNewButton_1.setBounds(1051, 2, 31, 23);
		panel_1.add(btnNewButton_1);
		
		JPanel panel = new JPanel();
		panel.setBounds(16, 66, 1326, 48);
		panel_2.add(panel);
		panel.setLayout(null);
		
		change = new JTextField();
		change.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
			}
		});
		change.setColumns(10);
		change.setBounds(500, 11, 473, 28);
		panel.add(change);
		
		
		
		
		
		final JComboBox yearStart = new JComboBox();
		yearStart.setModel(new DefaultComboBoxModel(new String[] {"", "2016", "2015", "2014", "2013", "2012", "2011", "2010", "2009", "2008", "2007", "2006", "2005", "2004", "2003", "2002", "2001", "2000", "1999", "1998", "1997", "1996", "1995", "1994", "1993", "1992", "1991", "1990", "1989", "1988", "1987", "1986", "1985", "1984", "1983", "1982", "1981", "1980", "1979", "1978", "1977", "1976", "1975", "1974", "1973", "1972", "1971", "1970", "1969", "1968", "1967", "1966", "1965", "1964", "1963", "1962", "1961", "1960", "1959", "1958", "1957", "1956", "1955", "1954", "1953", "1952", "1951", "1950", "1949", "1948", "1947", "1946", "1945", "1944", "1943", "1942", "1941", "1940", "1939", "1938", "1937", "1936", "1935", "1934", "1933", "1932", "1931", "1930", "1929", "1928", "1927", "1926", "1925", "1924", "1923", "1922", "1921", "1920", "1919", "1918", "1917", "1916", "1915", "1914", "1913", "1912", "1911", "1910", "1909", "1908", "1907", "1906", "1905", "1904", "1903", "1902", "1901", "1900"}));
		yearStart.setBounds(570, 11, 138, 28);
		yearStart.setVisible(false);
		panel.add(yearStart);
		
		
		
		JButton generateResultsButton = new JButton("Generate Results");
		generateResultsButton.setBounds(990, 11, 326, 26);
		panel.add(generateResultsButton);
		
		final JComboBox yearEnd = new JComboBox();
		yearEnd.setModel(new DefaultComboBoxModel(new String[] {"", "2016", "2015", "2014", "2013", "2012", "2011", "2010", "2009", "2008", "2007", "2006", "2005", "2004", "2003", "2002", "2001", "2000", "1999", "1998", "1997", "1996", "1995", "1994", "1993", "1992", "1991", "1990", "1989", "1988", "1987", "1986", "1985", "1984", "1983", "1982", "1981", "1980", "1979", "1978", "1977", "1976", "1975", "1974", "1973", "1972", "1971", "1970", "1969", "1968", "1967", "1966", "1965", "1964", "1963", "1962", "1961", "1960", "1959", "1958", "1957", "1956", "1955", "1954", "1953", "1952", "1951", "1950", "1949", "1948", "1947", "1946", "1945", "1944", "1943", "1942", "1941", "1940", "1939", "1938", "1937", "1936", "1935", "1934", "1933", "1932", "1931", "1930", "1929", "1928", "1927", "1926", "1925", "1924", "1923", "1922", "1921", "1920", "1919", "1918", "1917", "1916", "1915", "1914", "1913", "1912", "1911", "1910", "1909", "1908", "1907", "1906", "1905", "1904", "1903", "1902", "1901", "1900"}));
		yearEnd.setBounds(796, 11, 138, 28);
		yearEnd.setVisible(false);
		panel.add(yearEnd);
		
		final JLabel yearFromLabel = new JLabel("From:");
		yearFromLabel.setBounds(500, 18, 46, 14);
		yearFromLabel.setVisible(false);
		panel.add(yearFromLabel);
		
		final JLabel yearToLabel = new JLabel("To:");
		yearToLabel.setBounds(740, 18, 46, 14);
		yearToLabel.setVisible(false);
		panel.add(yearToLabel);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBounds(10, 0, 1346, 62);
		contentPane.add(panel_3);
		panel_3.setLayout(null);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBorder(new TitledBorder(null, "Action", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_4.setBounds(0, 0, 1346, 56);
		panel_3.add(panel_4);
		panel_4.setLayout(null);
		
		JButton btnSettings = new JButton("Settings");
		btnSettings.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				SettingsPage sp = new SettingsPage();
				sp.setVisible(true);
				
			}
		});
		btnSettings.setBounds(10, 24, 89, 23);
		panel_4.add(btnSettings);
		
		JButton btnExportAsCsv = new JButton("Export as CSV");
		btnExportAsCsv.setBounds(97, 24, 180, 23);
		panel_4.add(btnExportAsCsv);
		
		queryTextField = new JTextField();
		queryTextField.setFont(new Font("Tahoma", Font.PLAIN, 15));
		queryTextField.setBounds(10, 198, 1348, 78);
		contentPane.add(queryTextField);
		queryTextField.setColumns(10);
		PromptSupport.setPrompt("Input Query here", queryTextField);
		PromptSupport.setFocusBehavior(PromptSupport.FocusBehavior.HIDE_PROMPT, queryTextField);
		PromptSupport.setFontStyle(Font.ITALIC, queryTextField);
		
		
		
		
		final JComboBox options = new JComboBox();
		options.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(options.getSelectedItem().toString().equals("Year of Date Registered") ||
					     options.getSelectedItem().toString().equals("Year of Date Effective")){
					change.setVisible(false);
					yearStart.setVisible(true);
					yearEnd.setVisible(true);
					yearFromLabel.setVisible(true);
					yearToLabel.setVisible(true);
					
					
				}
				else{
					change.setVisible(true);
					yearStart.setVisible(false);
					yearEnd.setVisible(false);
					yearFromLabel.setVisible(false);
					yearToLabel.setVisible(false);
				}
			}
		});
		options.setModel(new DefaultComboBoxModel(new String[] {"(Select option here)", "Company Name", "Year of Date Registered", "Year of Date Effective", "PSIC Code"}));
		options.setBounds(10, 11, 473, 28);
		panel.add(options);
		
		//extension of options combobox for generate results
		options.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				if(options.getSelectedItem().toString().equals("Company Name")) 					     
				{
					PromptSupport.setPrompt("Input company name here", change);
					PromptSupport.setFocusBehavior(PromptSupport.FocusBehavior.HIDE_PROMPT, change);
					PromptSupport.setFontStyle(Font.ITALIC, change);
				}
				else if(options.getSelectedItem().toString().equals("PSIC Code"))
				{
					PromptSupport.setPrompt("Input PSIC Code here", change);
					PromptSupport.setFocusBehavior(PromptSupport.FocusBehavior.HIDE_PROMPT, change);
					PromptSupport.setFontStyle(Font.ITALIC, change);
				}
				else if(options.getSelectedItem().toString().equals("(Select option here)")){
					PromptSupport.setPrompt("", change);
				}
			}
		});
		
		JButton btnNewButton = new JButton("Execute Query");
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 25));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnNewButton.setBounds(10, 286, 253, 30);
		contentPane.add(btnNewButton);
		
		JPanel panel_5 = new JPanel();
		panel_5.setBorder(new TitledBorder(null, null, TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_5.setBounds(273, 287, 1081, 30);
		contentPane.add(panel_5);
		panel_5.setLayout(null);
		
		JLabel lblNull = new JLabel("null");
		lblNull.setBounds(10, 0, 975, 30);
		panel_5.add(lblNull);
		
		JPanel bottomPanel = new JPanel();
		bottomPanel.setBounds(10, 327, 1361, 370);
		contentPane.add(bottomPanel);
		bottomPanel.setLayout(null);
		
		table = new JTable();
		table.setBounds(10, 347, 1330, -269);
		bottomPanel.add(table);
		
		JPanel panel_7 = new JPanel();
		panel_7.setBorder(new TitledBorder(null, null, TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_7.setBounds(4, 11, 1340, 54);
		bottomPanel.add(panel_7);
		panel_7.setLayout(null);
		
		JPanel panel_6 = new JPanel();
		panel_6.setBounds(10, 11, 1330, 32);
		panel_7.add(panel_6);
		panel_6.setLayout(null);
		
		JButton button_1 = new JButton("");
		button_1.setEnabled(false);
		button_1.setIcon(new ImageIcon("C:\\Users\\Edrick\\Desktop\\newWorkspace\\DQLEDITOR\\src\\right.png"));
		button_1.setBounds(208, 0, 38, 30);
		panel_6.add(button_1);
		
		JButton button = new JButton("");
		button.setEnabled(false);
		button.setIcon(new ImageIcon("C:\\Users\\Edrick\\Desktop\\newWorkspace\\DQLEDITOR\\src\\font-awesom2e.png"));
		button.setBounds(22, 0, 38, 30);
		panel_6.add(button);
		
		JButton button_2 = new JButton("");
		button_2.setEnabled(false);
		button_2.setIcon(new ImageIcon("C:\\Users\\Edrick\\Desktop\\newWorkspace\\DQLEDITOR\\src\\right1.png"));
		button_2.setBounds(160, 0, 38, 30);
		panel_6.add(button_2);
		
		final JButton button_3 = new JButton("");
		button_3.setEnabled(false);
		button_3.setIcon(new ImageIcon("C:\\Users\\Edrick\\Desktop\\newWorkspace\\DQLEDITOR\\src\\font-awesome (1).png"));
		button_3.setBounds(67, 0, 38, 30);
		panel_6.add(button_3);
		
		JLabel lblNewLabel = new JLabel("of {0}");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblNewLabel.setBounds(115, 0, 46, 30);
		panel_6.add(lblNewLabel);
		
		JLabel lblItems = new JLabel("Items");
		lblItems.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblItems.setBounds(279, 0, 46, 30);
		panel_6.add(lblItems);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setModel(new DefaultComboBoxModel(new String[] {"10", "100", "1000", "All"}));
		comboBox_1.setBounds(335, 0, 87, 30);
		panel_6.add(comboBox_1);
		
		JLabel lblOutputTo = new JLabel("Output to");
		lblOutputTo.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblOutputTo.setBounds(451, 0, 87, 30);
		panel_6.add(lblOutputTo);
		
		JComboBox comboBox_2 = new JComboBox();
		comboBox_2.setModel(new DefaultComboBoxModel(new String[] {"Viewer", "CSV"}));
		comboBox_2.setBounds(528, 0, 104, 30);
		panel_6.add(comboBox_2);
		
		table_1 = new JTable();
		table_1.setBounds(14, 77, 1320, 282);
		bottomPanel.add(table_1);
		btnExportAsCsv.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		
		
	}
}
