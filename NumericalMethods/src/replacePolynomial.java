import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


public class replacePolynomial {

	public static void main(String[] args)
	{
		String ex1= "log(2x)";
		String cos= "ln(2x)";
		String sin= "sin(x)";
		String e= "e^(x^3)";
		
		//System.out.println(getParameter(e));
		
		ArrayList<String> getNum = new ArrayList<String>();
		String polynomial = "2x^2 + e^(4x+3)/x^2";	
		System.out.println("InitialPolynomial: " + polynomial);
		String toReplace = findWhatToReplace(polynomial);
		System.out.println(toReplace);
		String replace = replaceE(polynomial);
		System.out.println(replace);
		String tayloredPolynomial = polynomial.replace(toReplace, replace);
		System.out.println("Taylored Polynomial: " + tayloredPolynomial);
		getNum = getNumeratorAndDenominator(tayloredPolynomial); // get num and den sa taylored polynomial
		String numerator = getNum.get(0); // get numerator
		
		
	}
	public static String distributeOperatorToTayloredPolynomial(String numerator, String replace)
	{
		String tayloredNumerator = new String();
		char[] digits = numerator.toCharArray();
		char[] toBeDist = replace.toCharArray();
		int operatorIndex = (new String(digits).indexOf("(")-1);
		Map container = new HashMap(); //for each term after madistribute
		boolean nasaLoobNgParenthesisYungOperator = false;
		switch(operatorIndex){
		
		case('+'):
			String[] terms = replace.split("\\+|\\-");
			ArrayList<String> chars = new ArrayList<String>();
			
			for(int i = 0; i<=toBeDist.length;i++)
			{
				if(i == '(' && i >2)  
				{
					nasaLoobNgParenthesisYungOperator = true;
				}
				else if(i == ')')
				{
					nasaLoobNgParenthesisYungOperator = false;
				}
				else if((toBeDist[i] == '+' || toBeDist[i] == '-') && nasaLoobNgParenthesisYungOperator == false)
				{
					chars.add(Character.toString(toBeDist[i]));
				}	
			}
			
		case('-'):
			
		}
		return tayloredNumerator;
	}
	
//	public static String distributeOperatorToTayloredPolynomialAlg2(String numerator, String replace)
//	{
//		String tayloredNumerator = new String();
//		
//		char[] digits = numerator.toCharArray();
//		char[] toBeDist = replace.toCharArray();
//		int operatorIndex = (new String(digits).indexOf("(")-1);
//		String beforeDistOperator =  numerator.substring(0,operatorIndex-1);
//		String toBeDist = numerator.substring(operatorIndex,numerator.length()-2);
//		Map container = new HashMap(); //for each term after madistribute
//		boolean nasaLoobNgParenthesisYungOperator = false;
//		switch(operatorIndex){
//		
//		case('+'):
//			String[] terms = replace.split("\\+|\\-");
//			ArrayList<String> chars = new ArrayList<String>();
//			
//			for(int i = 0; i<=toBeDist.length;i++)
//			{
//				if(i == '(' && i >2)  
//				{
//					nasaLoobNgParenthesisYungOperator = true;
//				}
//				else if(i == ')')
//				{
//					nasaLoobNgParenthesisYungOperator = false;
//				}
//				else if((toBeDist[i] == '+' || toBeDist[i] == '-') && nasaLoobNgParenthesisYungOperator == false)
//				{
//					chars.add(Character.toString(toBeDist[i]));
//				}	
//			}
//			
//		case('-'):
//			
//		}
//		return tayloredNumerator;
//	}
	//method for separating numerator and denominator 
	//Arraylist index 0 = numerator & index 1 = denominator
	public static ArrayList<String> getNumeratorAndDenominator(String polynomial)
	{
		char[] digits = polynomial.toCharArray();
		ArrayList<String> numAndDen = new ArrayList<String>();
		int index = (new String(digits).indexOf("/"));
		numAndDen.add(Arrays.toString(digits).replaceAll(", ", "").substring(1,index+1));
		numAndDen.add(Arrays.toString(digits).replaceAll(", ", "").substring(index+2,digits.length+1));
		
		return numAndDen;
		
	}
	
	public static String findWhatToReplace(String polynomial)
	{
		String toReplace = new String();
		char[] digits = polynomial.toCharArray();
		
		if(polynomial.contains("e")){
			int start = (new String(digits).indexOf("e"));
			int end = (new String(digits).indexOf(")"));
			toReplace = Arrays.toString(digits).replaceAll(", ", "").substring(start+1,end+2);
		}
		
		return toReplace;
			
	}
	
	public static String getParameter(String polynomial){
		String parameter = new String();
		char[] digits =  polynomial.toCharArray();
		int indexStart = (new String(digits).indexOf("("));
		int indexEnd = (new String(digits).indexOf(")"));
		parameter = Arrays.toString(digits).replaceAll(", ", "").substring(indexStart+2,indexEnd+1);
		return parameter;
	}
	
	public static String replaceE(String polynomial)
	{
		int highestDegree = findHighestOrder(polynomial);
		String toReplace = findWhatToReplace(polynomial);
		String eParameter = getParameter(toReplace);
		String eTaylors = new String();
		StringBuilder multiTaylor = new StringBuilder();
		if(highestDegree == 0)
		{
			return null;
		}
		
		else if(highestDegree == 1)
		{
			return "1+" + eParameter;
			
		}
		else{
			multiTaylor.append("1+" + eParameter + "+");
			for(int i = 2; i<= highestDegree;i++)
			{	
				if(i == highestDegree){
					multiTaylor.append("((" + eParameter+ ")^" + i +"/" + i +"!)");
				}
				else{
					multiTaylor.append("((" + eParameter+ ")^" + i +"/" + i +"!)+");
				}
			}
			
			eTaylors = multiTaylor.toString();
		}
		
		return eTaylors;
	}
	
	public static String replaceSin(String sinParameter, int highestDegree)
	{
		String sinTaylors = new String();
		StringBuilder multiTaylor = new StringBuilder();
		
		//to know if plus or minus
		// true = plus ; false = minus;
		boolean plusOrMinus = false;
		if(highestDegree == 0)
		{
			return null;
		}
		
		else if(highestDegree == 1|| highestDegree ==2)
		{
			return "x";
			
		}
		else
		{
			multiTaylor.append("x");
			for(int i = 2; i<= highestDegree;)
			{	
				if(i % 2 == 0){
					i++;
				}
				else
				{
					if(plusOrMinus == true){
							multiTaylor.append("+((" + sinParameter+ ")^" + i +"/" + i +"!)");
					}
					
					else
					{
							multiTaylor.append("-((" + sinParameter+ ")^" + i +"/" + i +"!)");
					}
					plusOrMinus = !plusOrMinus;
					i++;		
				}	
			}
				
				
		}
			
			sinTaylors = multiTaylor.toString();
		
		
		return sinTaylors;
	}
	
	
	
	public static String replaceCos(String cosParameter, int highestDegree)
	{
		String cosTaylors = new String();
		StringBuilder multiTaylor = new StringBuilder();
		
		//to know if plus or minus
		// true = plus ; false = minus;
		boolean plusOrMinus = false;
		if(highestDegree == 0)
		{
			return null;
		}
		
		else if(highestDegree == 1)
		{
			return "1";
			
		}
		else
		{
			multiTaylor.append("1");
			for(int i = 2; i<= highestDegree;)
			{	
				if(i % 2 != 0)
				{
					i++;
				}
				else
				{
					if(plusOrMinus == true){
							multiTaylor.append("+((" + cosParameter+ ")^" + i +"/" + i +"!)");
					}
					else
					{
							multiTaylor.append("-((" + cosParameter+ ")^" + i +"/" + i +"!)");
					}
					plusOrMinus = !plusOrMinus;
					i++;		
				}	
			}
		}
		cosTaylors = multiTaylor.toString();
		return cosTaylors;
	}
	
	
	//method to find the highest degree of a function to be used pag nag taylors
	public static int findHighestOrder(String polynomial){
		char[] digits = polynomial.toCharArray();
		int highestDegree = 0;
		
		for (int i = 0; i <= digits.length-1;i++)
		{
			if(i > new String(digits).indexOf("/"))
			{
				if(digits[i] == '^')
				{
					if(Character.getNumericValue(digits[i+1]) > highestDegree)
					{
						highestDegree = Character.getNumericValue(digits[i+1]);
						
					}
					
				}
			}
			
		}
		return highestDegree;
	}
}
