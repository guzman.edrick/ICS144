import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;
import bsh.Interpreter;


public class factoring {
	
	public static void main(String[] args)
	{
		String polynomial = "1-1+(x^2)/2!/x^2";
		System.out.println("Polynomial:" + polynomial);
		
		
		String exponentsDistributedPolynomials = distributeExponent(polynomial);
		System.out.println("Distributed Polynomials:" + exponentsDistributedPolynomials);
		
		String evaluatedFactorial = evaluateFactorial(exponentsDistributedPolynomials);
		System.out.println("No Factorial Polynomial:" + evaluatedFactorial);

		
		ArrayList<String> numAndDen = getNumeratorAndDenominator(evaluatedFactorial);
		String numerator = numAndDen.get(0);
		String denominator = numAndDen.get(1);
		String noConstantPolynomial = checkForConstants(numerator);
		System.out.println("No constant Polynomial:" + noConstantPolynomial);

		
		String factoredPolynomial=factorPolynomial(noConstantPolynomial);
		System.out.println("Factored Polynomial: " + factoredPolynomial);

		
		String canceledDenominator = cancelDenominator(factoredPolynomial,denominator);
		System.out.println("Canceled Denominator: " + canceledDenominator);

		
		canceledDenominator = canceledDenominator.substring(1,canceledDenominator.length());
		double answer = finalEvaluation(canceledDenominator);
		System.out.println("ANSWER: " + answer);

		
		
//		System.out.println("no X polynomial: " + plugInX(canceledDenominator));
//		String noXPolynomial = plugInX(canceledDenominator);
//		evaluateNoX(noXPolynomial);
		
		
		

//		
		
		
		
	}
	
	public static String evaluateFactorial(String newPolynomial)
	{
		char[] digits = newPolynomial.toCharArray();
		for(int i = 0; i <= digits.length-1;i++)
		{
			if(digits[i] == '!' && Character.isDigit(digits[i-1]) == true)
			{
				StringBuilder sb = new StringBuilder();
				sb.append(digits[i-1]);
				sb.append(digits[i]);
				String insert = sb.toString();
				int numberToUndergoFactorial = factorialMethod(Character.getNumericValue(digits[i-1]));
				newPolynomial = newPolynomial.replace(insert, Integer.toString(numberToUndergoFactorial));
				
			}
		}
		
		
		return newPolynomial;
	}
	
	public static Integer factorialMethod(int number)
	{
		int fact =1;
		for(int i=1;i<=number;i++)
		{
			fact= fact*i;
		}
		return fact;
	}
	
	public static String distributeExponent(String polynomial)
	{
		char[] digits = polynomial.toCharArray();
		
		for(int i = 0; i <= digits.length-1;i++)
		{
			if(digits[i] == ')' && Character.isDigit(digits[i+2]) == true)
			{
				StringBuilder sb = new StringBuilder();
				sb.append(digits[i-1]);
				sb.append(digits[i]);
				sb.append(digits[i+1]);
				sb.append(digits[i+2]);
				String insert = sb.toString();
				int insideExp = Character.getNumericValue(digits[i-1]);
				int outsideExp = Character.getNumericValue(digits[i+2]);
				int answer = outsideExp + insideExp;
				polynomial = polynomial.replace(insert, answer+")");
			}
		}
		
		return polynomial;	
	}
	public static double finalEvaluation(String canceledDenominator)
	{
		double answer = 0;
		ExecutorService exec = Executors.newFixedThreadPool(1);
		Expression e = new ExpressionBuilder(canceledDenominator)
		        .variables("x")
		        .build()
		        .setVariable("x", 0);
		Future<Double> future = e.evaluateAsync(exec);
		try {
			double result = future.get();
			System.out.println("answer = " + result);
			answer = result;
			return answer;
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ExecutionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return answer;

	}
//	public static String evaluateNoX(String noXPolynomial)
//	{
//		String answer = new String();
//		String[] parts = noXPolynomial.split("(?=[/*])|(?<=[/*])");
//		for(int i =0; i <= parts.length-1;i++)
//		{
//			System.out.println(parts[i]);
//		}
//		
//		return answer;
//	}
	
	public static String plugInX(String canceledDenominator)
	{
		String noXPolynomial = new String();
		noXPolynomial = canceledDenominator.replace("x", "0");
		noXPolynomial = noXPolynomial.substring(1,noXPolynomial.length());
		noXPolynomial = noXPolynomial.replace("^[0-9]+", "");
		
		char[] digits = noXPolynomial.toCharArray();
		for(int i = 0; i<=digits.length-1;i++)
		{
			if(digits[i] == '^')
			{
				digits[i] = '?';
				digits[i+1] = '?';
			}
		}
		
		noXPolynomial = String.valueOf(digits);
		noXPolynomial = noXPolynomial.replace("?", "");
		return noXPolynomial;
	}
	
	public static ArrayList<String> getNumeratorAndDenominator(String polynomial)
	{
		char[] digits = polynomial.toCharArray();
		ArrayList<String> numAndDen = new ArrayList<String>();
		boolean insidePar = true;
		int indexOfDivide = 0;
		for(int i = 0; i<= digits.length-1; i++)
		{
			if(digits[i] == '(')
			{
				insidePar = true;
			}
			else if(digits[i] == ')'){
				insidePar = false;
			}
			else if(insidePar == false && digits[i] == '/')
			{
				indexOfDivide = i;
			}
		}
		numAndDen.add(Arrays.toString(digits).replaceAll(", ", "").substring(1,indexOfDivide+1));
		numAndDen.add(Arrays.toString(digits).replaceAll(", ", "").substring(indexOfDivide+2,digits.length+1));

		return numAndDen;
		
	}
	public static String cancelDenominator(String factoredPolynomial,String denominator)
	{
		String noXDenominator = new String();
		char[] digits = factoredPolynomial.toCharArray();
		int indexEndOfFactorCandidate = (new String(digits).indexOf("k"));
		String numToBeCanceled = factoredPolynomial.substring(0,indexEndOfFactorCandidate);		
		String remainingValues = factoredPolynomial.substring(indexEndOfFactorCandidate+1, digits.length);	
		if(numToBeCanceled.equals(denominator))
		{
			noXDenominator = remainingValues;
		}
		return noXDenominator;
	}
	
	public static String checkForConstants(String polynomial){
		String noConsString = new String();
		char[] digits = polynomial.toCharArray();
		ArrayList<Integer> constants = new ArrayList<Integer>();
		
		for(int i= 0; i <= digits.length-1;i++)
		{
			
			if(i !=0)
			{
				if(Character.isDigit(digits[i]) == true && digits[i-1] != '^' && digits[i-1] != '/' && digits[i+1] != ')'
						&& Character.isDigit(digits[i+1]) ==false && Character.isDigit(digits[i-1]) ==false)
				{
					if(digits[i-1] == '-')
					{
						constants.add(Character.getNumericValue(digits[i])*-1);
					}
					else{
						constants.add(Character.getNumericValue(digits[i]));
					}
				}
			}
			else{
				if((Character.isDigit(digits[i]) == true && ((digits[i+1]=='+' || digits[i+1] =='-') && digits[i+1] != 'x')))
				{
					constants.add(Character.getNumericValue(digits[i]));
				}
			}
		}
		
		//calculate constants
		int sum =0;
		for(int i=0; i<=constants.size()-1;i++)
		{
			sum = sum + constants.get(i);
		}
		
		//eliminate constants in polynomial
		for(int i= 0; i <= digits.length-1;i++)
		{
			if(i != 0)
			{
				if(Character.isDigit(digits[i]) == true && digits[i-1] != '^' && digits[i-1] != '/' && digits[i+1] != ')'
						&& Character.isDigit(digits[i+1]) ==false && Character.isDigit(digits[i-1]) ==false)
				{
					digits[i] = '?';
					digits[i-1] ='?';
				}
			}
			else
			{
				digits[0] = '?';
				
			}
		}
		String polyWithoutSpaces = String.valueOf(digits);
		if(sum == 0){
			noConsString = polyWithoutSpaces.replace("?", "");
			noConsString = noConsString.substring(1,noConsString.length());
		}
		else{
		noConsString = sum + polyWithoutSpaces.replace("?", ""); 
		}
		return noConsString;
	}
	
	
	public static int findFactorCandidate(String polynomial)
	{
		char[] digits = polynomial.toCharArray();
		int factorCandidate = 100;
		
		
		for(int i = 0; i <= digits.length-1;i++)
		{
			if(digits[i] == 'x' && digits[i+1] != '^')
			{
				factorCandidate = 1;
			
			}
			else if(digits[i] == '^'&& Character.isDigit(digits[i+2]) ==false)
			{
				if(Integer.parseInt(Character.toString(digits[i+1])) < factorCandidate)
				{
					factorCandidate = Integer.parseInt(Character.toString(digits[i+1]));
				}
			}
			
		}
		return factorCandidate;
	}
	
	public static String factorPolynomial(String polynomial)
	{
		
		int factorCandidate = findFactorCandidate(polynomial); //hahanapin yung highest na pwede ifactor out
		String factoredPolynomial =new String();
		StringBuilder sb = new StringBuilder();
		char[] digits = polynomial.toCharArray();
		
		if(factorCandidate == 1) 	//pag 1 lang yung highes na pwede ifactor out, mamiminus one lang lahat ng exponent
		{
			sb.append("xk(");		// yung k marereplace sya ng "" sa dulo so nevermind yung 'k' space lang yon
			for(int i = 0; i <= digits.length-1; i++) 		//iiterate nya yung buong polynomial char by char
			{
				if(digits[i] == 'x' && digits[i+1] != '^')	//pag nakakita sya ng x tas walang exponent papalitan nya ng 1
				{	
					StringBuilder sbInitial = new StringBuilder(); //dito iistore yung papalitan ng polynomial.replace();
					
					for(int j = i;  j <= digits.length-1; ){
						//kunin lahat ng digits (this is for numbers na may more than 1 chars (ex. 120,24)
						if(digits[j+1] == ')' || digits[j+1] == '+' || digits[j+1] == '-')
						{
							break;		
						}
						else
						{
							sbInitial.append(digits[j+1]);
							j++;
						}
					}
					polynomial = polynomial.replace(sbInitial.toString(), "1");
				}
				
				else if(digits[i] == '^')	// pag nakakita sya ng x na may exponent babawasan nya ng 1 yung exponent kasi factorCandidate = 1
				{
					StringBuilder sbInitial = new StringBuilder();//dito iistore yung papalitan ng polynomial.replace();
					for(int j = i;  j <= digits.length-1; ){
						if(digits[j+1] == ')' || digits[j+1] == '+' || digits[j+1] == '-')
						{
							break;
						}
						else
						{
							sbInitial.append(digits[j+1]);
							j++;
						}
					}
					
					int difference = Integer.parseInt(sbInitial.toString())-1;
					polynomial = polynomial.replace("^" + sbInitial.toString(),"^" + Integer.toString(difference));							
				}
				
			}
		}
			
			else
			{
				sb.append("x^" + factorCandidate + "k(");
				for(int i = 0; i <= digits.length-1; i++)
				{
					if(digits[i] == 'x' && digits[i+1] != '^')
					{	
						StringBuilder sbInitial = new StringBuilder();
						int j = i;
						do{
							sbInitial.append(digits[j]);
							j++;
						}while(digits[j] != ')');
						polynomial = polynomial.replace(sbInitial.toString(), "1");
					}
					
					else if(digits[i] == '^')
					{
						StringBuilder sbInitial = new StringBuilder();
						
						for(int j = i;  j <= digits.length-1; ){
							if(digits[j+1] == ')' || digits[j+1] == '+' || digits[j+1] == '-')
							{
								break;
							}
							else
							{
								sbInitial.append(digits[j+1]);
								j++;
							}
						}
						
						int difference = Integer.parseInt(sbInitial.toString())-factorCandidate; // pag di 1 yung highestFactor imiminus yung mga exponents ng factorCandidate
						polynomial = polynomial.replace("^" + sbInitial.toString(), "^"+Integer.toString(difference));								
					}
				}
			
			}
		factoredPolynomial = sb.toString() + polynomial;
		factoredPolynomial = factoredPolynomial.replace("x^0", "1");
		factoredPolynomial = factoredPolynomial.replace("x^1", "x");
		
		return factoredPolynomial;
			
	}
}
