import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


public class highestDegree {
	public static void main(String[] args)
	{
		String initialpolynomial = "x+(1+x+((x^2)/2))  /x^2";
		String polynomial = initialpolynomial.replaceAll("\\s+", "");
//		System.out.println(polynomial);
//		int highestDegree = findHighestOrder(polynomial);
//		System.out.println("Highest Degree: " + highestDegree);
		ArrayList<String> numAndDen = getNumeratorAndDenominator(polynomial);
		String numerator = numAndDen.get(0);
		char[] digits = numerator.toCharArray();
//		System.out.println("Numerator:" + numAndDen.get(0));
		int operatorIndex = (new String(digits).indexOf("(")-1);
		String beforeDistOperator =  numerator.substring(0,operatorIndex);
		String toBeDist = numerator.substring(operatorIndex+2,numerator.length());
		System.out.println(beforeDistOperator);
		System.out.println(toBeDist);
		
//		System.out.println("Denominator:" + numAndDen.get(1));
	}
	
	
	
	
	public static String distributeOperatorToTayloredPolynomialAlg2(String numerator, String replace)
	{
		String tayloredNumerator = new String();
		char[] digits = numerator.toCharArray();
		int operatorIndex = (new String(digits).indexOf("(")-1);
		String beforeDistOperator =  numerator.substring(0,operatorIndex);
		String toBeDist = numerator.substring(operatorIndex+2,numerator.length());
		
//		if(digits[operatorIndex] == '+')
//		{
//			for(int i = operatorIndex; i>=numerator.length());
//		}
		
		
		
		
		
		return tayloredNumerator;
	}
	
	//method for separating numerator and denominator 
	//Arraylist index 0 = numerator & index 1 = denominator
	public static ArrayList<String> getNumeratorAndDenominator(String polynomial)
	{
		char[] digits = polynomial.toCharArray();
		ArrayList<String> numAndDen = new ArrayList<String>();
		boolean insidePar = true;
		int indexOfDivide = 0;
		for(int i = 0; i<= digits.length-1; i++)
		{
			if(digits[i] == '(')
			{
				insidePar = true;
			}
			else if(digits[i] == ')'){
				insidePar = false;
			}
			else if(insidePar == false && digits[i] == '/')
			{
				indexOfDivide = i;
			}
		}
		numAndDen.add(Arrays.toString(digits).replaceAll(", ", "").substring(1,indexOfDivide+1));
		numAndDen.add(Arrays.toString(digits).replaceAll(", ", "").substring(indexOfDivide+2,digits.length+1));

		return numAndDen;
		
	}
	
	public static int findHighestOrder(String polynomial){
		char[] digits = polynomial.toCharArray();
		int highestDegree = 0;
		
		for (int i = 0; i <= digits.length-1;i++)
		{
			if(i > new String(digits).indexOf("/"))
			{
				if(digits[i] == '^')
				{
					if(Character.getNumericValue(digits[i+1]) > highestDegree)
					{
						highestDegree = Character.getNumericValue(digits[i+1]);
						
					}
					
				}
			}
			
		}
		return highestDegree;
	}
	
	
	
}
