import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class distributeExpAndSignsForPolynomial {

	public static void main(String[] args) 
	{
		String polynomial = "x^6+((x^8)/2)+((x^9)/6)+((x^10)/24)+((x^11)/120)+((x^12)/720)";
		//String newPoly = distributeExponent(polynomial);
//		System.out.println("Original Polynomial: " + polynomial);
//		System.out.println("Distributed Exponent: " + newPoly);
//		System.out.println("No Factorial Polynomial: " + evaluateFactorial(newPoly));
		System.out.println("Factor Polynomial: " + factorPolynomial(polynomial));
		
	}
	
	public static String distributeExponent(String polynomial)
	{
		String newPolynomial = new String();
		char[] digits = polynomial.toCharArray();
		
		for(int i = 0; i <= digits.length-1;i++)
		{
			if(digits[i] == ')' && Character.isDigit(digits[i+2]) == true)
			{
				StringBuilder sb = new StringBuilder();
				sb.append(digits[i-1]);
				sb.append(digits[i]);
				sb.append(digits[i+1]);
				sb.append(digits[i+2]);
				String insert = sb.toString();
				int insideExp = Character.getNumericValue(digits[i-1]);
				int outsideExp = Character.getNumericValue(digits[i+2]);
				int answer = outsideExp + insideExp;
				polynomial = polynomial.replace(insert, answer+")");
			}
		}
		
		return polynomial;	
	}
	
	public String evaluateFactorial(String newPolynomial)
	{
		char[] digits = newPolynomial.toCharArray();
		for(int i = 0; i <= digits.length-1;i++)
		{
			if(digits[i] == '!' && Character.isDigit(digits[i-1]) == true)
			{
				StringBuilder sb = new StringBuilder();
				sb.append(digits[i-1]);
				sb.append(digits[i]);
				String insert = sb.toString();
				int numberToUndergoFactorial = factorialMethod(Character.getNumericValue(digits[i-1]));
				newPolynomial = newPolynomial.replace(insert, Integer.toString(numberToUndergoFactorial));
				
			}
		}
		
		
		return newPolynomial;
	}
	public static String factorPolynomial(String polynomial)
	{
		int factorCandidate = findFactorCandidate(polynomial);
		String factoredPolynomial = new String();
		char[] digits  = polynomial.toCharArray();
		StringBuilder sb = new StringBuilder();
		if(factorCandidate == 1)
		{
			
			for(int i = 0; i <= digits.length-1;i++)
			{
				if(digits[i] == 'x' && digits[i+1] != '^')
				{
					polynomial = polynomial.replace(digits[i], '1');
				
				}
				else if(digits[i] == '^')
				{
					polynomial = polynomial.replace(digits[i+1],Integer.toString((Character.getNumericValue(digits[i+1]-1))).charAt(0));
					
				}
				
			}
			polynomial = "xk(" + polynomial;
		}
		else{
			
			for(int i = 0; i <= digits.length-1;i++)
			{
				if(digits[i] == 'x' && digits[i+1] != '^')
				{
					digits[i] =(char)1;
				
				}
				else if(digits[i] == '^' && Character.isDigit(digits[i+2]) == false)
				{
					polynomial = polynomial.replace(digits[i+1], Integer.toString((Character.getNumericValue(digits[i+1]-factorCandidate))).charAt(0));
					//digits[i+1] =Integer.toString((Character.getNumericValue(digits[i+1]-factorCandidate))).charAt(0);
				}
				else if(digits[i] == '^' && Character.isDigit(digits[i+2]) == true)
				{
					StringBuilder sb2 = new StringBuilder();
					sb2.append(digits[i+1]);
					sb2.append(digits[i+2]);
					int insert = (Integer.parseInt(sb2.toString())-factorCandidate);
					polynomial = polynomial.replace(sb2.toString(),Integer.toString(insert));
				}
				
			}
			polynomial = "x^" + factorCandidate+"k(" + polynomial;
		}
		
//		sb.append(String.valueOf(digits));
//		factoredPolynomial = sb.toString();
		
		polynomial = polynomial.replace("x^0", "1");
		polynomial = polynomial.replace("x^1", "x");
		return polynomial;
	}
	
	public static int findFactorCandidate(String polynomial)
	{
		char[] digits = polynomial.toCharArray();
		int factorCandidate = 100;
		
		
		for(int i = 0; i <= digits.length-1;i++)
		{
			if(digits[i] == 'x' && digits[i+1] != '^')
			{
				factorCandidate = 1;
			
			}
			else if(digits[i] == '^'&& Character.isDigit(digits[i+2]) ==false)
			{
				if(Integer.parseInt(Character.toString(digits[i+1])) < factorCandidate)
				{
					factorCandidate = Integer.parseInt(Character.toString(digits[i+1]));
				}
			}
			
		}
		return factorCandidate;
	}
	
	public static Integer factorialMethod(int number)
	{
		int fact =1;
		for(int i=1;i<=number;i++)
		{
			fact= fact*i;
		}
		return fact;
	}
}
