
public class factorPoly {
	
	public static void main(String[] args)
	{
		// yung value ng polynomial is yung ready to factor na
		String polynomial = "x^6+((x^8)/2)+((x^9)/6)+((x^10)/24)+((x^11)/120)+((x^12)/720)";
		System.out.println("Factor Polynomial: " + factorPolynomial2(polynomial));
	}
	
	public static String factorPolynomial2(String polynomial)
	{
		
		int factorCandidate = findFactorCandidate(polynomial); //hahanapin yung highest na pwede ifactor out
		String factoredPolynomial =new String();
		StringBuilder sb = new StringBuilder();
		char[] digits = polynomial.toCharArray();
		
		if(factorCandidate == 1) 	//pag 1 lang yung highes na pwede ifactor out, mamiminus one lang lahat ng exponent
		{
			sb.append("xk(");		// yung k marereplace sya ng "" sa dulo so nevermind yung 'k' space lang yon
			for(int i = 0; i <= digits.length-1; i++) 		//iiterate nya yung buong polynomial char by char
			{
				if(digits[i] == 'x' && digits[i+1] != '^')	//pag nakakita sya ng x tas walang exponent papalitan nya ng 1
				{	
					StringBuilder sbInitial = new StringBuilder(); //dito iistore yung papalitan ng polynomial.replace();
					
					for(int j = i;  j <= digits.length-1; ){
						//kunin lahat ng digits (this is for numbers na may more than 1 chars (ex. 120,24)
						if(digits[j+1] == ')' || digits[j+1] == '+' || digits[j+1] == '-')
						{
							break;		
						}
						else
						{
							sbInitial.append(digits[j+1]);
							j++;
						}
					}
					polynomial = polynomial.replace(sbInitial.toString(), "1");
				}
				
				else if(digits[i] == '^')	// pag nakakita sya ng x na may exponent babawasan nya ng 1 yung exponent kasi factorCandidate = 1
				{
					StringBuilder sbInitial = new StringBuilder();//dito iistore yung papalitan ng polynomial.replace();
					for(int j = i;  j <= digits.length-1; ){
						if(digits[j+1] == ')' || digits[j+1] == '+' || digits[j+1] == '-')
						{
							break;
						}
						else
						{
							sbInitial.append(digits[j+1]);
							j++;
						}
					}
					
					int difference = Integer.parseInt(sbInitial.toString())-1;
					polynomial = polynomial.replace("^" + sbInitial.toString(),"^" + Integer.toString(difference));							
				}
				
			}
		}
			
			else
			{
				sb.append("x^" + factorCandidate + "k(");
				for(int i = 0; i <= digits.length-1; i++)
				{
					if(digits[i] == 'x' && digits[i+1] != '^')
					{	
						StringBuilder sbInitial = new StringBuilder();
						int j = i;
						do{
							sbInitial.append(digits[j]);
							j++;
						}while(digits[j] != ')');
						polynomial = polynomial.replace(sbInitial.toString(), "1");
					}
					
					else if(digits[i] == '^')
					{
						StringBuilder sbInitial = new StringBuilder();
						
						for(int j = i;  j <= digits.length-1; ){
							if(digits[j+1] == ')' || digits[j+1] == '+' || digits[j+1] == '-')
							{
								break;
							}
							else
							{
								sbInitial.append(digits[j+1]);
								j++;
							}
						}
						
						int difference = Integer.parseInt(sbInitial.toString())-factorCandidate; // pag di 1 yung highestFactor imiminus yung mga exponents ng factorCandidate
						polynomial = polynomial.replace("^" + sbInitial.toString(), "^"+Integer.toString(difference));								
					}
				}
			
			}
		factoredPolynomial = sb.toString() + polynomial;
		factoredPolynomial = factoredPolynomial.replace("x^0", "1");
		factoredPolynomial = factoredPolynomial.replace("x^1", "x");
		factoredPolynomial = factoredPolynomial.replace("k", "");//tanggalin lahat ng k
		return factoredPolynomial;
			
	}

	public static String factorPolynomial(String polynomial)
	{
		int factorCandidate = findFactorCandidate(polynomial);
		String factoredPolynomial = new String();
		char[] digits  = polynomial.toCharArray();
		StringBuilder sb = new StringBuilder();
		if(factorCandidate == 1)
		{
			
			for(int i = 0; i <= digits.length-1;i++)
			{
				if(digits[i] == 'x' && digits[i+1] != '^')
				{
					polynomial = polynomial.replace(digits[i], '1');
				
				}
				else if(digits[i] == '^')
				{
					polynomial = polynomial.replace(digits[i+1],Integer.toString((Character.getNumericValue(digits[i+1]-1))).charAt(0));
					
				}
				
			}
			polynomial = "xk(" + polynomial;
		}
		else{
			
			for(int i = 0; i <= digits.length-1;i++)
			{
				if(digits[i] == 'x' && digits[i+1] != '^')
				{
					digits[i] =(char)1;
				
				}
				else if(digits[i] == '^' && Character.isDigit(digits[i+2]) == false)
				{
					polynomial = polynomial.replace(digits[i+1], Integer.toString((Character.getNumericValue(digits[i+1]-factorCandidate))).charAt(0));
					//digits[i+1] =Integer.toString((Character.getNumericValue(digits[i+1]-factorCandidate))).charAt(0);
				}
				else if(digits[i] == '^' && Character.isDigit(digits[i+2]) == true)
				{
					StringBuilder sb2 = new StringBuilder();
					sb2.append(digits[i+1]);
					sb2.append(digits[i+2]);
					int insert = (Integer.parseInt(sb2.toString())-factorCandidate);
					polynomial = polynomial.replace(sb2.toString(),Integer.toString(insert));
				}
				
			}
			polynomial = "x^" + factorCandidate+"k(" + polynomial;
		}
		
//		sb.append(String.valueOf(digits));
//		factoredPolynomial = sb.toString();
		
		polynomial = polynomial.replace("x^0", "1");
		polynomial = polynomial.replace("x^1", "x");
		return polynomial;
	}
	public static int findFactorCandidate(String polynomial)
	{
		char[] digits = polynomial.toCharArray();
		int factorCandidate = 100;
		
		
		for(int i = 0; i <= digits.length-1;i++)
		{
			if(digits[i] == 'x' && digits[i+1] != '^')
			{
				factorCandidate = 1;
			
			}
			else if(digits[i] == '^'&& Character.isDigit(digits[i+2]) ==false)
			{
				if(Integer.parseInt(Character.toString(digits[i+1])) < factorCandidate)
				{
					factorCandidate = Integer.parseInt(Character.toString(digits[i+1]));
				}
			}
			
		}
		return factorCandidate;
	}
}
