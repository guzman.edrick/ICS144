package conversion.helper;

import conversion.model.CurrencyBean;

public class Factory {

	public static CurrencyBean getInstance(double phpAmount, 
		String currencyType) {
		
		CurrencyBean currency = new CurrencyBean();
		currency.setPhpAmount(phpAmount);
		currency.setCurrencyType(currencyType);
		
		currency.convert();
		return currency;
	}
}