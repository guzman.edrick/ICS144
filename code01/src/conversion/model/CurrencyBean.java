package conversion.model;

public class CurrencyBean {

	private double phpAmount;
	private String currencyType;
	private double result;
	
	public double getPhpAmount() {
		return phpAmount;
	}
	public void setPhpAmount(double phpAmount) {
		this.phpAmount = phpAmount;
	}
	public String getCurrencyType() {
		return currencyType;
	}
	public void setCurrencyType(String currencyTye) {
		this.currencyType = currencyTye.toUpperCase();
	}
	public double getResult() {
		return result;
	}
	public void setResult(double result) {
		this.result = result;
	}
	
	public void convert() {
		switch(getCurrencyType()) {
			case "US$":
				setResult(getPhpAmount() / 46.660);
				break;
			case "EURO":
				setResult(getPhpAmount() / 52.8400);
				break;
			case "AUS$":
				setResult(getPhpAmount() / 35.1500);
				break;
			case "YEN":
				setResult(getPhpAmount() / 0.3816);
				break;
		}
	}
}
