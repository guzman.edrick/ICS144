<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<jsp:useBean id="exchangeCurrency" type="conversion.model.CurrencyBean" 
	scope="request"/>
    
<!DOCTYPE html>
<html>
<head>
<title>Welcome to BPI Forex</title>
</head>
<body>
	<img src="images/forex.JPG"/>
	<h1>Welcome to BPI Forex</h1>
	<h2>Here are the values</h2>

	<h2>Display using JSP Expression</h2>
	<p>Philippine Peso Amount: PHP<b><%= exchangeCurrency.getPhpAmount() %> </b></p>
	<p>Currency Type: <b> <%= exchangeCurrency.getCurrencyType() %></b></p>
	<p>Converted Amount: PHP<b><%= exchangeCurrency.getResult() %> </b></p>
	
	<hr/>
	<h2>Display using JSP getProperty</h2>
	<p>Philippine Peso Amount: PHP<b> <jsp:getProperty property="phpAmount" name="exchangeCurrency"/> </b></p>
	<p>Currency Type: <b> <jsp:getProperty property="currencyType" name="exchangeCurrency"/> </b></p>
	<p>Converted Amount: PHP<b> <jsp:getProperty property="result" name="exchangeCurrency"/> </b></p>
	
	<hr/>
	<h2>Display using JSP out</h2> <!--  this is something you already know -->
	<p>Philippine Peso Amount: PHP<b><%out.print(exchangeCurrency.getPhpAmount()); %>  </b></p>
	<p>Currency Type: <b> <%out.print(exchangeCurrency.getCurrencyType()); %> </b></p>
	<p>Converted Amount: PHP<b> <%out.print(exchangeCurrency.getResult()); %> </b></p>
	
	<hr/>
	<form action="index.jsp" action="post">
		<input type="submit" value=" << GO BACK >>">
	</form>
</body>
</html>