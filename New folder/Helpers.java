package ib.commons.uploader.utilities.helpers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.InputStream;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;


import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.google.gwt.i18n.client.LocaleInfo;
import com.google.gwt.i18n.shared.DateTimeFormat;
import com.google.gwt.i18n.shared.DateTimeFormatInfo;

public class Helpers {

	public static String CleanInvalidXmlChars(String text) {
		StringBuilder out = new StringBuilder();
		char current;

		if (text == null || ("".equals(text)))
			return "";
		for (int i = 0; i < text.length(); i++) {
			current = text.charAt(i);
			if ((current == 0x9) || (current == 0xA) || (current == 0xD)
					|| ((current >= 0x20) && (current <= 0xD7FF))
					|| ((current >= 0xE000) && (current <= 0xFFFD))
					|| ((current >= 0x10000) && (current <= 0x10FFFF)))
				out.append(current);
		}
		return out.toString();
	}

	public static String CleanString(String entry) {
		// '- &amp;
		// < - &lt;
		// > - &gt;
		// " - &quot;
		// ' - &#39;

		if (entry.contains("'")) {
			entry = entry.replace("'", "&apos;");
		}
		if (entry.contains("<")) {
			entry = entry.replace("<", "&lt;");
		}
		if (entry.contains(">")) {
			entry = entry.replace(">", "&gt;");
		}
		if (entry.contains("\"")) {
			entry = entry.replace("\"", "&quot;");
		}

		return entry;
	}
	
	 private String removeXMLBadAttribute(String entry)
     {
		 
		 if(entry.isEmpty())
			 return "";
         
		
         //'- &amp;
         //< - &lt;
         //> - &gt;
         //" - &quot;
         //' - &#39;

         if (entry.contains("'"))
         {
             entry = entry.replace("'", "");
         }
         if (entry.contains("<"))
         {
             entry = entry.replace("<", "");
         }
         if (entry.contains(">"))
         {
             entry = entry.replace(">", "");
         }
         if (entry.contains("\""))
         {
             entry = entry.replace("\"", "");
         }

         return entry;
     }

	public static Boolean CheckInvalidXmlChars(String text) {
		StringBuilder out = new StringBuilder();
		char current;

		if (text == null || ("".equals(text)))
			return false;
		for (int i = 0; i < text.length(); i++) {
			current = text.charAt(i);
			if ((current == 0x9) || (current == 0xA) || (current == 0xD)
					|| ((current >= 0x20) && (current <= 0xD7FF))
					|| ((current >= 0xE000) && (current <= 0xFFFD))
					|| ((current >= 0x10000) && (current <= 0x10FFFF)))
				out.append(current);
		}
		if (out.toString() == text) {
			return true;

		} else {
			return false;
		}

	}

	public static Node SelectChildNode(Node parent, String element)
	{
		Node returnthis = null;

		if(element.contains("/"))
		{
			String[] split = element.split("/");

			Node temp = parent;


			for(int i = 0; i< split.length;i++)
			{
				temp = Helpers.SelectChildNode(temp, split[0]);
			}

			returnthis = temp;
		} 
		else{
			Node child = null;
			for(int i = 0; i< parent.getChildNodes().getLength(); i++)
			{
				Node childnode = parent.getChildNodes().item(i);

				if(childnode.getNodeName() == element)
				{
					child = childnode;	
				}
			}
			returnthis = child;

		}
		return returnthis;
	}

	public static int CountSpecialCharacters(String value)
	{
		char[] specialchar = { '�', '�' }; //add more
		int count = 0;
		for (char c : specialchar)
		{
			int startindex = 0;
			while (true)
			{
				int indexvalue = value.indexOf(c, startindex);
				//Arrays.asList(headers).indexOf("Document Subclass");
				if (indexvalue != -1)
				{
					count++;
					startindex = indexvalue + 1;
				}
				else break;
			}
		}


		return count;
	}

	public static String DirtyString(String entry) {
		// '- &amp;
		// < - &lt;
		// > - &gt;
		// " - &quot;
		// ' - &#39;

		if (entry.contains("&apos;")) {
			entry = entry.replace("&apos;", "'");
		}
		if (entry.contains("&lt;")) {
			entry = entry.replace("&lt;", "<");
		}
		if (entry.contains("&gt;")) {
			entry = entry.replace("&gt;", "<");
		}
		if (entry.contains("&quot;")) {
			entry = entry.replace("&quot;", "\"");
		}

		return entry;

	}

	public static int GetFileCount(File dir, String file_extension) {
		final String ext = file_extension;

		if (file_extension.equals(""))
			return dir.listFiles().length;
		else {

			File[] di = dir.listFiles(new FilenameFilter() {
				public boolean accept(File dir, String name) {
					return name.toLowerCase().endsWith(ext);
				}
			});

			return di.length;
		}
	}

	public static String DateTimeNow(){

		Date today = new Date();
		SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy'T'HH:mm:SS");
		return DATE_FORMAT.format(today);
	}

	public static String FolderDate()
	{
		Date today = new Date();
		SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd_MM_yyyy_HHmmSS");
		return DATE_FORMAT.format(today);
	}
	
	public static Date ConvertToDate(String givenDate) throws ParseException
	{
		DateFormat formatter = new SimpleDateFormat("dd-MM-yy'T'HH:mm:SS");
		Date startDate = (Date)formatter.parse(givenDate);

		return startDate;

	}

	public static String GetDateInfo(String givenDate, String variable) throws ParseException
	{

		DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		Date startDate = (Date)formatter.parse(givenDate);

		Calendar cal = Calendar.getInstance();
		cal.setTime(startDate);

		switch (variable.toLowerCase()) {
		case "day":
			return GetDayofWeek(cal.get(Calendar.DAY_OF_WEEK + 1));
		case "month":
			return GetMonth(cal.get(Calendar.MONTH + 1));
		case "year":
			return String.valueOf(cal.get(Calendar.YEAR));

		default:
			return "";
		}

	}

	public static String GetDateTimeFormat() {
		Date date = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH + 1);
		int day = cal.get(Calendar.DAY_OF_MONTH + 1);
		int hour = cal.get(Calendar.HOUR);
		int min = cal.get(Calendar.MINUTE);
		int sec = cal.get(Calendar.SECOND);
		StringBuilder sb = new StringBuilder();
		sb.append(year);
		if (month < 10)
			sb.append("0");
		sb.append(month);
		if (day < 10)
			sb.append("0");
		sb.append(day).append("_");
		if (hour < 10)
			sb.append("0");
		sb.append(hour);
		if (min < 10)
			sb.append("0");
		sb.append(min);
		if (sec < 10)
			sb.append("0");
		sb.append(sec);

		return sb.toString();
	}

	@SuppressWarnings("deprecation")
	public static String GetMonth(String givenDate) throws ParseException {

		DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		Date startDate = (Date)formatter.parse(givenDate);

		int month = startDate.getMonth()-1;

		switch (month) {
		case 1:
			return "January";
		case 2:
			return "February";
		case 3:
			return "March";
		case 4:
			return "April";
		case 5:
			return "May";
		case 6:
			return "June";
		case 7:
			return "July";
		case 8:
			return "August";
		case 9:
			return "September";
		case 10:
			return "October";
		case 11:
			return "November";
		case 12:
			return "December";

		default:
			return "";
		}
	}

	public static String GetMonth(int month) {
		switch (month) {
		case 1:
			return "January";
		case 2:
			return "February";
		case 3:
			return "March";
		case 4:
			return "April";
		case 5:
			return "May";
		case 6:
			return "June";
		case 7:
			return "July";
		case 8:
			return "August";
		case 9:
			return "September";
		case 10:
			return "October";
		case 11:
			return "November";
		case 12:
			return "December";

		default:
			return "";
		}
	}

	public static String GetDayofWeek(int day) {
		switch (day) {
		case 1:
			return "Sunday";
		case 2:
			return "Monday";
		case 3:
			return "Tuesday";
		case 4:
			return "Wednesday";
		case 5:
			return "Thursday";
		case 6:
			return "Friday";
		case 7:
			return "Saturday";

		default:
			return "";
		}
	}

	public static String TimerGetElapsed(long elapsedseconds) {
		long elapsedhr = (elapsedseconds / 3600);
		long elapsedmin = ((elapsedseconds % 3600) / 60);
		long elapsedsec = ((elapsedseconds % 3600) % 60);

		StringBuilder sb = new StringBuilder();
		if (elapsedhr < 10) {
			sb.append("0");
		}
		sb.append(elapsedhr).append(":");

		if (elapsedmin < 10) {
			sb.append("0");
		}
		sb.append(elapsedmin).append(":");

		if (elapsedsec < 10) {
			sb.append("0");
		}
		sb.append(elapsedsec);

		return sb.toString();
	}

	public static String GetMimeType(String filetype) {

		switch (filetype) {
		case ".au":
			return "audio/basic";
		case ".avi":
			return "video/msvideo, video/avi, video/x-msvideo";
		case ".bmp":
			return "image/bmp";
		case ".bz2":
			return "application/x-bzip2";
		case ".css":
			return "text/css";
		case ".dtd":
			return "application/xml-dtd";
		case ".doc":
			return "application/msword";
		case ".docx":
			return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
		case ".dotx":
			return "application/vnd.openxmlformats-officedocument.wordprocessingml.template";
		case ".es":
			return "application/ecmascript";
		case ".exe":
			return "application/octet-stream";
		case ".gif":
			return "image/gif";
		case ".gz":
			return "application/x-gzip";
		case ".hqx":
			return "application/mac-binhex40";
		case ".html":
			return "text/html";
		case ".jar":
			return "application/java-archive";
		case ".jpg":
			return "image/jpeg";
		case ".js":
			return "application/x-javascript";
		case ".midi":
			return "audio/x-midi";
		case ".mp3":
			return "audio/mpeg";
		case ".mpeg":
			return "video/mpeg";
		case ".ogg":
			return "audio/vorbis, application/ogg";
		case ".pdf":
			return "application/pdf";
		case ".pl":
			return "application/x-perl";
		case ".png":
			return "image/png";
		case ".potx":
			return "application/vnd.openxmlformats-officedocument.presentationml.template";
		case ".ppsx":
			return "application/vnd.openxmlformats-officedocument.presentationml.slideshow";
		case ".ppt":
			return "application/vnd.ms-powerpointtd>";
		case "":
			return "";
		case ".pptx":
			return "application/vnd.openxmlformats-officedocument.presentationml.presentation";
		case ".ps":
			return "application/postscript";
		case ".qt":
			return "video/quicktime";
		case ".ra":
			return "audio/x-pn-realaudio, audio/vnd.rn-realaudio";
		case ".ram":
			return "audio/x-pn-realaudio, audio/vnd.rn-realaudio";
		case ".rdf":
			return "application/rdf, application/rdf+xml";
		case ".rtf":
			return "application/rtf";
		case ".sgml":
			return "text/sgml";
		case ".sit":
			return "application/x-stuffit";
		case ".sldx":
			return "application/vnd.openxmlformats-officedocument.presentationml.slide";
		case ".svg":
			return "image/svg+xml";
		case ".swf":
			return "application/x-shockwave-flash";
		case ".tar.gz":
			return "application/x-tar";
		case ".tgz":
			return "application/x-tar";
		case ".tiff":
			return "image/tiff";
		case ".tsv":
			return "text/tab-separated-values";
		case ".txt":
			return "text/plain";
		case ".wav":
			return "audio/wav, audio/x-wav";
		case ".xlam":
			return "application/vnd.ms-excel.addin.macroEnabled.12";
		case ".xls":
			return "application/vnd.ms-excel";
		case ".xlsb":
			return "application/vnd.ms-excel.sheet.binary.macroEnabled.12";
		case ".xlsx":
			return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
		case ".xltx":
			return "application/vnd.openxmlformats-officedocument.spreadsheetml.template";
		case ".xml":
			return "application/xml";
		case ".zip":
			return "application/zip, application/x-compressed-zip";
		}

		return "";

	}

	public static String CombinePaths(String path1, String path2) {
		File file1 = new File(path1);
		File file2 = new File(file1, path2);
		return file2.getPath();
	}

	public static String CombinePaths(String path1, String path2, String path3) {
		File file1 = new File(path1);
		File file2 = new File(file1, path2);
		File file3 = new File(file2, path3);
		return file3.getPath();
	}

	public static String[] StringSplit(String s, int delimiterCase)
	{
		switch (delimiterCase)
		{
		case 1:
			//assume , is the original delimiter and there are cases \, 
			//s = s.Replace(".,.", ",").Replace(".,", "|").Replace(",.", "|");
			//s = s.Replace(",", "|").Replace("\\|", ",");
			//return s.Split("|".ToCharArray());
			s = s.replace(".,.", "|").replace(".,", "^").replace(",.", "^");
			s = s.replace(",", "^").replace("\\^", ",");
			return s.split("\\^");
		case 2:
			
			
		default:
			return s.split(",");
		}

	}
	
	public static String[] StringSplit(String textString)
	{
		int delimiterCase = 1;
		
		 if(textString.contains("\""))
         {

             delimiterCase = 2;
         }
		
		switch (delimiterCase)
		{
		case 1:
			//assume , is the original delimiter and there are cases \, 
			//s = s.Replace(".,.", ",").Replace(".,", "|").Replace(",.", "|");
			//s = s.Replace(",", "|").Replace("\\|", ",");
			//return s.Split("|".ToCharArray());
			textString = textString.replace(".,.", "|").replace(".,", "^").replace(",.", "^");
			textString = textString.replace(",", "^").replace("\\^", ",");
			return textString.split("\\^");
		case 2:
			
			 List<String> combineString = new ArrayList<String>();

             String tempString = "";

             Boolean trailing = false;

             for(int i = 0; i<textString.length(); i++)
             {
            	 if(tempString.equals("don"))
            	 {
            		 System.out.print("");
            	 }
                 char car = textString.charAt(i);

                 if((car == '"') && trailing)
                 { 
                     combineString.add(tempString);
                     tempString = "";
                     if(textString.charAt(i+1) == ',')
                     {
                    	 trailing = false;
                     }
                     
                 }
                 else if(car == ',' && trailing)
                 {
                     tempString = tempString + car;
                 }
                 else if(car == '"' && !trailing)
                 {
                     trailing = true;
                     combineString.add(tempString);
                     tempString = "";
                 }
                 else if(car == ',' && !trailing)
                 {
                	
                	 if(textString.charAt(i+1) == '"')
                	 {
                		 trailing = true;
                	 }
                	 else if(textString.charAt(i-1) == '"')
                	 {
                		 trailing = false;
	                    
	                     tempString = "";
                	 }
                	 else  if(i == textString.length() - 1 )
                	 {
                		 combineString.add(tempString);
                		 tempString = "";
                	 }
                	 else
                	 {
                		 combineString.add(tempString);
                		 tempString = "";
                	 }
                 }
                
                 else
                 {
                	 
                     tempString = tempString + car;
                     
                     if(i == textString.length() - 1 )
                	 {
                		 combineString.add(tempString);
                		 tempString = "";
                	 }
                 }


             }

             return  combineString.toArray(new String[0]);
			
		default:
			return textString.split(",");
		}

	}
	

	public static String getDateTimeFormatInfo(String dateString, String pattern)
	{
		//	   CultureInfo culture = (CultureInfo)CultureInfo.GetCultureInfo("en-US");

		//  Thread.CurrentThread.CurrentCulture = new CultureInfo("da-DK", false);

		Locale invariant = new Locale("en","gb","");

		DateTimeFormatInfo dfi = LocaleInfo.getCurrentLocale().getDateTimeFormatInfo();

		String dtOut = "";

		try
		{
			DateTimeFormat formatter = DateTimeFormat.getFormat(pattern);

			Date dtIn = formatter.parse(dateString); 

			SimpleDateFormat outFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
			dtOut = outFormat.format(dtIn);
		}
		catch(Exception ex){

		}

		return dtOut;
	}
	
	public  static void saveXML(String xmlFile, Document domSource)
			throws TransformerFactoryConfigurationError {
		try {

			Source source = new DOMSource(domSource);

			TransformerFactory factory = TransformerFactory.newInstance();
			factory.setAttribute("indent-number", new Integer(2));

			Transformer transformer = factory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
			Result result = new StreamResult(xmlFile);
			transformer.transform(source, result);

		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
	}

	public static String toISO8601CompliantDate(String inputDate, String date_format) throws ParseException
	{
		// create a date object for testing
		SimpleDateFormat simpleFormat = (SimpleDateFormat) DateFormat.getDateInstance();
	    simpleFormat.applyPattern(date_format);
	    Date date = new Date();
	    // parse date 2013-04-12 10:02:12
	    date = simpleFormat.parse(inputDate);
	    DateFormat format = DateFormat.getDateInstance();
		format = DateFormat.getDateInstance(DateFormat.LONG);
	   
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmXXX"); 
		df.format(date);
	//	Date dd = Helpers.parse(format.format(date));
	//	Date rr =  Helpers.parse(df.format(date));
	//	String rrr = Helpers.toString(rr);
		String rrr  = df.format(date);
		return rrr;
		
//	    System.out.println(date); // Fri Apr 12 10:02:12 IST 2013
//		
//		
//		// create a date instance using default style
//		DateFormat format = DateFormat.getDateInstance();
//		System.out.println(format.format(date)); //29 Jul, 2013
//		
//		// create a dateinstance using short style
//	
//		System.out.println(format.format(date));// 29/7/13
//		
//		// create a dateinstance using medium style (Default)
//		format = DateFormat.getDateInstance(DateFormat.MEDIUM);
//		System.out.println(format.format(date));// 29 Jul, 2013
//	
//		// create a dateinstance using full style (Default)
//		format = DateFormat.getDateInstance(DateFormat.FULL);
//		System.out.println(format.format(date));// Monday, 29 July, 2013
//	
//		// create a dateinstance using long style (Default)
//		format = DateFormat.getDateInstance(DateFormat.LONG);
//		System.out.println(format.format(date));// 29 July, 2013
//	
//		// create time instance - default
//		format = DateFormat.getTimeInstance();
//		System.out.println(format.format(date));// 8:24:59 PM
//	
//		// create time instance - short
//		format = DateFormat.getTimeInstance(DateFormat.SHORT);
//		System.out.println(format.format(date));// 8:26 PM
//	
//		// create time instance - medium
//		format = DateFormat.getTimeInstance(DateFormat.MEDIUM);
//		System.out.println(format.format(date));// 8:26:21 PM
//	
//		// create time instance - full
//		format = DateFormat.getTimeInstance(DateFormat.FULL);
//		System.out.println(format.format(date));// 8:26:21 PM IST
//	
//		// create time instance - long
//		format = DateFormat.getTimeInstance(DateFormat.LONG);
//		System.out.println(format.format(date));// 8:26:21 PM IST
//	
//		// create date time instance - default.
//		format = DateFormat.getDateTimeInstance();
//		System.out.println(format.format(date));// 29 Jul, 2013 8:27:22 PM
//	
//		// create date time instance - specify format and locale.
//		format = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL, Locale.GERMAN);
//		System.out.println(format.format(date));// Montag, 29. Juli 2013 20:57 Uhr IST
		
	
	}
	public static Date parse( String input ) throws java.text.ParseException 
	{

		//NOTE: SimpleDateFormat uses GMT[-+]hh:mm for the TZ which breaks
		//things a bit.  Before we go on we have to repair this.
		SimpleDateFormat df = new SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ssz" );

		//this is zero time so we need to add that TZ indicator for 
		if ( input.endsWith( "Z" ) ) {
			input = input.substring( 0, input.length() - 1) + "GMT-00:00";
		} else {
			int inset = 6;

			String s0 = input.substring( 0, input.length() - inset );
			String s1 = input.substring( input.length() - inset, input.length() );

			input = s0 + "GMT" + s1;
		}

		return df.parse( input );

	}

	public static String toString( Date date ) {

		SimpleDateFormat df = new SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ssz" );

		TimeZone tz = TimeZone.getTimeZone( "UTC" );

		df.setTimeZone( tz );

		String output = df.format( date );

		int inset0 = 9;
		int inset1 = 6;

		String s0 = output.substring( 0, output.length() - inset0 );
		String s1 = output.substring( output.length() - inset1, output.length() );

		String result = s0 + s1;

		result = result.replaceAll( "UTC", "+00:00" );

		return result;

	}

	public static Boolean hasSpecialCharacter(String queryString)
	{
		Pattern p = Pattern.compile("[^a-z0-9 /\\\\]", Pattern.CASE_INSENSITIVE);
		Matcher m = p.matcher(queryString);
		boolean b = m.find();

		return b;
	}
}
