package ib.commons.uploader.utilities.logger;

import ib.commons.uploader.utilities.configuration.Configuration;
import ib.commons.uploader.utilities.helpers.Helpers;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.springframework.util.StringUtils;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class ULogger {


	static String operationlog = "";
	static String operationlogfolder = "";
	static String operationlogfile = "";
	static PrintWriter printWriter;
	static Boolean isConsoleProgress = false;


	public static void Logger()
	{

		try
		{
			Configuration conf = new Configuration();

			String logFolder = conf.get("Operationlogs");
			isConsoleProgress = Boolean.valueOf(conf.get("conf_isConsoleProgress"));

			Date today = new Date();


			Calendar cal = Calendar.getInstance();
			cal.setTime(today);
			int year = cal.get(Calendar.YEAR);
			int month = cal.get(Calendar.MONTH);
			int day = cal.get(Calendar.DAY_OF_MONTH);

			String logtemp = "log_" + Helpers.GetMonth(month+1)+ "_" + day + "_" + year + ".log";

			Path temp = Paths.get(logFolder, logtemp);

			File fi = new File(temp.toString());


			if (fi.exists())
			{
				operationlogfile = temp.toString();
			}
			else
			{
				try
				{
					FileWriter fw = new FileWriter(fi,true); //the true will append the new data
					//	fw.write("add a line\n");//appends the string to the file
					fw.close();
				}
				catch(IOException ioe)
				{
					System.err.println("IOException: " + ioe.getMessage());
				}

				operationlogfile = temp.toString();
			}
		}
		catch(Exception ex)
		{

		}

	}

	public static void Logger(Path path)
	{

		try
		{
			Configuration conf = new Configuration();

			String logFolder = conf.get("Operationlogs");
			isConsoleProgress = Boolean.valueOf(conf.get("conf_isConsoleProgress"));

			Date today = new Date();


			//			Calendar cal = Calendar.getInstance();
			//			cal.setTime(today);
			//			int year = cal.get(Calendar.YEAR);
			//			int month = cal.get(Calendar.MONTH);
			//			int day = cal.get(Calendar.DAY_OF_MONTH);
			//	
			//			String logtemp = "log_" + Helpers.GetMonth(month+1)+ "_" + day + "_" + year + ".log";
			//	
			//			Path temp = Paths.get(logFolder, logtemp);
			//	
			//			File fi = new File(temp.toString());
			//	
			File fi = new File(path.toString());


			if (fi.exists())
			{

			}
			else
			{
				try
				{
					FileWriter fw = new FileWriter(fi,true); //the true will append the new data
					//	fw.write("add a line\n");//appends the string to the file
					fw.close();
				}
				catch(IOException ioe)
				{
					System.err.println("IOException: " + ioe.getMessage());
				}


			}
		}
		catch(Exception ex)
		{

		}

	}

	public static void Logger(PrintWriter printWriter)
	{

		try
		{
			Configuration conf = new Configuration();

			String logFolder = conf.get("Operationlogs");

			Date today = new Date();


			Calendar cal = Calendar.getInstance();
			cal.setTime(today);
			int year = cal.get(Calendar.YEAR);
			int month = cal.get(Calendar.MONTH);
			int day = cal.get(Calendar.DAY_OF_MONTH);

			String logtemp = "log_" + Helpers.GetMonth(month+1)+ "_" + day + "_" + year + ".log";

			Path temp = Paths.get(logFolder, logtemp);

			File fi = new File(temp.toString());


			if (fi.exists())
			{
				operationlogfile = temp.toString();
			}
			else
			{
				try
				{
					FileWriter fw = new FileWriter(fi,true); //the true will append the new data
					//	fw.write("add a line\n");//appends the string to the file
					fw.close();
				}
				catch(IOException ioe)
				{
					System.err.println("IOException: " + ioe.getMessage());
				}

				operationlogfile = temp.toString();
			}
		}
		catch(Exception ex)
		{

		}

	}

	public static void WebLogger()
	{
		//  operationlogfolder = System.Web.HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["Operationlogs"]);
		Date today = new Date();

		Calendar cal = Calendar.getInstance();
		cal.setTime(today);
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH);
		int day = cal.get(Calendar.DAY_OF_MONTH);

		String logtemp = "log_" + Helpers.GetMonth(month+1)+ "_" + day + "_" + year + ".log";

		Path temp = Paths.get(logtemp,logtemp);

		File fi = new File(temp.toString());


		if (fi.exists())
		{

			operationlogfile = temp.toString();
		}
		else
		{
			try
			{
				FileWriter fw = new FileWriter(fi,true); //the true will append the new data

				fw.close();
			}
			catch(IOException ioe)
			{
				System.err.println("IOException: " + ioe.getMessage());
			}

			operationlogfile = temp.toString();
		}
	}

	private static String DescriptorLog(String path)
	{
		Date today = new Date();

		Calendar cal = Calendar.getInstance();
		cal.setTime(today);
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH);
		int day = cal.get(Calendar.DAY_OF_MONTH);

		String logtemp = "DescriptorLog_" + Helpers.GetMonth(month+1)+ "_" + day + "_" + year + ".log";

		String temp = Paths.get(path, logtemp).toString();

		try
		{
			File fi = new File(temp);
			FileWriter fw = new FileWriter(fi,true); //the true will append the new data
			fw.write("\n");

			fw.close();

		}
		catch(Exception ex)
		{
			
			
		}

		return temp;
		
	}

	public static void AppendtoLog(String type, String value)
	{
		if (operationlogfile == "")
			Logger();

		Date today = new Date();
		SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yy:HH:mm:SS");

		try
		{
			if(isConsoleProgress)
			{
				System.out.println(value);
			}

			File fi = new File(operationlogfile);
			FileWriter fw = new FileWriter(fi,true); //the true will append the new data
			fw.write("\n");
			fw.write(DATE_FORMAT.format(today) + "," + type + "," + value.trim());;//appends the string to the file
			fw.close();
		}
		catch(IOException ioe)
		{
			if(isConsoleProgress)
			{
				System.err.println("IOException: " + ioe.getMessage());
			}
		}

	}

	public static void AppendtoLog(String type, String user, String value)
	{
		if (operationlogfile == "")
			Logger();

		Date today = new Date();
		SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yy:HH:mm:SS");

		try
		{
			if(isConsoleProgress)
			{
				System.out.println(value);
			}
			File fi = new File(operationlogfile);

			FileWriter fw = new FileWriter(fi,true); //the true will append the new data
			fw.write("\n");
			fw.write(DATE_FORMAT.format(today) + "," + type + "," + user + "," + value.trim());;//appends the string to the file
			fw.close();
		}
		catch(IOException ioe)
		{
			if(isConsoleProgress)
			{
				System.err.println("IOException: " + ioe.getMessage());
			}

		}

	}

	public static void AppendtoLog(String type, String value, Exception ex)
	{
		if (operationlogfile == "")
			Logger();

		Date today = new Date();
		SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yy:HH:mm:SS");

		try
		{
			if(isConsoleProgress)
			{
				System.err.println(ex);
			}

			File fi = new File(operationlogfile);

			FileWriter fw = new FileWriter(fi,true); //the true will append the new data
			fw.write("\n");
			fw.write(DATE_FORMAT.format(today) + "," + type + "," + value.trim()  + "," + "[Exception] " + ex.toString());//appends the string to the file
			fw.close();
		}
		catch(IOException ioe)
		{
			if(isConsoleProgress)
			{
				System.err.println("IOException: " + ioe.getMessage());
			}
		}

	}

	public static void AppendtoLog(String type, String user, String value, Exception ex)
	{
		if (operationlogfile == "")
			Logger();

		Date today = new Date();
		SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yy:HH:mm:SS");

		try
		{
			if(isConsoleProgress)
			{
				System.err.println(ex);
			}

			File fi = new File(operationlogfile);

			FileWriter fw = new FileWriter(fi,true); //the true will append the new data
			fw.write("\n");
			fw.write(DATE_FORMAT.format(today) + "," + type + "," + user + ","  + value.trim()  + "," + "[Exception] " + ex.getMessage());//appends the string to the file
			fw.close();
		}
		catch(IOException ioe)
		{
			if(isConsoleProgress)
			{
				System.err.println("IOException: " + ioe.getMessage());
			}
		}

	}

	public static void AppendtoWebLog(String type, String value)
	{
		if (operationlogfile == "")
			WebLogger();

		Date today = new Date();
		SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yy:HH:mm:SS");

		try
		{
			File fi = new File(operationlogfile);

			FileWriter fw = new FileWriter(fi,true); //the true will append the new data
			fw.write(DATE_FORMAT.format(today) + "," + type + "," + value.trim());;//appends the string to the file
			fw.close();
		}
		catch(IOException ioe)
		{
			System.err.println("IOException: " + ioe.getMessage());
		}

	}

	public static void AppendtoWebLog(String type, String user, String value)
	{
		if (operationlogfile == "")
			WebLogger();

		Date today = new Date();
		SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yy:HH:mm:SS");

		try
		{
			File fi = new File(operationlogfile);

			FileWriter fw = new FileWriter(fi,true); //the true will append the new data
			fw.write(DATE_FORMAT.format(today) + "," + user + "," + type + "," + value);;//appends the string to the file
			fw.close();
		}
		catch(IOException ioe)
		{
			System.err.println("IOException: " + ioe.getMessage());
		}

	}

	public static void AppendtoDescriptorLog(String descriptorlogfile, String index, String line, String currentProcess, String status, String message) throws Exception
	{  
//		String descriptorlogfile = DescriptorLog(workingDirectory);
		
		File fi = new File(descriptorlogfile);

		if (fi.exists())
		{
			Document doc_descriptor_log;
			DocumentBuilderFactory doc_descriptor_Factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder doc_descriptor_Builder = doc_descriptor_Factory.newDocumentBuilder();
			doc_descriptor_log = doc_descriptor_Builder.parse(descriptorlogfile);

			XPath xPath =  XPathFactory.newInstance().newXPath();

			Node parentnode = (Node)xPath.compile("vd/children").evaluate(doc_descriptor_log, XPathConstants.NODE);  // doc_descriptor_log.SelectSingleNode("/InfobuilderVolume/logs");


			Element childnode = doc_descriptor_log.createElement("index");

			childnode.setAttribute("no", index);
			childnode.setAttribute("status", status);
			childnode.setAttribute("currentProcess", currentProcess);
			childnode.setAttribute("message", message);

			parentnode.appendChild(childnode);

			DOMSource source = new DOMSource(doc_descriptor_log);

			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			StreamResult result = new StreamResult(descriptorlogfile);
			transformer.transform(source, result);
		}
		else
		{

			DocumentBuilderFactory docFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			Document doc_descriptor_log = docBuilder.newDocument();
			Element rootElement = doc_descriptor_log.createElement("InfobuilderVolume");
			Element log = doc_descriptor_log.createElement("log");
			Element childnode = doc_descriptor_log.createElement("index");

			childnode.setAttribute("no", index);
			childnode.setAttribute("status", status);
			childnode.setAttribute("currentProcess", currentProcess);
			childnode.setAttribute("message", message);

			log.appendChild(childnode);
			rootElement.appendChild(log);

			DOMSource source = new DOMSource(doc_descriptor_log);

			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			StreamResult result = new StreamResult(descriptorlogfile);
			transformer.transform(source, result);

		}

	}

//	public static void AppendtoDescriptorLog(String workingDirectory, String index, String line, String currentProcess, String status, String message) throws Exception
//	{  
//		String descriptorlogfile = DescriptorLog(workingDirectory);
//		
//		File fi = new File(descriptorlogfile);
//
//		if (fi.exists())
//		{
//			Document doc_descriptor_log;
//			DocumentBuilderFactory doc_descriptor_Factory = DocumentBuilderFactory.newInstance();
//			DocumentBuilder doc_descriptor_Builder = doc_descriptor_Factory.newDocumentBuilder();
//			doc_descriptor_log = doc_descriptor_Builder.parse(descriptorlogfile);
//
//			XPath xPath =  XPathFactory.newInstance().newXPath();
//
//			Node parentnode = (Node)xPath.compile("vd/children").evaluate(doc_descriptor_log, XPathConstants.NODE);  // doc_descriptor_log.SelectSingleNode("/InfobuilderVolume/logs");
//
//
//			Element childnode = doc_descriptor_log.createElement("index");
//
//			childnode.setAttribute("no", index);
//			childnode.setAttribute("status", status);
//			childnode.setAttribute("currentProcess", currentProcess);
//			childnode.setAttribute("message", message);
//
//			parentnode.appendChild(childnode);
//
//			DOMSource source = new DOMSource(doc_descriptor_log);
//
//			TransformerFactory transformerFactory = TransformerFactory.newInstance();
//			Transformer transformer = transformerFactory.newTransformer();
//			StreamResult result = new StreamResult(descriptorlogfile);
//			transformer.transform(source, result);
//		}
//		else
//		{
//
//			DocumentBuilderFactory docFactory = DocumentBuilderFactory
//					.newInstance();
//			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
//
//			Document doc_descriptor_log = docBuilder.newDocument();
//			Element rootElement = doc_descriptor_log.createElement("InfobuilderVolume");
//			Element log = doc_descriptor_log.createElement("log");
//			Element childnode = doc_descriptor_log.createElement("index");
//
//			childnode.setAttribute("no", index);
//			childnode.setAttribute("status", status);
//			childnode.setAttribute("currentProcess", currentProcess);
//			childnode.setAttribute("message", message);
//
//			log.appendChild(childnode);
//			rootElement.appendChild(log);
//
//			DOMSource source = new DOMSource(doc_descriptor_log);
//
//			TransformerFactory transformerFactory = TransformerFactory.newInstance();
//			Transformer transformer = transformerFactory.newTransformer();
//			StreamResult result = new StreamResult(descriptorlogfile);
//			transformer.transform(source, result);
//
//		}
//
//	}
//
//	
	
	////////////Customize Path /////////////
	public static void AppendtoLog(int level, String type, String value)
	{




		if (operationlogfile == "")
			Logger();

		Date today = new Date();
		SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yy:HH:mm:SS");

		try
		{
			Configuration conf = new Configuration();
			String logLevels = conf.get("conf_logLevel");

			if(logLevels.equals("1") || logLevels.contains(String.valueOf(level)))
			{
				if(isConsoleProgress)
				{
					System.out.println("   " + value);
				}

				File fi = new File(operationlogfile);
				FileWriter fw = new FileWriter(fi,true); //the true will append the new data
				fw.write("\n");
				fw.write(DATE_FORMAT.format(today) + "," + type + "," + value.trim());;//appends the string to the file
				fw.close();
			}
		}
		catch(Exception ioe)
		{
			if(isConsoleProgress)
			{
				System.err.println("IOException: " + ioe.getMessage());
			}
		}


	}

	public static void AppendtoLog(int level, String type, String user, String value)
	{
		if (operationlogfile == "")
			Logger();

		Date today = new Date();
		SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yy:HH:mm:SS");

		try
		{
			Configuration conf = new Configuration();
			String logLevels = conf.get("conf_logLevel");

			if(logLevels.equals("1") || logLevels.contains(String.valueOf(level)))
			{
				if(isConsoleProgress)
				{
					System.out.println(value);
				}
				
				File fi = new File(operationlogfile);

				FileWriter fw = new FileWriter(fi,true); //the true will append the new data
				fw.write("\n");
				fw.write(DATE_FORMAT.format(today) + "," + type + "," + user + "," + value.trim());;//appends the string to the file
				fw.close();
			}
		}
		catch(Exception ioe)
		{
			if(isConsoleProgress)
			{
				System.err.println("IOException: " + ioe.getMessage());
			}

		}

	}

	public static void AppendtoLog(int level, String type, String value, Exception ex)
	{
		if (operationlogfile == "")
			Logger();

		Date today = new Date();
		SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yy:HH:mm:SS");

		try
		{
			Configuration conf = new Configuration();
			String logLevels = conf.get("conf_logLevel");

			if(logLevels.equals("1") || logLevels.contains(String.valueOf(level)))
			{
				if(isConsoleProgress)
				{
					System.err.println(ex);
				}


				File fi = new File(operationlogfile);

				FileWriter fw = new FileWriter(fi,true); //the true will append the new data
				fw.write("\n");
				fw.write(DATE_FORMAT.format(today) + "," + type + "," + value.trim()  + "," + "[Exception] " + ex.toString());//appends the string to the file
				fw.close();
			}
		}
		catch(Exception ioe)
		{
			if(isConsoleProgress)
			{
				System.err.println("IOException: " + ioe.getMessage());
			}
		}

	}

	public static void AppendtoLog(int level, String type, String user, String value, Exception ex)
	{
		if (operationlogfile == "")
			Logger();

		Date today = new Date();
		SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yy:HH:mm:SS");

		try
		{
			Configuration conf = new Configuration();
			String logLevels = conf.get("conf_logLevel");

			if(logLevels.equals("1") || logLevels.contains(String.valueOf(level)))
			{
				if(isConsoleProgress)
				{
					System.out.println(value);
				}
				File fi = new File(operationlogfile);

				FileWriter fw = new FileWriter(fi,true); //the true will append the new data
				fw.write("\n");
				fw.write(DATE_FORMAT.format(today) + "," + type + "," + user + ","  + value.trim()  + "," + "[Exception] " + ex.getMessage());//appends the string to the file
				fw.close();
			}
		}
		catch(Exception ioe)
		{
			if(isConsoleProgress)
			{
				System.err.println("IOException: " + ioe.getMessage());
			}
		}

	}

	//////////// Customize Path /////////////
	public static void AppendtoLog(String type, String value, Path path)
	{
		if(path == null)
		{
			AppendtoLog(type, value);
			return;
		}

		if (!path.toFile().exists())
			Logger(path);

		Date today = new Date();
		SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yy:HH:mm:SS");

		try
		{
			if(isConsoleProgress)
			{
				System.out.println(value);
			}

			File fi = path.toFile();
			FileWriter fw = new FileWriter(fi,true); //the true will append the new data
			fw.write("\n");
			fw.write(DATE_FORMAT.format(today) + "," + type + "," + value.trim());;//appends the string to the file
			fw.close();
		}
		catch(IOException ioe)
		{
			if(isConsoleProgress)
			{
				System.err.println("IOException: " + ioe.getMessage());
			}
		}
	}

	public static void AppendtoLog(String type, String user, String value, Path path)
	{
		if(path == null)
		{
			AppendtoLog(type, user, value);
			return;
		}

		if (!path.toFile().exists())
			Logger(path);

		Date today = new Date();
		SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yy:HH:mm:SS");

		try
		{
			if(isConsoleProgress)
			{
				System.out.println(value);
			}
			File fi = path.toFile();

			FileWriter fw = new FileWriter(fi,true); //the true will append the new data
			fw.write("\n");
			fw.write(DATE_FORMAT.format(today) + "," + type + "," + user + "," + value.trim());;//appends the string to the file
			fw.close();
		}
		catch(IOException ioe)
		{
			if(isConsoleProgress)
			{
				System.err.println("IOException: " + ioe.getMessage());
			}

		}

	}

	public static void AppendtoLog(String type, String value, Exception ex, Path path)
	{
		if(path == null)
		{
			AppendtoLog(type, value, ex);
			return;
		}

		if (!path.toFile().exists())
			Logger(path);

		Date today = new Date();
		SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yy:HH:mm:SS");

		try
		{
			if(isConsoleProgress)
			{
				System.err.println(value);
			}

			File fi = path.toFile();

			FileWriter fw = new FileWriter(fi,true); //the true will append the new data
			fw.write("\n");
			fw.write(DATE_FORMAT.format(today) + "," + type + "," + value.trim()  + "," + "[Exception] " + ex.toString());//appends the string to the file
			fw.close();
		}
		catch(IOException ioe)
		{
			if(isConsoleProgress)
			{
				System.err.println("IOException: " + ioe.getMessage());
			}
		}

	}

	public static void AppendtoLog(String type, String user, String value, Exception ex, Path path)
	{
		if(path == null)
		{
			AppendtoLog(type, user, value,ex);
			return;
		}

		if (!path.toFile().exists())
			Logger(path);

		Date today = new Date();
		SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yy:HH:mm:SS");

		try
		{
			if(isConsoleProgress)
			{
				System.err.println(value);
			}


			File fi = path.toFile();

			FileWriter fw = new FileWriter(fi,true); //the true will append the new data
			fw.write("\n");
			fw.write(DATE_FORMAT.format(today) + "," + type + "," + user + ","  + value.trim()  + "," + "[Exception] " + ex.getMessage());//appends the string to the file
			fw.close();
		}
		catch(IOException ioe)
		{
			if(isConsoleProgress)
			{
				System.err.println("IOException: " + ioe.getMessage());
			}
		}

	}

	////////////Customize Path with Level/////////////
	public static void AppendtoLog(int level, String type, String value, Path path)
	{
		if(path == null)
		{
			AppendtoLog(level, type, value);
			return;
		}

		if (!path.toFile().exists())
			Logger(path);

		Date today = new Date();
		SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yy:HH:mm:SS");

		try
		{
			Configuration conf = new Configuration();
			String logLevels = conf.get("conf_logLevel");

			if(logLevels.equals("1") || logLevels.contains(String.valueOf(level)))
			{
				if(isConsoleProgress)
				{
					System.out.println(value);
				}

				File fi = path.toFile();
				FileWriter fw = new FileWriter(fi,true); //the true will append the new data
				fw.write("\n");
				fw.write(DATE_FORMAT.format(today) + "," + type + "," + value.trim());;//appends the string to the file
				fw.close();
			}
		}
		catch(Exception ioe)
		{
			if(isConsoleProgress)
			{
				System.err.println("IOException: " + ioe.getMessage());
			}
		}
	}

	public static void AppendtoLog(int level, String type, String user, String value, Path path)
	{
		if(path == null)
		{
			AppendtoLog(level, type, user, value);
			return;
		}

		if (!path.toFile().exists())
			Logger(path);

		Date today = new Date();
		SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yy:HH:mm:SS");

		try
		{
			Configuration conf = new Configuration();
			int logLevel = Integer.valueOf(conf.get("conf_logLevel"));
			String logLevels = conf.get("conf_logLevel");

			if(logLevels.equals("1") || logLevels.contains(String.valueOf(level)))
			{
				if(isConsoleProgress)
				{
					System.out.println(value);
				}
				File fi = path.toFile();

				FileWriter fw = new FileWriter(fi,true); //the true will append the new data
				fw.write("\n");
				fw.write(DATE_FORMAT.format(today) + "," + type + "," + user + "," + value.trim());;//appends the string to the file
				fw.close();
			}
		}
		catch(Exception ioe)
		{
			if(isConsoleProgress)
			{
				System.err.println("IOException: " + ioe.getMessage());
			}

		}

	}

	public static void AppendtoLog(int level, String type, String value, Exception ex, Path path)
	{
		if(path == null)
		{
			AppendtoLog(level, type, value, ex);
			return;
		}

		if (!path.toFile().exists())
			Logger(path);

		Date today = new Date();
		SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yy:HH:mm:SS");

		try
		{
			Configuration conf = new Configuration();
			String logLevels = conf.get("conf_logLevel");

			if(logLevels.equals("1") || logLevels.contains(String.valueOf(level)))
			{
				if(isConsoleProgress)
				{
					System.err.println(value);
				}

				File fi = path.toFile();

				FileWriter fw = new FileWriter(fi,true); //the true will append the new data
				fw.write("\n");
				fw.write(DATE_FORMAT.format(today) + "," + type + "," + value.trim()  + "," + "[Exception] " + ex.toString());//appends the string to the file
				fw.close();
			}
		}
		catch(Exception ioe)
		{
			if(isConsoleProgress)
			{
				System.err.println("IOException: " + ioe.getMessage());
			}
		}

	}

	public static void AppendtoLog(int level, String type, String user, String value, Exception ex, Path path)
	{
		if(path == null)
		{
			AppendtoLog(type, user, value,ex);
			return;
		}

		if (!path.toFile().exists())
			Logger(path);

		Date today = new Date();
		SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yy:HH:mm:SS");

		try
		{
			Configuration conf = new Configuration();
			String logLevels = conf.get("conf_logLevel");

			if(logLevels.equals("1") || logLevels.contains(String.valueOf(level)))
			{
				if(isConsoleProgress)
				{
					System.err.println(value);
				}


				File fi = path.toFile();

				FileWriter fw = new FileWriter(fi,true); //the true will append the new data
				fw.write("\n");
				fw.write(DATE_FORMAT.format(today) + "," + type + "," + user + ","  + value.trim()  + "," + "[Exception] " + ex.getMessage());//appends the string to the file
				fw.close();
			}
		}
		catch(Exception ioe)
		{
			if(isConsoleProgress)
			{
				System.err.println("IOException: " + ioe.getMessage());
			}
		}

	}

}
